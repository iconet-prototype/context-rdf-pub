# iconet prototype development - context A: rdf-pub

Original rdf-pub project: https://gitlab.com/linkedopenactors/rdf-pub/

## Active Development

### Communication: 
https://matrix.to/#/#iconet-foundation:matrix.org

Our project matrix space, find the development room for development specific communication

### Weekly meeting: 
Wednesdays 19:30 (if not otherwise arranged via Matrix)
Our Gather Space:
 
https://gather.town/app/j6buhleMZk51ANEi/SNAC

### Protocols
https://iconet-foundation.org/dev-meeting-protocols


## Team Bootstrapping

To speed up the bootstrapping process, it would be a great time saver if everyone had the following tools already installed:
- Git
- IntelliJ Community Edition
- Docker

There are more to come, we are currently figuring them out.
These will be the absolute basics, so please get a fundamental understanding of what each of these tools are used for if you don't do so already. I can give a refresher course showing anyone who wants the minimal steps you need to know. Please look into this in advance so we don't have to start from scratch.

To the Linux users: if your distribution's version of the docker package is lagging behind too much, it may be necessary to follow the installation instructions on the website.
