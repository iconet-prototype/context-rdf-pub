package org.linkedopenactors.rdfpub.client;

import java.time.Instant;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleActivity {

	private Model activityModel;
	private IRI activityId;

	public SimpleActivity(IRI activityId, Model activityModel) {
		this.activityId = activityId;
		this.activityModel = activityModel;
	}

	public IRI type() {
		Set<IRI> typeIRIs = types();
		if(typeIRIs.size()>1) {
			throw new RuntimeException("more than one type IRI! " + typeIRIs);
		} 
		return typeIRIs.stream().findFirst().orElseThrow(()->new RuntimeException("no type IRI!"));
	}

	private Set<IRI> types() {
		return Models.getPropertyIRIs(activityModel, activityId, RDF.TYPE);
	}

	public IRI actor() {
		return Models.getPropertyIRI(activityModel, activityId, AS.actor).orElseThrow();
	}

	public IRI object() {
		return Models.getPropertyIRI(activityModel, activityId, AS.object).orElseThrow();
	}

	public Instant published() {
		return Instant.parse(Models.getPropertyLiteral(activityModel, activityId, AS.published).orElseThrow().stringValue());
	}
	
	public void trace(String msg) {
		ModelLogger.trace(log, activityModel, msg);
	}
}
