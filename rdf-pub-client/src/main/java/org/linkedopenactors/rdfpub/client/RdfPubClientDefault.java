package org.linkedopenactors.rdfpub.client;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.Header;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

/**
 * Default implementation of {@link RdfPubClient}.  
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class RdfPubClientDefault implements RdfPubClient {
	
	private static final MediaType MEDIA_TYPE_JSON_LD = new MediaType("application", "ld+json");
	private WebClient webClient;
	private String keycloakServerUrl;
	private String clientApplicationRealm;
	private String clientApplicationClientId;
	private IRI clientApplicationActor;
	private String clientApplicationPassword;
	private String clientApplicationClientSecret;
	private int activityPubClientFactoryHttpClientTimeout;
	private String clientApplicationUserName;
	private Optional<Model> profileOptional;
	
	/**
	 * 
	 * @param webClient The webclient to use for http communication.
	 * @param keycloakServerUrl The base url of the auth server. E.g. http://localhost:8080/auth
	 * @param clientApplicationRealm The realm name of the keycloak server that contains the cliet application. E.g. LOA
	 * @param clientApplicationClientId The oauth2 client_id (The client_id is a public identifier for apps). 
	 * @param clientApplicationClientSecret The oauth2 client_secret (The client_secret is a secret known only to the application and the authorization server).
	 * @param clientApplicationUserName The userName to use when accessing the rdf-pub server 
	 * @param clientApplicationPassword The password to use when accessing the rdf-pub server
	 * @param clientApplicationActor The iri/url of the user. TODO determinate this by webfinger ?!
	 */
	public RdfPubClientDefault(WebClient webClient, String keycloakServerUrl, String clientApplicationRealm,
			String clientApplicationClientId, String clientApplicationClientSecret, String clientApplicationUserName, String clientApplicationPassword, IRI clientApplicationActor) {
		this.clientApplicationUserName = clientApplicationUserName;
		this.activityPubClientFactoryHttpClientTimeout = 30;
		this.webClient = webClient;
		this.keycloakServerUrl = keycloakServerUrl;
		this.clientApplicationRealm = clientApplicationRealm;
		this.clientApplicationClientId = clientApplicationClientId;
		this.clientApplicationClientSecret = clientApplicationClientSecret;		
		this.clientApplicationPassword = clientApplicationPassword;
		this.clientApplicationActor = normalize(clientApplicationActor);
	}
	
	/**
	 * Determinates the actorIRI in it's original form with it's userId. E.g. http://localhost:8080/camel/0815
	 * We allow actor iri's with preferredUserNames, but the system (other camel components) expect the 'real' actor with it's userId.
	 * So we replace the preferredUserName by it's userId.
	 * @param actorId With preferredUserName or UserId. E.g. http://localhost:8080/camel/max or http://localhost:8080/camel/0815 
	 * @return The actor in it's original form with it's userId. E.g. http://localhost:8080/camel/0815
	 */
    private IRI normalize(IRI actorId) {
		Model profile = getProfile(actorId).orElseThrow(()->new RuntimeException("no profile for '" + actorId + "'"));
		return (IRI)profile.filter(null,  null, null).stream().findFirst().orElseThrow(()->new RuntimeException("no statement in profile for '" + actorId + "'")).getSubject();
	}

	private String getAuthTokenApp() {
    	return "Bearer " + getTokenNonBlocking(clientApplicationRealm, clientApplicationClientId, clientApplicationClientSecret, clientApplicationUserName, clientApplicationPassword, Collections.emptyList());
    }

    @Override
	public IRI postActivity(Model activity) {
		log.trace("postActivity("+activity+")");
		IRI outbox = getOutboxId(clientApplicationActor);
		log.trace("outbox: " + outbox);		 
		StringWriter bodyAsStringWriter = new StringWriter();
		Rio.write(activity, bodyAsStringWriter, RDFFormat.JSONLD);
		
		String body = bodyAsStringWriter.toString();
		log.trace("body: " + body );
		
		String authToken = getAuthTokenApp();
		
		String activityIriAsString = webClient
		.post()			
		.uri(outbox.stringValue())
		.contentType(MEDIA_TYPE_JSON_LD)
		.body(BodyInserters.fromValue(body))
		.header(HttpHeaders.AUTHORIZATION, authToken)
		.header("profile", "https://www.w3.org/ns/activitystreams")
		.retrieve()
		.onStatus(HttpStatus::isError, ClientResponse::createException)
		.toEntity(String.class)
		.doOnNext(it->log.trace("post response: " + it))
		.flatMap(res->{
			return getLocationHeader(res.getHeaders().get("Location"));
		})
		.doOnNext(location->log.trace("post location: " + location))
		.block();
		return iri(activityIriAsString);
	}

	private IRI getOutboxId(IRI actiorIri) {
		Model profile = getProfile().orElseThrow();		
		Model outboxStatement = profile.filter(null, AS.outbox, null);		
		if(outboxStatement.size()!=1) {
			ModelLogger.error(log, profile, "profile: ");
			throw new RuntimeException("no property "+AS.outbox+" in pofile.");	
		}
		IRI outbox = iri(outboxStatement.stream().findFirst().get().getObject().stringValue());
		log.debug("determined outbox id for '"+actiorIri+"' is " + outbox);
		return outbox;
	}

	private Mono<String> getLocationHeader(List<String> locs) {
		if(locs == null) {
			return Mono.just("");
		}
		return Mono.just(locs)
		.map(List::stream)
		.map(Stream::findFirst)
		.map(optional->optional.orElse(""));
	}

	@Override
	public Optional<Model> getProfile() {
		return getProfile(clientApplicationActor);
	}

	private Optional<Model> getProfile(IRI actorId) {
		log.trace("getProfile("+actorId+")");
		if(profileOptional==null) {
			profileOptional = httpGetApString(actorId);
			if(profileOptional.isPresent()) {
				ModelLogger.trace(log, profileOptional.get(), "profile: ");	
			} else {
				log.warn("no profile for: " + actorId);
			}
		} else {
			log.trace("returning cached profile!");
		}
		return profileOptional;
	}

	@Override
	public Optional<Model> read(IRI idOfTheResourceToRead) {
		log.trace("read(" + idOfTheResourceToRead + ")");
		return httpGetApString(idOfTheResourceToRead, getAuthTokenApp());
	}

	private Optional<Model> httpGetApString(IRI idOfTheResourceToRead) {
		return httpGetApString(idOfTheResourceToRead, null);	
	}
	
	private Optional<Model> httpGetApString(IRI idOfTheResourceToRead, String authToken) {
		
		var request = webClient
				.get()
				.uri(idOfTheResourceToRead.stringValue())
				.accept(MEDIA_TYPE_JSON_LD)
				.header("profile", "https://www.w3.org/ns/activitystreams");
		Optional.ofNullable(authToken).ifPresent(token->request.header(HttpHeaders.AUTHORIZATION, token));
		
		return request 
				.retrieve()
				.onStatus(HttpStatus::isError, ClientResponse::createException)
				.bodyToMono(String.class)
//				.retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
				.map(Optional::ofNullable)
				.map(httpResponseBodyOptional->
					{
						return httpResponseBodyOptional.map(httpResponseBody->{
							try {
								return Rio.parse(new StringReader(httpResponseBody), RDFFormat.JSONLD);
							} catch (RDFParseException | UnsupportedRDFormatException | IOException e1) {
								String message = "Error parsing httpResponseBody. " + e1.getMessage();								
								log.trace(message + ": " + httpResponseBody);								
								throw new RuntimeException(message, e1);
								}
							});
					})
				.block();
	}
	
	
	public String getTokenNonBlocking(String realm, String clientId, String clientSecret, String username, String password, List<String> scopes) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/x-www-form-urlencoded");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("password",password);
		map.add("grant_type","password");
		map.add("username",username);
		map.add("client_id",clientId);
		map.add("client_secret",clientSecret);
		if(!scopes.isEmpty()) {
			map.add("scope",scopes.stream().collect(Collectors.joining(" ")));
		}
		
		String accessTokenUrl = keycloakServerUrl + "/realms/"+realm+"/protocol/openid-connect/token";
		log.trace("getting token for '"+username+"' ("+clientId+") from " + accessTokenUrl);
		JacksonJsonParser jsonParser = new JacksonJsonParser();
		Mono<String> token = webClient
				.post().uri(accessTokenUrl)
				.header("Content-Type", "application/x-www-form-urlencoded")
				.body(BodyInserters.fromFormData(map))
				.retrieve()
				.onStatus(org.springframework.http.HttpStatus::isError, ClientResponse::createException)
				.bodyToMono(String.class)
				.map(jsonParser::parseMap)
				.map(json->json.get("access_token").toString());
		log.trace("got token for " + username); 
		return token.block();
	}

	@Override
	public List<BindingSet> query(IRI sparqlEndpoint, String query) {
		final SPARQLRepository repo = getSparqlRepository(sparqlEndpoint);

		return Flux.just(query)
				.subscribeOn(Schedulers.boundedElastic())
				.flatMap(q -> {
					log.trace("now querying using 'Repositories.tupleQuery(...)'");
					log.trace("query("+sparqlEndpoint+", "+query+")");
					final List<BindingSet> bindingSets = Repositories.tupleQuery(repo, q, r -> QueryResults.asList(r));
					log.trace("querying bindingSets count: {}", bindingSets.size());
					return Flux.fromIterable(bindingSets);
				})
				.retryWhen(Retry.backoff(3, Duration.ofSeconds(2)))
//				.doOnNext(bindingSet -> log.debug("query returning bindingSet with {} bindings. {}", bindingSet.size(), bindingSet.getBindingNames()))
				.onErrorResume(Throwable.class, e -> {
					log.error("Failed to query repository", e);
					return Mono.error(new RuntimeException(e.getClass().getSimpleName() + " while SPARQL querying: " + e.getMessage(), e));
				})
				.collectList()
		        .block();
	}

	@Override
	public SPARQLRepository getSparqlRepository(IRI sparqlEndpoint) {
		log.trace("getSparqlRepository("+sparqlEndpoint+")");
		final SPARQLRepository repo = new SPARQLRepository(sparqlEndpoint.stringValue());
		repo.setHttpClient(getHttpClient());
		return repo;
	}
	
	private HttpClient getHttpClient() {
		Header header = new BasicHeader("Authorization", getAuthTokenApp());
		List<Header> headers = List.of(header);
		int timeout = activityPubClientFactoryHttpClientTimeout * 1000;
		log.trace("timeout: " + activityPubClientFactoryHttpClientTimeout + " s / " + timeout + " ms");
		RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout)
				.setConnectionRequestTimeout(timeout).setSocketTimeout(timeout).build();
		CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config)
				.setDefaultHeaders(headers).build();
		return httpClient;
	}

	@Override
	public IRI getActorId() {
		return clientApplicationActor;
	}
}
