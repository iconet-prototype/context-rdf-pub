package org.linkedopenactors.rdfpub.client;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleOrderedCollection {

	private Model collectionModel;

	public SimpleOrderedCollection(SimpleProfile simpleProfile, IRI collectionId) {
		this.collectionModel = simpleProfile.collection(collectionId);
	}
	
	public Set<IRI> items() {
		return Models.getPropertyIRIs(collectionModel, getOrderedCollectionSubject(), AS.items);	
	}

	private IRI getOrderedCollectionSubject() {
		return collectionModel.filter(null, RDF.TYPE, AS.OrderedCollectionPage).stream().findFirst()
				.map(stmt -> (IRI) stmt.getSubject()).orElseThrow();
	}

	public Integer totalItems() {
		return Integer.parseInt(Models.getPropertyLiteral(collectionModel, getOrderedCollectionSubject(), AS.totalItems)
				.orElseThrow().stringValue());
	}

	public SimpleActivity getActivity(IRI activityIri) {
		return new SimpleActivity(activityIri, collectionModel.filter(activityIri, null, null));
	}
	
	public void trace(String msg) {
		ModelLogger.trace(log, collectionModel, msg);
	}
}
