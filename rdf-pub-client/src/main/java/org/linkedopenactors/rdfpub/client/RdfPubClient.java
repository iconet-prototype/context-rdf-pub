package org.linkedopenactors.rdfpub.client;

import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

/**
 * The interface that describes the convinience methods for accessing a activity-pub client (C2S). 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface RdfPubClient {

	Optional<Model> getProfile();
	IRI postActivity(Model activity);
	Optional<Model> read(IRI idOfTheResourceToRead);
	List<BindingSet> query(IRI sparqlEndpoint, String query);
	IRI getActorId();
	SPARQLRepository getSparqlRepository(IRI sparqlEndpoint);
}
