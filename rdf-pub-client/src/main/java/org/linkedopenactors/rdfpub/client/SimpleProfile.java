package org.linkedopenactors.rdfpub.client;

import java.io.StringWriter;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.ns.rdfpub.RDFPUB;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleProfile {

	private RdfPubClient rdfPubClient;
	private Model profileModel;
	
	public SimpleProfile(RdfPubClient rdfPubClient) {
		this.rdfPubClient = rdfPubClient;
		profileModel = rdfPubClient.getProfile().orElseThrow(()->new RuntimeException("no profile for " + this.rdfPubClient.getActorId()));
	}
	
	public Model outbox() {
		return collection(AS.outbox);
	}
	
	public Repository outboxRepository() {
		return rdfPubClient.getSparqlRepository(getOutboxSparqlEndpoint());
	}
	
	public Model inbox() {
		return collection(AS.inbox);
	}

	public IRI actorId() {
		return this.rdfPubClient.getActorId();
	}
	
	public Model collection(IRI collectionId) {
		IRI collectionIri = Models.getPropertyIRI(profileModel, this.rdfPubClient.getActorId(), collectionId).orElseThrow(()->new RuntimeException("no "+collectionId+" for " + this.rdfPubClient.getActorId()));
		return rdfPubClient.read(collectionIri).orElseThrow(()->new RuntimeException("no "+collectionId+" content for " + collectionIri));		
	}

	public IRI outboxSparqlEndpoint() {
		return Models.getPropertyIRI(profileModel, this.rdfPubClient.getActorId(), RDFPUB.OUTBOX_SPARQL).orElseThrow(()->new RuntimeException("no '" + RDFPUB.OUTBOX_SPARQL + "' for '" + this.rdfPubClient.getActorId() + "'"));
	}
	
	public List<BindingSet> queryOutbox(String query) {
		return rdfPubClient.query(getOutboxSparqlEndpoint(), query);
	}
	
	public Model constructFromOutbox(String query) {
		Repository rep = rdfPubClient.getSparqlRepository(getOutboxSparqlEndpoint());
		Model m = Repositories.graphQuery(rep, query, r -> QueryResults.asModel(r));
		return m;
	}

	private IRI getOutboxSparqlEndpoint() {
		IRI sparqlEndpoint = Models.getPropertyIRI(profileModel, this.rdfPubClient.getActorId(), RDFPUB.OUTBOX_SPARQL).orElseThrow(()->{
			ModelLogger.error(log, profileModel, "profile: ");
			return new RuntimeException("no '" + RDFPUB.OUTBOX_SPARQL + "' for '" + this.rdfPubClient.getActorId() + "'");
			});
		return sparqlEndpoint;
	}
	
	public IRI getActorId() {
		return rdfPubClient.getActorId();
	}
	
	public String toString() {
		StringWriter sw = new StringWriter();
		if (profileModel != null) {
			Rio.write(profileModel, sw, RDFFormat.TURTLE);
		}
		return sw.toString();
	}
	
	public Model asModel() {
		return profileModel;
	}
}
