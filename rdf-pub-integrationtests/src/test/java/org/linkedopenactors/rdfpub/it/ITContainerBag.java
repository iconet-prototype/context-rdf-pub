package org.linkedopenactors.rdfpub.it;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.RDFContainers;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.linkedopenactors.rdfpub.client.SimpleActivity;

import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITContainerBag extends ITBase {

	@Test
	void test() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		TestUser testUser = getRandomTestUser();
		RdfPubClient rdfPubClient = getRdfPubClient(testUser);
		
		IRI containerUrl = iri("http://example.com");
		
		List<IRI> iris = Arrays.asList(new IRI[] { iri("http://example.com/test1"), iri("http://example.com/test2"), iri("http://example.com/test3") });
		Model sampleSequenceModel = RDFContainers.toRDF(RDF.SEQ, iris, containerUrl, new TreeModel());
		sampleSequenceModel.add(containerUrl, RDF.TYPE, AS.Object);
		IRI activity = rdfPubClient.postActivity(sampleSequenceModel);
		
		SimpleActivity sa = rdfPubClient.read(activity).map(activityModel->new SimpleActivity(activity, activityModel)).orElseThrow();
		sa.trace("activty: ");
		
		List<Value> newList = getBag(rdfPubClient, sa.object()).orElseThrow();
		newList.forEach(it->System.out.println("item: " + it));
		
		assertEquals(iris.size(), newList.size());
	}

	private Optional<List<Value>> getBag(RdfPubClient rdfPubClient, IRI id) {
		return rdfPubClient.read(id).map(model->RDFContainers.toValues(RDF.BAG, model, id, new ArrayList<>()));
	}
}
