package org.linkedopenactors.rdfpub.it;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.ns.rdfpub.RDFPUB;
import org.linkedopenactors.rdfpub.client.RdfPubClient;

import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITReadProfile extends ITBase {

	@Test
	void test() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		TestUser testUser = getRandomTestUser();
		RdfPubClient rdfPubClient = getRdfPubClient(testUser);
		Model profile = rdfPubClient.getProfile().orElseThrow(() -> new RuntimeException("no profile for " + rdfPubClient .getActorId()));
		assertEquals(rdfPubClient.getActorId() + "/inbox",  Models.getPropertyIRI(profile, rdfPubClient.getActorId(), AS.inbox).orElseThrow().stringValue());
		assertEquals(rdfPubClient.getActorId() + "/outbox",  Models.getPropertyIRI(profile, rdfPubClient.getActorId(), AS.outbox).orElseThrow().stringValue());
		assertEquals(rdfPubClient.getActorId() + "/outbox/sparql",  Models.getPropertyIRI(profile, rdfPubClient.getActorId(), RDFPUB.OUTBOX_SPARQL).orElseThrow().stringValue());
		assertEquals(testUser.getUserName(),  Models.getPropertyLiteral(profile, rdfPubClient.getActorId(), AS.preferredUsername).orElseThrow().stringValue());
		assertEquals(AS.Person,  Models.getPropertyIRI(profile, rdfPubClient.getActorId(), RDF.TYPE).orElseThrow());
	}
}
