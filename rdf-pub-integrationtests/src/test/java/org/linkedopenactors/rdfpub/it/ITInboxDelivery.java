package org.linkedopenactors.rdfpub.it;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.client.SimpleActivity;
import org.linkedopenactors.rdfpub.client.SimpleOrderedCollection;
import org.linkedopenactors.rdfpub.client.SimpleProfile;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
class ITInboxDelivery extends ITBase {

	@Test
	void testReadInbox() throws Exception {
		TestUser toTestUser = getRandomTestUser();
		IRI actorIri = getMax().getActorId();
		IRI asObjectIri = iri(actorIri.stringValue() + "/object");
		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject(actorIri.stringValue() + "/activity")
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, asObjectIri)
					.add(AS.to, toTestUser.getActorId())
				.subject(asObjectIri)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, nameLiteral)
				.build();
						
		IRI activityIri = rdfPubClient4Max.postActivity(activity);
		assertNotNull(activityIri);
		
		Model activityModel = rdfPubClient4Max.read(activityIri).orElseThrow();
		Models.getPropertyIRI(activityModel, activityIri, AS.object).orElseThrow();
		
		SimpleProfile profile = new SimpleProfile(getRdfPubClient(toTestUser));		
		log.info("testuser: " + toTestUser);
		SimpleOrderedCollection inbox = new SimpleOrderedCollection(profile, AS.inbox);
		assertEquals(1, inbox.totalItems());
		assertEquals(activityIri,inbox.items().stream().findFirst().get()); 
		SimpleActivity simpleActivity = inbox.getActivity(activityIri);
		simpleActivity.trace("SimpleActivity: ");
		assertEquals(AS.Create, simpleActivity.type());
		assertEquals(rdfPubClient4Max.getActorId(), simpleActivity.actor());
		assertTrue(simpleActivity.published().isBefore(Instant.now()));
	}
}
