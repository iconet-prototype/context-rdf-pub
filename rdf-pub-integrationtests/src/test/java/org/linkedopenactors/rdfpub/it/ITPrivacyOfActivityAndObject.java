package org.linkedopenactors.rdfpub.it;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
class ITPrivacyOfActivityAndObject extends ITBase {

	@Test
	void testReadObjectOfOtherUser() throws Exception {
		IRI actorIri = getMax().getActorId();
		IRI asObjectIri = iri(actorIri.stringValue() + "/object");
		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject(actorIri.stringValue() + "/activity")
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, asObjectIri)
				.subject(asObjectIri)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, nameLiteral)
				.build();
						
		IRI activityIri = rdfPubClient4Max.postActivity(activity);
		assertNotNull(activityIri);
		
		Model activityModel = rdfPubClient4Max.read(activityIri).orElseThrow();
		IRI objectIri = Models.getPropertyIRI(activityModel, activityIri, AS.object).orElseThrow();
		
		TestUser randomTestUser = getRandomTestUser();
		RdfPubClient rdfPubClient = getRdfPubClient(randomTestUser);		
		
		WebClientResponseException exception = assertThrows(WebClientResponseException.class, () -> {
			rdfPubClient.read(activityIri);
		    });

		assertEquals("403 Forbidden from GET " + activityIri.stringValue(), exception.getMessage());
		
		exception = assertThrows(WebClientResponseException.class, () -> {
			rdfPubClient.read(objectIri);
		    });

		assertEquals("403 Forbidden from GET " + objectIri.stringValue(), exception.getMessage());
	}
}
