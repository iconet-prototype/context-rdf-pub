package org.linkedopenactors.rdfpub.it;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.client.SimpleProfile;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
class ITReadCollectionOutbox extends ITBase {

	@Test
	void testWithCreateActivity() throws Exception {
		IRI asObjectIri = iri("http://example.com/someObject");
		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject("http://example.com/someActivity")
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, asObjectIri)
				.subject(asObjectIri)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, nameLiteral)
				.build();
		
		IRI activityIri = rdfPubClient4Max.postActivity( activity);
		assertNotNull(activityIri);
		
		SimpleProfile profile = new SimpleProfile(rdfPubClient4Max);
		
		Model outbox = profile.outbox();
		
		Optional<IRI> objectIriOptional = Models.getPropertyIRI(outbox, activityIri, AS.object);
		assertTrue(objectIriOptional.isPresent());
	}
	
	@Test
	void testWithoutActivity() throws Exception {
		IRI asObjectIri = iri("http://example.com/someObject");
		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject(asObjectIri)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, nameLiteral)
				.build();
		
		IRI activityIri = rdfPubClient4Max.postActivity( activity);
		assertNotNull(activityIri);
		
		SimpleProfile profile = new SimpleProfile(rdfPubClient4Max);
		
		Model outbox = profile.outbox();
		
		Optional<IRI> objectIriOptional = Models.getPropertyIRI(outbox, activityIri, AS.object);
		assertTrue(objectIriOptional.isPresent());
	}
}
