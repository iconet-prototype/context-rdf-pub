package org.linkedopenactors.rdfpub.it;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
class ITSendCreateActivityAndReadIt extends ITBase {

	@Test
	void test() throws Exception {
		IRI asObjectIri = iri("http://example.com/someObject");
		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject("http://example.com/someActivity")
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, asObjectIri)
				.subject(asObjectIri)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, nameLiteral)
				.build();
		
		IRI activityIri = rdfPubClient4Max.postActivity( activity);
		assertNotNull(activityIri);
		
		Model reReadActivity = rdfPubClient4Max.read(activityIri).orElseThrow();
		IRI objectIri = Models.getPropertyIRI(reReadActivity, activityIri, AS.object).orElseThrow();
		reReadActivity.addAll(rdfPubClient4Max.read(objectIri).orElseThrow());

		ModelLogger.debug(log, reReadActivity, "actReRead: ");
		
		assertEquals(rdfPubClient4Max.getActorId(), Models.getPropertyIRI(reReadActivity, activityIri, AS.actor).orElseThrow());
		assertTrue(Models.getPropertyLiteral(reReadActivity, activityIri, AS.published).isPresent());
		assertEquals(nameLiteral, Models.getPropertyLiteral(reReadActivity, objectIri, SCHEMA_ORG.name).orElseThrow());
		assertEquals(rdfPubClient4Max.getActorId(), Models.getPropertyIRI(reReadActivity, objectIri, AS.attributedTo).orElseThrow());
		assertTrue(Models.getPropertyLiteral(reReadActivity, objectIri, AS.published).isPresent());
	}
}
