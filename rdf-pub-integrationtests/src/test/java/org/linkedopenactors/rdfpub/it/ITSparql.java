package org.linkedopenactors.rdfpub.it;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.linkedopenactors.rdfpub.client.SimpleProfile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITSparql extends ITBase {

	@Test
	void test() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		TestUser testUser = getRandomTestUser();
		RdfPubClient rdfPubClient = getRdfPubClient(testUser);
		SimpleProfile profile = new SimpleProfile(rdfPubClient);
		Model profileModel = profile.asModel();
		Repository repository = profile.outboxRepository();
		Model profileFromRepositoryViaSparql = new ModelBuilder().build();
		List<Statement> stmts;
		try(RepositoryConnection con = repository.getConnection()) {
			stmts = Iterations.asList(con.getStatements(profile.getActorId(), null, null));			
		}
		profileFromRepositoryViaSparql.addAll(stmts);
		
		profileModel.forEach(stmt->{
			assertEquals(stmt.getObject(),  
					Models.getProperty(profileFromRepositoryViaSparql, stmt.getSubject(), stmt.getPredicate()).orElseThrow());
		});
	}
}
