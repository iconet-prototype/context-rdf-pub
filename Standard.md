# Standard decisions made during development:

Gather all decisions made during development here, 
which are relevant for communication and 
interconnectivity across networks. 

For this prototype, first make pragmatic decisions,
rely on your instincts and developer preferences may play a role. 
The development of the final standard recommendation is a process, detached from this development.

### 1. Package Transmission:

#### 1.1 Global Addresses

#### 1.2 Encryption & Authentication

#### 1.3 Package & Request Syntax

### 2.  Embedded Experience:

#### 2.1 Mark-Up

#### 2.2 Interaction transmission