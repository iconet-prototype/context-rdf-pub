<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.linkedopenactors.ap</groupId>
		<artifactId>rdf-pub</artifactId>
	    <version>0.0.4-SNAPSHOT</version>
	</parent>
	<artifactId>rdf-pub-core</artifactId>
	<name>${project.artifactId}</name>
	<description>The core of the rdf-pub server containing the activity-pub usecases.</description>
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.apache.camel.springboot</groupId>
				<artifactId>camel-spring-boot-dependencies</artifactId>
				<version>3.13.0</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>rdf-pub-client</artifactId>
			<version>${project.version}</version>
		</dependency>
		
		<!-- Spring Boot -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-tomcat</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.logging.log4j</groupId>
					<artifactId>log4j-to-slf4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-oauth2-resource-server</artifactId>
 		</dependency>
		<dependency>
			<!-- https://springhow.com/spring-boot-undertow/ 
				Undertow is the new embeddable web server written in Java. It has great HTTP2
				support, it provides blocking and not blocking APIs based in Java NiO. And 
				most importantly, the undertow is lightweight. -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-undertow</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
		</dependency>

  		<dependency>
			<groupId>org.keycloak</groupId>
			<artifactId>keycloak-admin-client</artifactId>
		</dependency>

		<!-- Camel -->
		<dependency>
			<groupId>org.apache.camel.springboot</groupId>
			<artifactId>camel-spring-boot-starter</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.camel.springboot</groupId>
			<artifactId>camel-servlet-starter</artifactId>
		</dependency>		
		<dependency>
			<groupId>org.apache.camel.springboot</groupId>
			<artifactId>camel-stream-starter</artifactId>
		</dependency>


    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-sleuth</artifactId>
    </dependency>

		<!-- Test -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.camel</groupId>
			<artifactId>camel-test-spring-junit5</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>		
		</dependency>
		
		<!-- rdf4j -->
		<dependency>
			<groupId>org.eclipse.rdf4j</groupId>
			<artifactId>rdf4j-model</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.rdf4j</groupId>
			<artifactId>rdf4j-rio-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.rdf4j</groupId>
			<artifactId>rdf4j-rio-turtle</artifactId>
		</dependency>

		<!-- Others -->
		<dependency>
			<groupId>org.linkedopenactors.ap</groupId>
			<artifactId>rdf-pub-repository</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>de.naturzukunft.rdf4j</groupId>
			<artifactId>vocabulary</artifactId>
		</dependency>
		<dependency>
			<groupId>de.naturzukunft.rdf4j</groupId>
			<artifactId>rdf4j-utils</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
	</dependencies>

	<build>
		<finalName>app</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>**/application*.yml</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>**/application*.yml</exclude>
                </excludes>
                <includes>
                    <include>**/*.*</include>
                </includes>
            </resource>
        </resources>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>repackage</id>
						<goals>
							<goal>repackage</goal>							
						</goals>
					</execution>
					<execution>
						<id>build-info</id>
						<goals>
							<goal>build-info</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
			</plugin>
	 		<plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <executions>
                    <execution>
                        <id>get-the-git-infos</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                        <phase>initialize</phase>
                    </execution>
                </executions>
                <configuration>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                    <generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
                    <includeOnlyProperties>
                        <includeOnlyProperty>^git.build.(time|version)$</includeOnlyProperty>
                        <includeOnlyProperty>^git.commit.id.(abbrev|full)$</includeOnlyProperty>
                    </includeOnlyProperties>
                    <commitIdGenerationMode>full</commitIdGenerationMode>
                </configuration>
            </plugin>
		</plugins>
	</build>
</project>
