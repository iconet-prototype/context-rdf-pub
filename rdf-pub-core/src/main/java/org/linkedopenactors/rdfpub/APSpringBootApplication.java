package org.linkedopenactors.rdfpub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;

@SpringBootApplication(scanBasePackages = {
		"org.linkedopenactors.rdfpub.repository", 
		"org.linkedopenactors.rdfpub"
}, exclude = { SolrAutoConfiguration.class })
public class APSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(APSpringBootApplication.class, args);
    }

}
