package org.linkedopenactors.rdfpub.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.processor.DefaultExchangeFormatter;
import org.springframework.stereotype.Component;

/**
 * Configuration of camel specific stuff. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
public class CamelRouteConfig extends RouteBuilder {

    @Override
    public void configure() {
    	getCamelContext().setLogMask(true);
    	if( getCamelContext().getTracer().getExchangeFormatter() instanceof DefaultExchangeFormatter ) {
    		DefaultExchangeFormatter def = (DefaultExchangeFormatter)getCamelContext().getTracer().getExchangeFormatter();
            def.setShowHeaders(true);
        }
    	restConfiguration().component("servlet");
    }
}
