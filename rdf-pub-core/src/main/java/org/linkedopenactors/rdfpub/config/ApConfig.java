package org.linkedopenactors.rdfpub.config;

import org.linkedopenactors.rdfpub.identity.ApKeycloakProperties;
import org.linkedopenactors.rdfpub.identity.CurrentUser;
import org.linkedopenactors.rdfpub.identity.CurrentUserSecurityContextHolder;
import org.linkedopenactors.rdfpub.identity.IdentityProviderDefault;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluator;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;

/**
 * Definition of some common beans. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Configuration
public class ApConfig {

	@Autowired
	private ApKeycloakProperties apKeycloakProperties;
	
	public static final String PROFILE_UNIT_TEST = "unittest";
	
	@Bean
	public SparqlQueryEvaluator getSparqlQueryEvaluator() {
		return new SparqlQueryEvaluatorRdf4j();
	}
		
	@Bean
	@ConditionalOnProperty(name="app.unittest", havingValue="false")	   
	public CurrentUser getCurrentUser() {
		return new CurrentUserSecurityContextHolder();
	}
	
	@Bean
	@ConditionalOnProperty(name="app.unittest", havingValue="false")	   
	public IdentityProvider getIdentityProvider() {
			return new IdentityProviderDefault(apKeycloakProperties);
	}
}
