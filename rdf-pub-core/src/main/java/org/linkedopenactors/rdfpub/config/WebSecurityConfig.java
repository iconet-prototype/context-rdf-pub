package org.linkedopenactors.rdfpub.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring security configuration. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Configuration
//@EnableWebSecurity(debug = true) // TODO REMOVE !!!
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        
        http
 
        	.authorizeRequests(authz -> authz
        			.antMatchers("/actuator/env").hasRole("admin")
        			.antMatchers("/actuator/**", "/v3/api-docs/**", "/swagger-resources/**", "/swagger-ui/**",
        					"/swagger-ui/index.html**", "/swagger-ui.html", "/webjars/**", "/camel/*",
        					"/.well-known/webfinger", "/error").permitAll()
        			.antMatchers("/camel/*/**").authenticated()
        			.anyRequest().authenticated()
        			)
        	.oauth2ResourceServer(oauth2 -> oauth2.jwt());
    }
}