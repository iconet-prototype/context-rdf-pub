package org.linkedopenactors.rdfpub.identity;

import java.util.Optional;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import lombok.extern.slf4j.Slf4j;

/**
 * Data of the current logged in user, extracted outr of the {@link SecurityContextHolder}
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class CurrentUserSecurityContextHolder implements CurrentUser{

	@Override
	public Optional<String> getPreferedUserName() {
		return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
				.map(auth -> {
					if (auth.getPrincipal() instanceof Jwt) {
						String claim = ((Jwt) auth.getPrincipal()).getClaim("preferred_username");
						return claim;
						} else {
							log.warn("auth.getPrincipal() is not Jwt, but " + auth.getPrincipal().getClass().getName());
							return null;
							}
					});
	}

	@Override
	public Optional<String> getUserId() {
		return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
				.map(org.springframework.security.core.Authentication::getName);
	}
}
