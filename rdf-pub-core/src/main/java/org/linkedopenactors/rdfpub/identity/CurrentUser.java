package org.linkedopenactors.rdfpub.identity;

import java.util.Optional;

/**
 * Data of the current logged in user.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface CurrentUser {
	Optional<String> getPreferedUserName();
	Optional<String> getUserId();
}
