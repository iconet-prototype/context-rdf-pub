package org.linkedopenactors.rdfpub.identity;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.NotFoundException;

import org.eclipse.rdf4j.model.IRI;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.linkedopenactors.rdfpub.repository.IdentityProviderUser;
import org.slf4j.Logger;
import org.slf4j.event.Level;

import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of {@link IdentityProvider} that is used as dependency inversion interface within the repository.  
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class IdentityProviderDefault implements IdentityProvider {

	private ApKeycloakProperties apKeycloakProperties;
	private Keycloak keycloak;
	
	public IdentityProviderDefault(ApKeycloakProperties apKeycloakProperties) {
		this.apKeycloakProperties = apKeycloakProperties;
	}
	
	@Override
	public List<IdentityProviderUser> search(IRI actorId) {
		return search(actorId.getLocalName());
	}
	
	@Override
	public List<IdentityProviderUser> search(String actorName) {
		if(hasUpperCase(actorName)) {
			throw new IllegalStateException("actorName '" +actorName+"' is invalid, because it contains upper case letters.");
		}
		logIdentityProviderProperties(log, Level.TRACE, "search for "+actorName+" using: ");
		UsersResource users = buildKeycloakClient().realm(apKeycloakProperties.getRealm()).users();		
		List<UserRepresentation> userRepresentations = users.search(actorName);
		log.trace("search by name["+actorName+"]: ("+userRepresentations.size()+") " + userRepresentations.stream().map(ur->ur.getId()).collect(Collectors.joining( "," )));
		if(userRepresentations.isEmpty()) {
			try {				
				userRepresentations = List.of(users.get(actorName).toRepresentation());
				log.trace("search by id["+actorName+"]: ("+userRepresentations.size()+") " + userRepresentations.stream().map(ur->ur.getId()).collect(Collectors.joining( "," )));
			} catch (NotFoundException e) {
				log.warn("search by id["+actorName+"] -> NOT found!");
				userRepresentations = Collections.emptyList();
			}
		}
		List<IdentityProviderUser> foundUsers = userRepresentations.stream()
				.map(ur -> new IdentityProviderUser(ur.getId(), ur.getUsername())).collect(Collectors.toList());
		log.trace("foundUsers: " + foundUsers);
		return foundUsers; 
	}

    private boolean hasUpperCase(String actorName) {
    	    char currentCharacter;
    	    boolean upperCasePresent = false;
			for (int i = 0; i < actorName.length(); i++) {
				currentCharacter = actorName.charAt(i);
				if (Character.isUpperCase(currentCharacter)) {
					upperCasePresent = true;
				}
			}
    	return upperCasePresent;
	}

	private Keycloak buildKeycloakClient() {
    	if(keycloak==null) {
	        keycloak = KeycloakBuilder.builder()
	                .serverUrl(apKeycloakProperties.getKeycloakServerUrl())
	                .realm(apKeycloakProperties.getRealm())
	                .clientId(apKeycloakProperties.getClientid())
	                .username(apKeycloakProperties.getAdminUser())
	                .password(apKeycloakProperties.getAdminPwd())
	                .build();
	        keycloak.tokenManager().getAccessToken(); // test access
    	} 
		return keycloak;
    }

	@Override
	public void logIdentityProviderProperties(Logger logger, Level level) {
		logIdentityProviderProperties(logger, level, "logIdentityProviderProperties");
	}

	@Override
	public void logIdentityProviderProperties(Logger logger, Level level, String msgParam) {
		String msg = msgParam + " - " + apKeycloakProperties.toString();
		switch (level) {
		case INFO:
			logger.info(msg);			
			break;

		case DEBUG:
			logger.debug(msg);			
			break;

		case TRACE:
			logger.trace(msg);			
			break;

		case WARN:
			logger.warn(msg);			
			break;

		case ERROR:
			logger.error(msg);			
			break;

		default:
			logger.trace(msg);
			break;
		}
	}

	@Override
	public IRI normalize(IRI actorId) {
		logIdentityProviderProperties(log, Level.DEBUG, "toActorWithUserId search("+actorId+")");
		List<IdentityProviderUser> matchingUsers = search(actorId).stream()
				.filter(user -> {
					boolean containsPreferredUserName = actorId.stringValue().contains(user.getPreferredUserName());
					boolean containsUserId = actorId.stringValue().contains(user.getUserId());
					log.trace(actorId.stringValue() +".contains("+user.getPreferredUserName()+"): " + containsPreferredUserName);
					log.trace(actorId.stringValue() +".contains("+user.getUserId()+"): " + containsUserId);
					return containsPreferredUserName || containsUserId;
					})
				.collect(Collectors.toList());
		log.trace("matchingUsers.size(): " + matchingUsers.size());
		if(matchingUsers.size()!=1) {
			throw new RuntimeException("IdentityProvider did not find exact one user for actor iri '"+actorId+"' but :" + matchingUsers);
		}
		String userId = matchingUsers.get(0).getUserId();
		log.trace("repositoryId: " + userId + " ("+matchingUsers.get(0).getUserId()+")");
		return iri(actorId.getNamespace() + userId);
	}

	@Override
	public Optional<IdentityProviderUser> getUser(IRI actorId) {
		List<IdentityProviderUser> users = search(actorId);
		if(users.size()>1) {
			throw new RuntimeException("more than one user for " + actorId);
		} else {
			return users.stream().findFirst();
		}
	}
}
