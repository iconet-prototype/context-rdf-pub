/**
 * Abstraction of the underlaying identity profider, e.g. keycloak 
 */
package org.linkedopenactors.rdfpub.identity;
