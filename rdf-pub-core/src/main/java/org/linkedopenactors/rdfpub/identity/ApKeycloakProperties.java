package org.linkedopenactors.rdfpub.identity;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
@Component
@Validated
@ConfigurationProperties(prefix = "keycloak", ignoreUnknownFields = false)
public class ApKeycloakProperties {

	@NotBlank
    private String keycloakServerUrl;
	
    @NotBlank
    private String realm;

    @NotBlank
    private String clientid;

    @NotBlank
    private String adminUser;

    @NotBlank
    private String adminPwd;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();		
		sb.append("keycloakServerUrl: ").append(keycloakServerUrl).append("; ");
		sb.append("realm: ").append(realm).append("; ");
		sb.append("clientid: ").append(clientid).append("; ");
		sb.append("adminUser: ").append(adminUser).append("; ");
		return sb.toString();
	}
}
