package org.linkedopenactors.rdfpub;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.linkedopenactors.rdfpub.repository.IdentityProviderUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Webfinger {

	@Value( "${app.context}" )
	private String context;

	@Autowired
	private IdentityProvider identityProvider;

	@GetMapping("/.well-known/webfinger")
	public ResponseEntity<String> webfinger(HttpServletRequest request, @RequestParam(value = "resource") String resource) {
		
		String user = removeAcct(resource);
		user = removeNamespace(user);
		log.debug("webfinger requested: " + resource + " resolved: " + user);
		
		Optional<String> userOpt = getUserId(user)
									.map(userId -> getBaseUrl() + userId)
									.map(Values::iri)
									.map(actorId -> getWebFingerProfile(actorId, resource));
		ResponseEntity<String> responseEntity;
		if(userOpt.isEmpty()) {
			responseEntity = ResponseEntity.notFound().build();
		} else {
			responseEntity = ResponseEntity.ok().header(HttpHeader.CONTENT_TYPE.asString(), "application/jrd+json; charset=utf-8").body(userOpt.get());
		}
		return responseEntity;
	}
	
	private String getBaseUrl() {
		return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + context;
	}

	private Optional<String> getUserId(String resource) {
		Optional<String> userIdOptional = Optional.empty();
		List<IdentityProviderUser> users = identityProvider.search(resource);
		users.forEach(it->System.out.println("resource: '"+resource+"' -> user: " + it));
		
		users = users.stream().filter(name->resource.equals(name.getPreferredUserName())).collect(Collectors.toList());
		
		if(users.size()==1) {			
			userIdOptional = Optional.of(users.get(0).getUserId());
		}
		return userIdOptional;
	}
	
	/**
	 * Creates the passed profile url and the requested resource in a webfinger json response string.
	 * @param actorId The profile url
	 * @param userId the requested resource
	 * @return A webfinger json response string.
	 */
	private String getWebFingerProfile(IRI actorId, String userId)  {
		log.debug("creating webfinger profile for: " + actorId.stringValue() + " ("+userId+")");
		String jsonTemplate = "{\n"
				+ "	\"subject\": \"${resource}\",\n"
				+ "\n"
				+ "	\"links\": [\n"
				+ "		{\n"
				+ "			\"rel\": \"self\",\n"
				+ "			\"type\": \"application/activity+json\",\n"
				+ "			\"href\": \"${iri}\"\n"
				+ "		}\n"
				+ "	]\n"
				+ "}\n";
		
		jsonTemplate = jsonTemplate.replace("${resource}", userId);
		return jsonTemplate.replace("${iri}", actorId.stringValue());
	}
	
	/**
	 * The requested resource can be prefixed with 'acct:' we ignore that.
	 * @param actorResource
	 * @return the 'cleaned' actorResource
	 */
	private String removeAcct(String actorResource) {
		if (actorResource.startsWith("acct:")) {
			actorResource = actorResource.substring(5);
		}
		return actorResource;
	}

	/**
	 * We accept ids with namespace e.g. @rdfpub.test.opensourceecology.de but ignoring it. 
	 * @param resource
	 * @return the 'cleaned' actorResource
	 */
	private String removeNamespace(String resource) {
		String manipulated = resource;
		if(resource.contains("@")) {
			if(StringUtils.split(resource, "@").length>1) {
				manipulated = resource.substring(0, resource.lastIndexOf("@"));
			}
			log.trace("removeNamespace("+resource+") -> " + manipulated);
		}
		return manipulated;		
	}
}
