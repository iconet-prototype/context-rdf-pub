/**
 * Tools that make life easier, but are not necessarily directly relevant to the project.
 */
package org.linkedopenactors.rdfpub.tools;
