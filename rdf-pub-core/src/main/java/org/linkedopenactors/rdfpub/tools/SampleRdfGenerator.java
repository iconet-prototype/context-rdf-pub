package org.linkedopenactors.rdfpub.tools;

import static org.eclipse.rdf4j.model.util.Values.*;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

/**
 * Sometimes we need e.g. a turtle file of some objects. here we can build the object with java/rdf4j and generate a turtle file. Works also with json-ld, etc. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public class SampleRdfGenerator {

	public static void main(String[] args) throws RDFParseException, UnsupportedRDFormatException, IOException {
		SampleRdfGenerator s = new SampleRdfGenerator();
		s.run();
//		s.sample();
//		s.run2();
	}
	
	public void run() {
		Model model = new ModelBuilder()
				.subject("http://localhost:8080/actor1/objectA")
					.add(RDF.TYPE, AS.Object)
					.add(RDF.TYPE, SCHEMA_ORG.Thing)
					.add(SCHEMA_ORG.nextItem, iri("http://localhost:8080/actor1/v2_objectA"))
					.add(AS.name, literal("Max"))
				.add(SCHEMA_ORG.version, Values.literal("1"))
				.subject("http://localhost:8080/actor1/v2_objectA")
					.add(RDF.TYPE, AS.Object)
					.add(RDF.TYPE, SCHEMA_ORG.Thing)
					.add(SCHEMA_ORG.previousItem, iri("http://localhost:8080/actor1/objectA"))
					.add(AS.name, literal("Anton"))
					.add(SCHEMA_ORG.version, Values.literal("2"))
				.build();
		Rio.write(model, System.out, RDFFormat.TURTLE);
	}
	
	

	public void run2() {
		Model model = new ModelBuilder()
				.subject("http://localhost:8090/kvm_loa/0815")
					.add(RDF.TYPE, AS.Create)
					.add(AS.actor, iri("http://localhost:8090/kvm_loa/Sally"))
					.add(AS.object, iri("http://localhost:8090/kvm_loa/V0_9d317daca74246d4be41b1a37e30ee2a"))
					.add(AS.summary, literal("Sally created a note"))
				.subject("http://localhost:8090/kvm_loa/Sally")
					.add(RDF.TYPE, AS.Person )
					.add(AS.name, literal("Sally created a note"))
				.build();
		Rio.write(model, System.out, RDFFormat.TURTLE);
	}
	
//	
//	@prefix as: <https://www.w3.org/ns/activitystreams#> .
//		@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
//
//		<http://localhost:8090/kvm_loa/0815>
//		  a as:Create ;
//		  as:actor <http://localhost:8090/kvm_loa/Sally> ;
//		  as:object <http://localhost:8090/kvm_loa/V0_9d317daca74246d4be41b1a37e30ee2a> ;
//		  as:summary "Sally created a note"^^xsd:string .
//
//
//		<http://localhost:8090/kvm_loa/Sally>
//			a as:Person ;
//			as:name "Sally" .
//	
	
	public void sample() throws RDFParseException, UnsupportedRDFormatException, IOException {
		String jsonLdIn = "{\n"
				+ "  \"@context\": \"https://www.w3.org/ns/activitystreams\",\n"
				+ "  \"type\": \"Activity\",\n"
				+ "  \"summary\": \"Sally did something to a note\",\n"
				+ "  \"actor\": {\n"
				+ "    \"type\": \"Person\",\n"
				+ "    \"name\": \"Sally\"\n"
				+ "  },\n"
				+ "  \"object\": {\n"
				+ "    \"type\": \"Note\",\n"
				+ "    \"name\": \"A Note\"\n"
				+ "  }\n"
				+ "}";
				
				Model m = Rio.parse(new StringReader(jsonLdIn), RDFFormat.JSONLD);
				Rio.write(m, System.out, RDFFormat.JSONLD);
	}
}
