package org.linkedopenactors.rdfpub.camel;

import java.util.Optional;

import org.apache.camel.Message;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;

/**
 * Kind of wrapper arround a {@link Message} that provide some convenience methods. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface ApMessage extends Message {

	/**
	 * @return The http accept header of the current request.
	 */
	Optional<String> getAcceptHeaderOpt();
	
	/**
	 * @return The content type of the current request.
	 * @throws RuntimeException if the property is not available!
	 */
	String getContentType();
	
	/**
	 * @return The id of the actor that is the base of {@link #getRequestedUrl()}. Can be different from the current logged in user!
	 */
	IRI getActorId();

	/**
	 * @return The url of the current request. E.g. http://localhost:8080/max/inbox
	 */
	IRI getRequestedUrl();

	/**
	 * @return The model of the current request. Build from the message body.
	 */
	Model getModel();

	/**
	 * @return The content type of the current request.
	 */
	Optional<String> getContentTypeOpt();
	
	/**
	 * @return same as {@link #getCollection()}, but without Exception.
	 */
	Optional<String> getCollectionOpt();
	
	/**
	 * @return The name of the collection that is requested.
	 * @throws RuntimeException if the property is not available!
	 */
	String getCollection();
	
	/**
	 * @return The id of the activity that is currently processed.
	 * @throws RuntimeException if the property is not available! 
	 */
	IRI getActivityId();
	
	/**
	 * @return The userId of the current logged in user. This id is provided by the identity provider !
	 * @throws RuntimeException if the property is not available!
	 */
	String getCurrentUserId();

	/**
	 * @return The preferred username of the current logged in user. This preferred username is provided by the identity provider !
	 * @throws RuntimeException if the property is not available!
	 */
	String getCurrentPreferedUserName();
	
	/**
	 * @return an {@link IRI} build from the namespace of {@link #getActorId()} and {@link #getCurrentUserId()}.
	 * @throws RuntimeException if the property is not available!
	 */
	IRI getCurrentUserIdIri();

	/**
	 * @return an {@link IRI} build from the namespace of {@link #getActorId()} and {@link #getCurrentPreferedUserName()}.
	 * @throws RuntimeException if the property is not available!
	 */
	IRI getCurrentPreferedUserNameIri();
}