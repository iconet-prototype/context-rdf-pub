package org.linkedopenactors.rdfpub.camel;

/**
 * Collection of constants for properties that can be stored in a message header. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface ActivityMessageHeaders {
	
	/**
	 * The id of the activity in the message body.
	 */
	public static final String ACTIVITY_IRI = "activityIri";
	
	/**
	 * A flag that indicates if the activity was successfully saved.
	 */
	public static final String ACTIVITY_SAVED = "activitySaved";

	/**
	 * The type of the activity in the body of the message. E.g. https://www.w3.org/ns/activitystreams#Create 
	 */
	public static final String ACTIVITY_TYPE_IRI = "activityTypeIri";
	
	/**
	 * The id of the actor for which a request was sent. This does not necessarily
	 * have to be the currently logged-in user, since a profile can also be accessed
	 * by an anonymous user, for example.
	 */
	public static final String ACTOR_IRI = "actorIri";
	
	/**
	 * The location of a created object
	 */
	public static final String LOCATION = "Location";
	
	/**
	 * The original url, that was called from a client.
	 */
	public static final String REQUESTED_URL = "CamelHttpUrl";
	
	/**
	 * Requested collection name, such as inbox, outbox, etc.
	 */
	public static final String COLLECTION = "collection";
	
	/**
	 * The preferred username of the current logged in user (provided from identity provider)
	 */
	public static final String CURRENT_PREFERED_USERNAME = "currentPreferedUserName";

	/**
	 * The userId of the current logged in user (provided from identity provider)
	 */
	public static final String CURRENT_USERID = "currentUserId";
}
