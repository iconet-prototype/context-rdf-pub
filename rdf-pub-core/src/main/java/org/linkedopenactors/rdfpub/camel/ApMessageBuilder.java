package org.linkedopenactors.rdfpub.camel;

import java.io.StringWriter;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.support.DefaultMessage;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.springframework.http.HttpHeaders;

/**
 * A convenient way to create {@link Message}.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public class ApMessageBuilder {

	private DefaultMessage defaultMessage;
	
	public ApMessageBuilder(Exchange exchange) {
		this(exchange.getContext());
	}
	
	public ApMessageBuilder(CamelContext camelContext) {
		defaultMessage = new DefaultMessage(camelContext);
	}

	public static ApMessageBuilder getInstance(Exchange exchange) {
		ApMessageBuilder apMessageBuilder = new ApMessageBuilder(exchange);
		apMessageBuilder.headers(exchange.getIn().getHeaders());
		return apMessageBuilder;
	}
	
	public static ApMessageBuilder getInstance(CamelContext camelContext) {
		return new ApMessageBuilder(camelContext);
	}

	public ApMessageBuilder actorIri(IRI actorIri) {
		defaultMessage.setHeader(ActivityMessageHeaders.ACTOR_IRI, actorIri);
		return this;
	}

	public ApMessageBuilder currentPreferedUserName(String currentPreferedUserName) {
		defaultMessage.setHeader(ActivityMessageHeaders.CURRENT_PREFERED_USERNAME, currentPreferedUserName);
		return this;
	}

	public ApMessageBuilder currentUserId(String currentUserId) {
		defaultMessage.setHeader(ActivityMessageHeaders.CURRENT_USERID, currentUserId);
		return this;
	}

	public ApMessageBuilder activityIri(IRI activityIri) {
		defaultMessage.setHeader(ActivityMessageHeaders.ACTIVITY_IRI, activityIri);
		return this;
	}

	public ApMessageBuilder headers(Map<String, Object> headers) {
		defaultMessage.setHeaders(headers);
		return this;
	}

	public ApMessageBuilder contentType(String contentType) {
		defaultMessage.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
		return this;
	}
	
	public ApMessageBuilder body(String body) {
		defaultMessage.setBody(body);
		return this;
	}

	public ApMessageBuilder body(StringWriter body) {
		defaultMessage.setBody(body.toString());
		return this;
	}

	public ApMessageBuilder body(Model model) {
		StringWriter modelTurtleWriter = new StringWriter();
		Rio.write(model, modelTurtleWriter, RDFFormat.TURTLE);
		defaultMessage.setBody(modelTurtleWriter.toString());
		return this;
	}

	public Message build() {
		return defaultMessage;
	}

	public ApMessageBuilder activityTypeIri(IRI activityType) {
		defaultMessage.setHeader(ActivityMessageHeaders.ACTIVITY_TYPE_IRI, activityType);
		return this;
	}

	public ApMessageBuilder activitySaved() {
		defaultMessage.setHeader(ActivityMessageHeaders.ACTIVITY_SAVED, true);
		return this;
	}
	
	public ApMessageBuilder location(IRI location) {
		defaultMessage.setHeader(ActivityMessageHeaders.LOCATION, location);
		return this;
	}
	
	public ApMessageBuilder requestedUrl(IRI requestedUrl) {
		defaultMessage.setHeader(ActivityMessageHeaders.REQUESTED_URL, requestedUrl);
		return this;
	}
	
	public ApMessageBuilder collection(String collection) {
		defaultMessage.setHeader(ActivityMessageHeaders.COLLECTION, collection);
		return this;
	}
}
