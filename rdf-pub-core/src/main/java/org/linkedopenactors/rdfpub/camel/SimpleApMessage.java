package org.linkedopenactors.rdfpub.camel;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.support.DefaultMessage;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.springframework.http.HttpHeaders;

import lombok.extern.slf4j.Slf4j;

/**
 * Default implementation of {@link ApMessage}. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class SimpleApMessage extends DefaultMessage implements ApMessage {
	
	/**
	 * Constructs a instance of this class.
	 * @param message {@link Exchange#getMessage()} or {@link Exchange#getIn()}
	 */
	public SimpleApMessage(Message message) {
		super(message.getExchange());
		this.setHeaders(message.getHeaders());
		this.setBody(message.getBody());
	}
	
	@Override
	public Optional<String> getAcceptHeaderOpt() {		
		return getProperty(HttpHeaders.ACCEPT, String.class);
	}

	@Override
	public IRI getActorId() {		
		return getMandatoryIriProperty(ActivityMessageHeaders.ACTOR_IRI);
	}

	@Override
	public String getCollection() {		
		return getMandatoryProperty(ActivityMessageHeaders.COLLECTION, String.class);
	}

	@Override
	public Optional<String> getCollectionOpt() {		
		return getProperty(ActivityMessageHeaders.COLLECTION, String.class);
	}

	private <T> Optional<T> getProperty(String name, Class<T> type) {
		return Optional.ofNullable(getHeader(name, type));
	}

	private <T> T getMandatoryProperty(String name, Class<T> type) {
		return Optional.ofNullable(getHeader(name, type)).orElseThrow(()->new RuntimeException("Message did not define header '" + name + "'. headers: " + getHeaders()));
	}

	private IRI getMandatoryIriProperty(String name) {
		return getMandatoryProperty(name, IRI.class);
	}

	@Override
	public Model getModel() {
		String messageBody = getBody(String.class);
		if(messageBody==null) {
			throw new RuntimeException("message is NULL");
		}
		StringReader sr = new StringReader(messageBody);
		try {
			return Rio.parse(sr, RDFFormat.TURTLE);
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			log.error(messageBody);
			throw new RuntimeException("error parsing message body into Turtle RDF Model", e);
		}
	}

	@Override
	public String getContentType() {
		return getMandatoryProperty(HttpHeaders.CONTENT_TYPE, String.class);
	}

	@Override
	public IRI getRequestedUrl() {
		return iri(getMandatoryProperty(ActivityMessageHeaders.REQUESTED_URL, String.class));
	}

	@Override
	public Optional<String> getContentTypeOpt() {
		return getProperty(HttpHeaders.CONTENT_TYPE, String.class);
	}


	@Override
	public IRI getActivityId() {
		return getMandatoryProperty(ActivityMessageHeaders.ACTIVITY_IRI, IRI.class);
	}


	@Override
	public String getCurrentUserId() {
		return getMandatoryProperty(ActivityMessageHeaders.CURRENT_USERID, String.class);
	}


	@Override
	public String getCurrentPreferedUserName() {
		return getMandatoryProperty(ActivityMessageHeaders.CURRENT_PREFERED_USERNAME, String.class);
	}


	@Override
	public IRI getCurrentUserIdIri() {
		return iri(getActorId().getNamespace() + getCurrentUserId());
	}


	@Override
	public IRI getCurrentPreferedUserNameIri() {
		return iri(getActorId().getNamespace() + getCurrentPreferedUserName());
	}
}
