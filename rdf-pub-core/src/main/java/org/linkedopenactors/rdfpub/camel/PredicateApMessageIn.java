package org.linkedopenactors.rdfpub.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

/**
 * Base class for {@link Predicate} that need the incomming message.  
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public abstract class PredicateApMessageIn implements org.apache.camel.Predicate {

	@Override
	public boolean matches(Exchange exchange) {
		return matches(new SimpleApMessage(exchange.getIn()));
	}

	/**
	 * @param apMessageIn the incomming message.
	 * @return true if the predicate matches
	 */
	protected abstract boolean matches(ApMessage apMessageIn);
}