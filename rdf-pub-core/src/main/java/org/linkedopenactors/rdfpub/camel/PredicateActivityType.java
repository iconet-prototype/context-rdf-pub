package org.linkedopenactors.rdfpub.camel;

import java.util.Set;

import org.apache.camel.Predicate;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.repository.RdfTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * Base class for {@link Predicate} that expects a specific activity type. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public abstract class PredicateActivityType extends PredicateApMessageIn {

	@Autowired
	private RdfTypeRepository rdfTypeRepository;

	@Override
	public boolean matches(ApMessage apMessage) {
		return expectedType().equals(
				determineActivityType(apMessage.getActivityId(), apMessage.getModel())
				);
	}

	/**
	 * @return The expected activity type.
	 */
	protected abstract IRI expectedType();
	
	/**
	 * @param activity the iri of the activity contained in the passed model
	 * @param model the activity
	 * @return the value/object of {@link RDF#TYPE} of the passed activity
	 * @throws RuntimeException if the activity has not exact one {@link RDF#TYPE} or the {@link RDF#TYPE} is not derived from {@link AS#Activity} 
	 */
	private IRI determineActivityType(IRI activity, Model model) {
		Set<IRI> types = Models.getPropertyIRIs(model, activity, RDF.TYPE);
		if(types.size()!=1) {
			throw new RuntimeException("activity ("+activity+") must have exact one RDF.TYPE.");
		}
		IRI activityType = types.iterator().next();
		
		if(!rdfTypeRepository.isSubclassOf(activityType, AS.Activity)) {
			throw new RuntimeException("activity type ("+activityType+") did not extend " + AS.Activity);
		}
		return activityType;
	}
	
}