package org.linkedopenactors.rdfpub.camel;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.identity.CurrentUser;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.repository.ApRepositoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import brave.Span;
import brave.Tracer;

/**
 * Abstract base class for camel processors that works with rdf activities. Provide convenience methods. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public abstract class AbstractApProcessor implements Processor {

	@Autowired
	private Tracer tracer;

	@Autowired
	protected ApRepositoryManager apRepositoryManager;

	@Autowired
	private CurrentUser currentUser;
	
	protected ApRepository getApRepository(IRI actorIri) {
		return apRepositoryManager.getActorsRepository(actorIri);
	}
	
	protected ApRepository getPublicApRepository(IRI baseUrlWithContext) {
		return apRepositoryManager.getPublicRepository(baseUrlWithContext);
	}

	protected ApMessage getIn(Exchange exchange) {
		return new SimpleApMessage(exchange.getIn());
	}

	protected String getPreferedUserName() {
		return getPreferedUserNameOpt().orElseThrow(()->new RuntimeException("IdentityProvider does not know a preferedUserName."));
	}
	
	protected Optional<String> getPreferedUserNameOpt() {
		return currentUser.getPreferedUserName();
	}

	protected String getUserId() {
		return getUserIdOpt().orElseThrow(()->new RuntimeException("IdentityProvider does not know a userId."));		
	}
	
	protected Optional<String> getUserIdOpt() {
		return currentUser.getUserId();
	}
	
	protected void addHttpStatusCodeAndTraceId(Exchange exchange, HttpStatus httpStatus) {
		exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, httpStatus.value());
		exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_TEXT, httpStatus.getReasonPhrase());
		exchange.getMessage().setHeader("x-b3-traceid", getTraceId());
		exchange.getMessage().setHeader("x-b3-spanid", getSpanId());
	}
	
	protected String getSpanAndTraceId() {
		String spanAndTrace = "n/a";
		Span currentSpan = tracer.currentSpan();
		if(currentSpan != null) {
			spanAndTrace = currentSpan
					.context()
					.spanId() + "/" + currentSpan.context().traceId();	
		}		
		return spanAndTrace;
	}
	
	private String getSpanId() {
		String spanId = "n/a";
		Span currentSpan = tracer.currentSpan();
		if(currentSpan != null) {
			spanId = currentSpan
					.context()
					.spanIdString();	
		}		
		return spanId;
	}	
	
	private String getTraceId() {
		String traceId = "n/a";
		Span currentSpan = tracer.currentSpan();
		if(currentSpan != null) {
			traceId = currentSpan
					.context()
					.traceIdString();	
		}		
		return traceId;
	}	
}