package org.linkedopenactors.rdfpub.usecases.overlapping;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.APException.ErrorType;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Translates message from different formats e.g. json-ld to turtle. If there is
 * a unsupported Content Type Header in the request you will get a list of available
 * content types.
 */
@Slf4j
@Component
public class InMessageTranslaterToTurtle extends MessageTranslaterToTurtle {

	public static String ID = StringUtils.uncapitalize(InMessageTranslaterToTurtle.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) throws Exception {
		ApMessage in = getIn(exchange);
		
		RDFFormat rdfFormat = in.getContentTypeOpt().map(contentType -> contentTypeHeaderMapping.get(contentType))
				.orElseThrow(()->new APException("Unsuported Content type. use one of '" + getSupportedMimeTypes() + "'",
						ErrorType.unsupported_content_type));
		String messageAsTurtle = in.getBody(String.class);
		try {
			messageAsTurtle = convertMessage(in, rdfFormat, RDFFormat.TURTLE);
		} catch (Exception e) {
			log.warn("message that causes problems: " + in.getBody());
			String msg = "Error converting '"+rdfFormat.getName()+"' Message to '" + RDFFormat.TURTLE.getName() + "' Did you specify the correct content type as http header? SupportedMimeTypes: " + getSupportedMimeTypes();
			log.warn(msg);
			throw new APException(msg, ErrorType.internal_server_error, e);
		}
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
								.body(messageAsTurtle)
								.contentType("text/turtle")
								.build());
	}
}

