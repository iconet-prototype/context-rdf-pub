package org.linkedopenactors.rdfpub.usecases;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * Common Exception for incomming requests. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Getter
@Setter
public class APException extends RuntimeException {

	/**
	 * The HTTP status, that should be used in a response. 
	 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
	 */
	public enum ErrorType {
		unsupported_content_type("unsupported content type",HttpStatus.UNSUPPORTED_MEDIA_TYPE.value()),
		internal_server_error("internal_server_error",HttpStatus.INTERNAL_SERVER_ERROR.value()),
		bad_request("bad request",HttpStatus.BAD_REQUEST.value()),
		not_found("not found",HttpStatus.NOT_FOUND.value());
		
		private String msg;
		private int id;
		
		private ErrorType(String msg, int id) {
			this.msg = msg;
			this.id = id;
		}
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 157650238366224686L;
	private ErrorType errorType;
	private String msg;

	public APException(String msg, ErrorType errorType) {
		super(msg + " ("+errorType.msg + "/" + errorType.id+")");
		this.msg = msg;
		this.errorType = errorType;		
	}

	public APException(String message, ErrorType errorType, Throwable cause) {
		super(message + " ("+errorType.msg + "/" + errorType.id+")", cause);
		this.msg = message;
		this.errorType = errorType;		
	}
}
