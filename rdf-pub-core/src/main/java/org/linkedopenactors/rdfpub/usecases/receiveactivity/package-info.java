/**
 * Filters in the sense of pipes &amp; filters that are needed for processing incomming activities and their objects.
 */
package org.linkedopenactors.rdfpub.usecases.receiveactivity;
