package org.linkedopenactors.rdfpub.usecases.overlapping;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.camel.Message;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import de.naturzukunft.rdf4j.utils.Framing;
import lombok.extern.slf4j.Slf4j;

/**
 * Translates message from different formats e.g. json-ld to turtle. If there is
 * a unsupported Content Type Header in the request you will get a list of available
 * content types.
 */
@Slf4j
@Component
public abstract class MessageTranslaterToTurtle extends AbstractApProcessor {

	private Framing framing = new Framing();
	
	static final Map<String, RDFFormat> contentTypeHeaderMapping = new HashMap<>();

	static {
		toContentTypeHeaderMapping(RDFFormat.TURTLE);
		toContentTypeHeaderMapping(RDFFormat.RDFXML);
		toContentTypeHeaderMapping(RDFFormat.NTRIPLES);
		toContentTypeHeaderMapping(RDFFormat.TURTLESTAR);
		toContentTypeHeaderMapping(RDFFormat.N3);
		toContentTypeHeaderMapping(RDFFormat.JSONLD);
		toContentTypeHeaderMapping(RDFFormat.NQUADS);
		toContentTypeHeaderMapping(RDFFormat.RDFJSON);
	}

	private static void toContentTypeHeaderMapping(RDFFormat rdfFormat) {
		rdfFormat.getMIMETypes().forEach(mt->contentTypeHeaderMapping.put(mt, rdfFormat));
	}
	
	protected String getSupportedMimeTypes() {
		return contentTypeHeaderMapping.values().stream()
				.map(RDFFormat::getMIMETypes)
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.collect(Collectors.joining(","));
	}

	protected String convertMessage(Message message, RDFFormat inputRdfFormat, RDFFormat outputRdfFormat) throws IOException {
		log.debug("converting "+inputRdfFormat.getName()+" message to " + outputRdfFormat);
		String messageAsString = message.getBody(String.class);
		if(!StringUtils.hasText(messageAsString)) {
			throw new RuntimeException("message has no text!");
		}
		Model model = Rio.parse(new StringReader(messageAsString), inputRdfFormat);
		if(RDFFormat.JSONLD.equals(outputRdfFormat)) {
			return framing.convert(model);
		} else {
			StringWriter stringWriter = new StringWriter();
			Rio.write(model, stringWriter, outputRdfFormat);
			return stringWriter.toString();
		}
	}
	
	protected boolean isNotTurtle(RDFFormat rdfFormat) {
		return !RDFFormat.TURTLE.equals(rdfFormat);
	}
}
