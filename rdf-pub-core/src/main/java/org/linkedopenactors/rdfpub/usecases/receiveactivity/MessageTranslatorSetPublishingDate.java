package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.literal;

import java.time.Instant;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * We set/replace the published date in each object and the activity with the
 * current dateTime. See also <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorSetPublishingDate extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorSetPublishingDate.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.body(setPublishingDate(in.getModel()))
			.build());
	}
	
	/**
	 * We set/replace the published date in each object and the activity with the current dateTime.  
	 * @param model The activity where the published date has to be set. 
	 * @return The adjusted data around the activity
	 */
	private Model setPublishingDate(Model model) {
		model.filter(null, RDF.TYPE, null).stream().map(stmt->stmt.getSubject()).forEach(subject->{
			model.remove(subject, AS.published, null);
			model.add(subject, AS.published, literal(Instant.now().toString()));
		});		
		return model;
	}
}
