package org.linkedopenactors.rdfpub.usecases.readcollection;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.APException.ErrorType;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterUrl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Extends the incoming message with all activities that are stored in the
 * requested collection. Based on the <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/DataEnricher.html">Content
 * Enricher</a> pattern.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class ContentEnricherReadCollection extends AbstractApProcessor {

	@Value( "${app.context}" )
	private String context;

	@Value( "${app.https}" )
	private Boolean https;

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentEnricherReadCollection.class.getSimpleName());

	private static final String COLLECTION = "collection";
	private static final String CAMEL_HTTP_URL = "CamelHttpUrl";

	@Override
	public void process(Exchange exchange) {		
		ApMessage in = getIn(exchange);
		
		if(in.getRequestedUrl()==null) {
			throw new APException("Header Property '"+CAMEL_HTTP_URL+"' missing.", ErrorType.bad_request);
		}

		AvailableCollection availableCollection = determineCollection(in.getCollection());

		Integer pageSize = Integer.parseInt(Optional.ofNullable(in.getHeader("pageSize")).orElse("100").toString());
		Integer startIndex = Integer.parseInt(Optional.ofNullable(in.getHeader("startIndex")).orElse("0").toString());

		ApRepository apRepository;
		switch (availableCollection) {
		case asPublic:
			apRepository = getPublicApRepository(getBaseUrlWithContext(in.getRequestedUrl()));	
			break;

		default:
			apRepository = getApRepository(in.getActorId());
			break;
		}
		
		Model model = apRepository.getCollection(availableCollection, pageSize, startIndex);

		StringWriter modelAsTurtleStringWriter = new StringWriter();
		Rio.write(model, modelAsTurtleStringWriter, RDFFormat.TURTLE);
		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(modelAsTurtleStringWriter)
				.build());
	}

	private IRI getBaseUrlWithContext(IRI requestedUrl) {
		String baseUrlWithContext;
		String requestUrlAsString = extendUrlWithSlash(requestedUrl.stringValue());
		try {			
			baseUrlWithContext = MessageTranslaterUrl.extractBaseUrl(context, https, requestUrlAsString);
		} catch (MalformedURLException e) {
			throw new RuntimeException("MalformedURLException for " + requestUrlAsString, e);
		}
		return iri(baseUrlWithContext);
	}

	/**
	 * @param requestUrl The requestesURL with or without ending slash.
	 * @return The url passed ending with a slash
	 */
	private String extendUrlWithSlash(String requestUrl) {
		if(!requestUrl.endsWith("/")) {
			requestUrl = requestUrl + "/";
		}
		return requestUrl;
	}

	private AvailableCollection determineCollection(String collectionHeader) {
		AvailableCollection availableCollection = Optional.ofNullable(collectionHeader)
				.map(collectionName -> {
					try {
						return AvailableCollection.valueOf(collectionName);
					} catch (Exception e) {
						throw new APException("Header Property '" + COLLECTION + "' is wrong. Possible values ("
								+ Stream.of(AvailableCollection.values()).map(it -> it.name())
										.collect(Collectors.joining(",")) + ") -> ",
								ErrorType.bad_request, e);
					}
				})
				.orElseThrow(() -> new APException("Header Property '" + COLLECTION + "' missing.", ErrorType.bad_request));
		return availableCollection;
	}
}
