/**
 * Provides information about a actors collection. E.g. outbox or inbox.
 */
package org.linkedopenactors.rdfpub.usecases.readcollection;
