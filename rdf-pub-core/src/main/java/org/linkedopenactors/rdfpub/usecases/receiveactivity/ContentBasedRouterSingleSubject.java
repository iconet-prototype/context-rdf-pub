package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Validates the {@link AS#object} property of an activity. The object must be
 * present, but only exact one object is possible. The object has to be derived
 * from {@link AS#Object} See <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/ContentBasedRouter.html">Content
 * Based Router</a>
 * 
 * We do not use a dead letter queue here, and notify the caller via an
 * exception. Since we cannot fix this error without information from the
 * author.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
@Component
@AllArgsConstructor
class ContentBasedRouterSingleSubject extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentBasedRouterSingleSubject.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		Model model = in.getModel();
		checkSingleObject(in.getActivityId(), model);
		exchange.setMessage( ApMessageBuilder.getInstance(exchange)
			.body(model)
			.build());
	}
	
	/**
	 * See description of {@link ContentBasedRouterSingleSubject}
	 * @param model The data send by a client  
	 * @param activity The {@link IRI} of the activity contained in the passed model
	 * @throws RuntimeException If there is no {@link AS#object}, if there is more than one {@link AS#object} and if the {@link AS#object} is not derived from {@link AS#Object}
	 */
	private void checkSingleObject(IRI activity, Model model) {
		long objectCount = model.filter(null,  RDF.TYPE, null).stream()
				.filter(stmt->stmt.getSubject().stringValue().equals(activity.stringValue()))
				.count();
		if( objectCount != 1) {
			ModelLogger.error(log, model, "model to check: ");
			throw new RuntimeException("we expect exact one object! Bus is: " + objectCount);
		}
	}
}
