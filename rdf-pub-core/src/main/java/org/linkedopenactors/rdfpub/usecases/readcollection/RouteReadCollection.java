package org.linkedopenactors.rdfpub.usecases.readcollection;

import org.apache.camel.builder.RouteBuilder;
import org.linkedopenactors.rdfpub.usecases.overlapping.ContentBasedRouterByActorMatchCurrentUser;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerProtocol;
import org.linkedopenactors.rdfpub.usecases.overlapping.OutMessageTranslater;
import org.springframework.stereotype.Component;

/**
 * Definition of the Apache Camel Route for the usecase. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class RouteReadCollection extends RouteBuilder {
	
	@Override
	public void configure() throws Exception {
    	rest().get("/{owner}/{collection}")
			.route()
			.routeId("readCollectionHttp")		
			.to("direct:readCollection")
			.endRest();
	
    	rest().get("/asPublic")
			.route()
			.routeId("readCollectionPublicHttp")		
			.to("direct:readCollectionPublic")
			.endRest();

    	from("direct:readCollectionPublic")
			.routeId("readCollectionPublic")
			.process(MessageTranslatorRequestedUrlNormalizerProtocol.ID)
			.process(ContentEnricherReadCollection.ID)
			.process(OutMessageTranslater.ID)
			.end();

    	from("direct:readCollection")
			.routeId("readCollection")
			.process(MessageTranslatorRequestedUrlNormalizerProtocol.ID)
			.process(MessageTranslatorRequestedUrlNormalizerActor.ID)
			.process(MessageTranslaterDetermineActor.ID)
			.process(ContentBasedRouterByActorMatchCurrentUser.ID)
			.process(ContentEnricherReadCollection.ID)
			.process(OutMessageTranslater.ID)
			.end();
	}
}
