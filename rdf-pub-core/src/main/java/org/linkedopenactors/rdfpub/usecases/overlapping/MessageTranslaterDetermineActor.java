package org.linkedopenactors.rdfpub.usecases.overlapping;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.net.MalformedURLException;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.linkedopenactors.rdfpub.repository.IdentityProviderUser;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Determines the actor from the requestedUrl, matches it with the data from the
 * identityProvider and creates a database(repository) for the actor if
 * necessary.
 * <br>
 * <b>in message</b>
 * <br>
 * <ul>
 * <li>{@link ActivityMessageHeaders#REQUESTED_URL}</li>
 * </ul> 
 * <br>
 * <b>out message</b>
 * <br>
 * <ul>
 * <li>{@link ActivityMessageHeaders#ACTOR_IRI}</li>
 * <li>optional {@link ActivityMessageHeaders#CURRENT_PREFERED_USERNAME}</li>
 * <li>optional {@link ActivityMessageHeaders#CURRENT_USERID}</li>
 * <li>the body of the incomming message.</li>
 * </ul> 
 */
@Component
@Slf4j
public class MessageTranslaterDetermineActor extends AbstractApProcessor {

	public static String ID = StringUtils.uncapitalize(MessageTranslaterDetermineActor.class.getSimpleName());
	
	@Value( "${app.context}" )
	private String context;

	@Value( "${app.https}" )
	private Boolean https;

	@Autowired
	private IdentityProvider identityProvider;
	
	public void process(Exchange exchange) {
		IRI actorIri = determineActorIri(getIn(exchange).getRequestedUrl().stringValue());
		exchange.setMessage(createMessage(exchange, actorIri));		
		createActorsRepositoryIfNotExists(actorIri);
	}

	/**
	 * If the passed actor is known in the {@link #identityProvider} and he has no repository yet, we create one. 
	 * @param actorId the id of the actor that should become a repository
	 */
	private void createActorsRepositoryIfNotExists(IRI actorId) {
		identityProvider.logIdentityProviderProperties(log, Level.TRACE);
		List<IdentityProviderUser> users = identityProvider.search(actorId);
		if(!users.isEmpty()) {
			boolean hasRepository = apRepositoryManager.hasRepository(actorId);
			log.trace("hasRepository: " + actorId + " ->  " + hasRepository);
			if(!hasRepository) {
				apRepositoryManager.createActorsRepository4Person(actorId);
			}
		}
	}

	/**
	 * @param exchange the axchange containing the message
	 * @param actorId the id of the actor.
	 * @return An out message containing the actor id, optional preferredUserName, optional userId and the body of the incomming message.
	 */
	private Message createMessage(Exchange exchange, IRI actorId) {
		ApMessage in = getIn(exchange);
		ApMessageBuilder apMessageBuilder = ApMessageBuilder.getInstance(exchange)
				.actorIri(actorId);
		getPreferedUserNameOpt().ifPresent(apMessageBuilder::currentPreferedUserName);
		getUserIdOpt().ifPresent(apMessageBuilder::currentUserId);
		apMessageBuilder.body(in.getBody(String.class));		
		Message apMessage = apMessageBuilder.build();
		return apMessage;
	}

	/**
	 * Get the actor id from the passed requested url.
	 * @param requestUrl The requested url.
	 * @return The actor id, determined from the requested url. 
	 */
	private IRI determineActorIri(String requestUrl) {
		requestUrl = extendUrlWithSlash(requestUrl);
		IRI actorIri = buildActorFromRequestUrl(requestUrl);
		actorIri = identityProvider.normalize(actorIri);
		return actorIri;
	}

	/**
	 * @param requestUrl The requestesURL with or without ending slash.
	 * @return The url passed ending with a slash
	 */
	private String extendUrlWithSlash(String requestUrl) {
		if(!requestUrl.endsWith("/")) {
			requestUrl = requestUrl + "/";
		}
		return requestUrl;
	}
	
	/**
	 * @param requestUrlAsString the requested URL, that contains the actors id.
	 * @return Extracted actor from the passed requestUrlAsString
	 */
	private IRI buildActorFromRequestUrl(String requestUrlAsString) {
		try {
			String baseUrlWithContext = MessageTranslaterUrl.extractBaseUrl(context, https, requestUrlAsString);
			String actorName = MessageTranslaterUrl.extractActorName(context, requestUrlAsString, baseUrlWithContext);
			return  iri(baseUrlWithContext + actorName);
		} catch (MalformedURLException e) {
			throw new RuntimeException("MalformedURLException for " + requestUrlAsString, e);
		}
	}
}
