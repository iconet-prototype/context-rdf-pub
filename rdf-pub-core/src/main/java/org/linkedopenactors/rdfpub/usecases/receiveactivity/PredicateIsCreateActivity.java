package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.PredicateActivityType;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * Checks if the incomming activity is a {@link AS#Create} activity.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class PredicateIsCreateActivity extends PredicateActivityType {

	public IRI expectedType() {
		return AS.Create;
	}
}