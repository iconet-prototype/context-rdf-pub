package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * When a Create activity is posted, the actor of the activity SHOULD be copied
 * onto the object's attributedTo field. See <a href=
 * "https://www.w3.org/TR/activitypub/#create-activity-outbox">create-activity-outbox</a>
 * See also <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorSetAttributedTo extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorSetAttributedTo.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(setAttributedTo(in.getActorId(), in.getActivityId(), in.getModel()))
				.build());
	}

	/**
	 * When a Create activity is posted, the actor of the activity SHOULD be copied onto the object's attributedTo field. 
	 * See https://www.w3.org/TR/activitypub/#create-activity-outbox
	 * @param actorIri The client who was sending the data
	 * @param activity The {@link IRI} of the activity
	 * @param model The data
	 * @return The adjusted data
	 */
	private Model setAttributedTo(IRI actorIri, IRI activity, Model model) {
		Set<IRI> subjects = getSubjectsOfAllObjects(activity, model);
		for (IRI subject : subjects) {
			model.add(subject, AS.attributedTo, actorIri);
		}
		return model;
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model, that are not the activity.
	 * @param activityId activity to filter out.
	 * @param model the model containing the objects
	 * @return Set of all subjects (IRI) in the passed model, that are not the activity.
	 */
	private Set<IRI> getSubjectsOfAllObjects(IRI activityId, Model model) {
		Set<IRI> subjects = getSubjects(model);
		subjects.remove(activityId);
		return subjects;	
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model.
	 * @param model to seacrh for subjects.
	 * @return Set of all subjects (IRI) in the passed model.
	 */
	private Set<IRI> getSubjects(Model model) {
		Set<IRI> subjects = model.stream().map(stmt->(IRI)stmt.getSubject()).collect(Collectors.toSet());
		return subjects;
	}
}
