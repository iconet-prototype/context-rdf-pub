package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.apache.camel.Exchange.HTTP_RESPONSE_CODE;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.usecases.overlapping.ContentBasedRouterByActorMatchCurrentUser;
import org.linkedopenactors.rdfpub.usecases.overlapping.InMessageTranslaterToTurtle;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Definition of the Apache Camel Route for the usecase. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class RouteReceiveActivity extends RouteBuilder {
	
	@Autowired
	private PredicateIsUpdateActivity isUpdateActivity;
	
	@Autowired
	private PredicateIsCreateActivity isCreateActivity;

	@Override
	public void configure() throws Exception {
    	routeReceiveActivity();
		routeCreateActivity();
    	routeUpdateActivity();
    	
    	rest().post("/{owner}/outbox")			
			.route()
			.routeId("receiveActivityHttp")
			.process(MessageTranslaterDetermineActor.ID)
			.process(ContentBasedRouterByActorMatchCurrentUser.ID)
			.process(InMessageTranslaterToTurtle.ID)
			.to("direct:receiveActivity")
			.endRest();
    }

	private void routeReceiveActivity() {
		from("direct:receiveActivity")
			.routeId("receiveActivity")					
			.process(MessageTranslaterDetermineActor.ID)
			.process(ContentEnricherSurroundingActivityCreator.ID)
			.choice()
				.when(isCreateActivity)
					.log(LoggingLevel.DEBUG, "Create Activity Received")
					.to("direct:createActivity")
				.when(isUpdateActivity)
					.log(LoggingLevel.DEBUG, "Update Activity Received")
					.to("direct:updateActivity")
				.otherwise()
					.log(LoggingLevel.WARN, "unknown activity " + header(ActivityMessageHeaders.ACTIVITY_TYPE_IRI) + " received.")
					.setHeader( HTTP_RESPONSE_CODE, constant("400"))
					.setBody().simple(" { \"message\" : \""+header(ActivityMessageHeaders.ACTIVITY_TYPE_IRI)+" not supported!\" }")
	            .endChoice();
	}
	
	private void routeCreateActivity() {
		from("direct:createActivity")
			.routeId("createActivityRoute")
			.process(ContentBasedRouterByObjectTypeValidation.ID)
			.process(MessageTranslatorRemoveOrphans.ID)
			.process(MessageTranslatorCheckOrSetActor.ID)
			.process(MessageTranslatorSetPublishingDate.ID)
			.process(MessageTranslatorSetAttributedTo.ID)
			.process(MessageTranslatorSetReceivers.ID)
			.process(MessageTranslatorReplaceObjectSubjects.ID)
			.process(MessageTranslatorReplaceActivitySubject.ID)
			.process(SharedDataBaseSaveActivity.ID)
			.end();
	}
	
	private void routeUpdateActivity() {
		from("direct:updateActivity")
			.routeId("updateActivityRoute")
			.process(ContentBasedRouterByObjectTypeValidation.ID)
			.process(ContentBasedRouterSingleSubject.ID)
			.process(MessageTranslatorRemoveOrphans.ID)
			.process(MessageTranslatorCheckOrSetActor.ID)
			.process(MessageTranslaterSetActivityPublishingDate.ID)
			.process(MessageTranslaterSetUpdateDate.ID)
			.process(MessageTranslatorReplaceActivitySubject.ID)
			.process(SharedDataBaseUpdateActivity.ID)
			.end();
	}
}
