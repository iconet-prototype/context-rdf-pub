package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

/**
 * The id of each object in the passed model should be unique an understandable
 * by our system. So we replace each id in the whole model. If there is an
 * object in the model, that has a subject, that is accessable over the internet
 * (already existing object) we ignore that and create a copy with a new id. See
 * <a href="https://www.w3.org/TR/activitypub/#obj-id">object id</a> . See also
 * <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorReplaceObjectSubjects extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorReplaceObjectSubjects.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		Model model = in.getModel();
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(replaceObjectSubjects(in.getActivityId(), in.getActorId(), model))
				.build());
	}

	/**
	 * The id of each object in the passed model should be unique an understandable
	 * by our system. So we replace each id in the whole model. If there is an
	 * object in the model, that has a subject, that is accessable over the internet
	 * (already existing object) we ignore that and create a copy with a new id. See
	 * https://www.w3.org/TR/activitypub/#obj-id
	 * 
	 * @param activity The IRI of the activity
	 * @param actorIri The IRI of the actor of the activity
	 * @param model the activity as rdf4j model
	 * @return The adjusted data around the activity
	 */
	public Model replaceObjectSubjects(IRI activity, IRI actorIri, Model model) {
		Set<IRI> subjects = getSubjectsOfAllObjects(activity, model);
		for (IRI oldSubject : subjects) {
			IRI newSubject = createAsObjectSubject(actorIri);

			// Replace the statement, that contains the object type. ($oldSubject - RDF.TYPE
			// - $type) -> ($newSubject - RDF.TYPE - $type)
			model = replaceSubject(model, oldSubject, newSubject);

			// replace all references to the oldSubject. ($someSubject - $someProperty -
			// $oldSubject) -> ($someSubject - $someProperty - $newSubject)
			Model referencesToOldSubject = clone(model.filter(null, null, oldSubject));
			for (Statement referenceToOldSubject : referencesToOldSubject) {
				model.remove(referenceToOldSubject);
				model.add(referenceToOldSubject.getSubject(), referenceToOldSubject.getPredicate(), newSubject);
			}
		}
		return model;
	}

	/**
	 * Replaces the passed subject in all matching statements. Subject and Object
	 * (references) of the triples are replaced!
	 * 
	 * @param model      The model to manipulate
	 * @param oldSubject the subject to replace
	 * @param newSubject the new subject that replaces the old one.
	 * @return manipulated model
	 */
	private Model replaceSubject(Model model, IRI oldSubject, IRI newSubject) {
		Model clone = new ModelBuilder().build();
		clone.addAll(model);

		Model filtered = model.filter(oldSubject, null, null);

		ModelBuilder mb = new ModelBuilder().subject(newSubject);
		for (Statement statement : filtered) {
			clone.remove(statement);
			mb.add(statement.getPredicate(), statement.getObject());
		}
		clone.addAll(mb.build());
		return clone;
	}

	/**
	 * Creates a clone of the passed model.
	 * 
	 * @param model model to clone
	 * @return a clone
	 */
	private Model clone(Model model) {
		Model clone = new ModelBuilder().build();
		clone.addAll(model);
		model.getNamespaces().forEach(ns -> clone.setNamespace(ns));
		return clone;
	}

	public IRI createAsObjectSubject(IRI actorIri) {
		return iri(actorIri.stringValue() + "/objects/object_" + UUID.randomUUID());
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model, that are not the
	 * activity.
	 * 
	 * @param activityId activity to filter out.
	 * @param model the model containing the objects
	 * @return Set of all subjects (IRI) in the passed model, that are not the
	 *         activity.
	 */
	private Set<IRI> getSubjectsOfAllObjects(IRI activityId, Model model) {
		Set<IRI> subjects = getSubjects(model);
		subjects.remove(activityId);
		return subjects;
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model.
	 * 
	 * @param model to seacrh for subjects.
	 * @return Set of all subjects (IRI) in the passed model.
	 */
	private Set<IRI> getSubjects(Model model) {
		Set<IRI> subjects = model.stream().map(stmt -> (IRI) stmt.getSubject()).collect(Collectors.toSet());
		return subjects;
	}
}
