package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.UUID;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

/**
 * We are creating a new subject for each new activity, because we have to make
 * sure, that it's available on the net with that id. And we are responsible for
 * the uniquess of that id. See https://www.w3.org/TR/activitypub/#obj-id See
 * <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorReplaceActivitySubject extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorReplaceActivitySubject.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		ModelAndSubject modelAndSubject = replaceActivitySubject(in.getActivityId(), in.getActorId(), in.getModel());
		exchange.setMessage( ApMessageBuilder.getInstance(exchange)
			.body(modelAndSubject.getModel())
			.activityIri(modelAndSubject.getSubject())
			.build());
	}
	
	/**
	 * We are creating a new subject for each new activity, because we have to make sure, that it's available on the net with that id.
	 * And we are responsible for the uniquess of that id. See https://www.w3.org/TR/activitypub/#obj-id
	 * @param activity The activity iri
	 * @param actorIri the client who sneds the adata
	 * @param model the data sent
	 * @return The adjusted data around the activity
	 */
	public ModelAndSubject replaceActivitySubject(IRI activity, IRI actorIri, Model model) {
		IRI activityIriNew = iri(actorIri.stringValue() + "/outbox/activity_" + UUID.randomUUID());
		// Replace all referenced to the activity
		Model references = clone(model).filter(null, null, activity);
		for (Statement statement : references) {
			model.remove(statement);
			model.add(statement.getSubject(), statement.getPredicate(), activityIriNew);
		}
		Model activityStatements = clone(model).filter(activity, null, null);
		for (Statement statement : activityStatements) {
			model.remove(statement);
			model.add(activityIriNew, statement.getPredicate(), statement.getObject());
		}
		return new ModelAndSubject(activityIriNew, model);
	}

	/**
	 * Creates a clone of the passed model.
	 * @param model model to clone
	 * @return a clone
	 */
	private Model clone(Model model) {
		Model clone = new ModelBuilder().build();
		clone.addAll(model);
		model.getNamespaces().forEach(ns->clone.setNamespace(ns));
		return clone;
	}
}
