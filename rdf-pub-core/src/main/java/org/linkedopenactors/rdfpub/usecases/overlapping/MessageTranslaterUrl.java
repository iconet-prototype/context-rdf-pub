package org.linkedopenactors.rdfpub.usecases.overlapping;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageTranslaterUrl {

	public static String ID = StringUtils.uncapitalize(MessageTranslaterUrl.class.getSimpleName());

	/**
	 * Extracts the actors name/id from the passed requestUrlAsString.
	 * !!! We expect the actors name/id after the baseUrlWithContext !!!
	 * @param context the context in the url. E.g. "/camel/" in 'http://localhost:8080/camel/userId/outbox' 
	 * @param requestUrlAsString the requested URL, that contains the actors id/name.
	 * @param baseUrlWithContextAsString the baseUrl / context url of the current running server.
	 * @return The actor name extracted from the passed url.
	 */
	public static String extractActorName(String context, String requestUrlAsString, String baseUrlWithContextAsString) {
		try {
			URL requestUrl = new URL(requestUrlAsString);
			URL baseUrlWithContext = new URL(baseUrlWithContextAsString);
			String withoutBase = requestUrl.getPath().replace(baseUrlWithContext.getPath(), "");
			String actorName = withoutBase.substring(0, withoutBase.indexOf("/"));
			log.trace("requestUrl: " + requestUrl.getPath() + " - baseUrlWithContext: " + baseUrlWithContext.getPath() + " - determinated actorName: " + actorName);
			return actorName;
		} catch (MalformedURLException e) {
			throw new RuntimeException("MalformedURLException", e);
		}
	}

	/**
	 * @param requestUrlAsString the url where te request was send to.
	 * @param context the context in the url. E.g. "/camel/" in 'http://localhost:8080/camel/userId/outbox'
	 * @param https true, if the {@link IRI} should start wit https.
	 * @return Extracted the baseUrl with protocol, host, port of the passed requestUrlAsString and adds the passed context
	 * @throws MalformedURLException if the passed requestUrlAsString is not a valid URL. 
	 */
	public static String extractBaseUrl(String context, boolean https, String requestUrlAsString) throws MalformedURLException {
		if(!requestUrlAsString.contains(context)) {
			throw new RuntimeException("requestUrl '"+requestUrlAsString+"' does not contain context '"+context+"'");
		}
		URL requestUrl = new URL(requestUrlAsString);
		String port = requestUrl.getPort()!=-1 ? ":" + requestUrl.getPort() : "";
		String base = https ? "https" : "http";
		base = base + "://"
				+ requestUrl.getHost()
				+ port
				+ context;
		return base;
	}
}
