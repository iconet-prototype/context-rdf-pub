package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * The actor in the activity should match to the outbox owner. If the actor in
 * the activity is empty, we set it with the outbox owner. See <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorCheckOrSetActor extends AbstractApProcessor {

	@Autowired
	private IdentityProvider identityProvider;

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorCheckOrSetActor.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(checkOrSetActor(in.getActorId(), in.getActivityId(), in.getModel()).getModel())
				.build()
				);
	}

	/**
	 * The actor in the activity should match to the outbox owner.  
	 * If the actor in the activity is empty, we set it with the outbox owner. 
	 * @param actorIri the one who was sending the data
	 * @param model The data send by a client  
	 * @param activity The {@link IRI} of the activity contained in the passed model
	 * @return the data with the correct {@link AS#actor}
	 * @throws RuntimeException if there is a {@link AS#actor} set, but it's not the passed one.
	 */
	private ModelAndSubject checkOrSetActor(IRI actorIri, IRI activity, Model model) {
		Optional<IRI> actorOfActivityOptional = Models.getPropertyIRI(model, activity, AS.actor);
		if (actorOfActivityOptional.isPresent()) {
			IRI actorIdFromModel = identityProvider.normalize(actorOfActivityOptional.get()); 
			if (!actorIdFromModel.equals(actorIri)) {
				throw new RuntimeException("The actor in the activity (" + actorIdFromModel
						+ ") does not match the actor of the outbox (" + actorIri + ")");
			}
		} else {
			model.add(activity, AS.actor, actorIri);
		}
		return new ModelAndSubject(activity, model);
	}
}