package org.linkedopenactors.rdfpub.usecases.queryactorsoutbox;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;
import org.linkedopenactors.rdfpub.repository.SparqlResult;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.APException.ErrorType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Extends the incoming message with the result of the query. Based on the
 * <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/DataEnricher.html">Content
 * Enricher</a> pattern.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
@Slf4j
class ContentEnricherSparql extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentEnricherSparql.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		String sparqlQuery = Optional.ofNullable(in.getHeader("query", String.class))
				.orElse(in.getBody(String.class));
		if(!StringUtils.hasText(sparqlQuery)) {
			throw new APException("sparqlQuery is mandatory.", ErrorType.bad_request);			
		}
		String headerAccept = in.getAcceptHeaderOpt().orElse(null);
		log.debug("proccess query: "+sparqlQuery+", acceptHeader: " + headerAccept +")");
		
		IRI actorId = in.getActorId();
		AvailableCollection[] collections = new AvailableCollection[] {determineCollection(in.getCollection())};
		
		SparqlResult sparqlResult = getApRepository(actorId).executeSparql(collections, 
				sparqlQuery, 
				headerAccept);
		log.debug("result: " + sparqlResult);
		exchange.getMessage().setHeader("Content-Type", sparqlResult.getMimeType() );
		exchange.getMessage().setBody(sparqlResult.getResult());
	}
	
	private AvailableCollection determineCollection(String collectionHeader) {
		AvailableCollection availableCollection = Optional.ofNullable(collectionHeader)
				.map(collectionName -> {
					try {
						return AvailableCollection.valueOf(collectionName);
					} catch (Exception e) {
						throw new APException("Header Property '" + ActivityMessageHeaders.COLLECTION + "' is wrong. Possible values ("
								+ Stream.of(AvailableCollection.values()).map(it -> it.name())
										.collect(Collectors.joining(",")) + ") -> ",
								ErrorType.bad_request, e);
					}
				})
				.orElseThrow(() -> new APException("Header Property '" + ActivityMessageHeaders.COLLECTION + "' missing.", ErrorType.bad_request));
		return availableCollection;
	}
}
