package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.literal;

import java.time.Instant;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.Model;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * We set/replace the published date in the update Activity with the current dateTime.  
 * See <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslaterSetActivityPublishingDate extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslaterSetActivityPublishingDate.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		
		Model model = in.getModel();
		model.remove(in.getActivityId(), AS.published, null);
		model.add(in.getActivityId(), AS.published, literal(Instant.now().toString()));
		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.body(model)
			.build());
	}
}
