package org.linkedopenactors.rdfpub.usecases.readactor;

import org.apache.camel.builder.RouteBuilder;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.OutMessageTranslater;
import org.springframework.stereotype.Component;

/**
 * Definition of the Apache Camel Route for the usecase. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class RouteReadActor extends RouteBuilder {

	@Override
    public void configure() {
    	rest()
			.get("/{owner}/")
			.route()
			.routeId("ReadProfileRouteHttp")
			.to("direct:readProfile")
			.endRest();
    	
    	from("direct:readProfile")
			.routeId("ReadProfileRoute")
			.process(MessageTranslaterDetermineActor.ID)  
			.process(ContentEnricherReadActor.ID)
			.process(OutMessageTranslater.ID)
			.end();
    }
}
