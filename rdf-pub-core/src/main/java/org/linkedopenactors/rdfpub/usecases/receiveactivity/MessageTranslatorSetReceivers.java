package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;

/**
 * Make sure, that all receivers are equal over all objects in the graph including the activity.
 * See https://www.w3.org/TR/activitypub/#client-addressing
 * See <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorSetReceivers extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorSetReceivers.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.body(setReceivers(in.getModel()))
			.build());
	}

	/**
	 * Make sure, that all receivers are equal over all objects in the graph including the activity.
	 * See https://www.w3.org/TR/activitypub/#client-addressing
	 * 
	 * @param model The data that the client was sending
	 * @return The adjusted data 
	 */
	private Model setReceivers(Model model) {
		Set<IRI> cc = getAllReceivers(model, AS.cc);
		Set<IRI> bcc = getAllReceivers(model, AS.bcc);
		Set<IRI> to = getAllReceivers(model, AS.to);
		Set<IRI> bto = getAllReceivers(model, AS.bto);
		Set<IRI> audience = getAllReceivers(model, AS.audience);
		
		Set<IRI> subjects = model.filter(null, RDF.TYPE, null).stream().map(Statement::getSubject).map(it->(IRI)it).collect(Collectors.toSet());
		subjects.forEach(subject->{
			addPropertyValues(model, subject, AS.cc, cc);
			addPropertyValues(model, subject, AS.bcc, bcc);
			addPropertyValues(model, subject, AS.to, to);
			addPropertyValues(model, subject, AS.bto, bto);
			addPropertyValues(model, subject, AS.audience, audience);
		});		
		return model;
	}

	/**
	 * For each item int he passed values a new statement is added to the passed model.
	 * The subject and predicate of the added statements are always the passed ones.
	 * @param model To be extended
	 * @param subject subject to use
	 * @param property predicate top use
	 * @param values objects that are added to the passed model
	 */
	private void addPropertyValues(Model model, IRI subject, IRI property, Set<IRI> values) {
		values.stream().forEach(value->model.add(subject, property, value));
	}

	/**
	 * Extract all receivers of the passe type over all subjects.
	 * See: https://www.w3.org/TR/activitypub/#create-activity-outbox
	 * A mismatch between addressing of the Create activity and its object is
	 * likely to lead to confusion. As such, a server SHOULD copy any 
	 * recipients of the Create activity to its object upon initial distribution, 
	 * and likewise with copying recipients from the object to the wrapping Create 
	 * activity. Note that it is acceptable for the object's addressing to be 
	 * changed later without changing the Create's addressing 
	 * (for example via an Update activity).
	 *  
	 * @param model The complete model sendet from a client
	 * @param type AS.cc, AS.bcc, AS.to, As.bto, AS.audience
	 * @return Set of receivers of the requested type.
	 */
	private Set<IRI> getAllReceivers(Model model, IRI type) {
		return model.filter(null, type, null).stream()
				.map(stmt->stmt.getObject()) // get all objects
				.filter(val->val.isIRI()) // a receiver have to be an IRI !
				.map(it->(IRI)it) // cast to IRI
				.collect(Collectors.toSet()); // convert to Set
	}
}
