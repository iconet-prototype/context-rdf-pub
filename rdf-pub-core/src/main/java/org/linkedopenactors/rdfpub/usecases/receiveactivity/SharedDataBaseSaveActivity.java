package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.linkedopenactors.rdfpub.apTools.ReceiverExtractor;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

/**
 * Activities and Objects are stored in a <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/SharedDataBaseIntegration.html">shared
 * database</a>, so that other application are able to access and query the
 * data. Also the internal delivrey is donme, by saviong the model in the
 * receivers inbox.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
@Component
class SharedDataBaseSaveActivity extends AbstractApProcessor {
	
	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(SharedDataBaseSaveActivity.class.getSimpleName());
	
	@Autowired
	private ReceiverExtractor receiverExtractor;
		
	@Autowired
	private IdentityProvider identityProvider;

	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		IRI activity = in.getActivityId();
		
		IRI actorIri = in.getActorId();
		
		ModelAndSubject modelAndSubject = new ModelAndSubject(activity, in.getModel());
		Model savedModel = saveToOutbox(actorIri, modelAndSubject);		
		internalDelivery(actorIri, modelAndSubject);
		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.body(savedModel)
			.activitySaved()
			.location(activity)
			.build());
	}

	private Model saveToOutbox(IRI actorIri, ModelAndSubject modelAndSubject) {
		ApRepository apRepository = getApRepository(actorIri);
		
		Model outboxStmt = apRepository.getProfile().filter(actorIri, AS.outbox, null, actorIri);
		IRI outbox = Models.getPropertyIRI(outboxStmt, actorIri, AS.outbox, actorIri).orElseThrow(()->new RuntimeException("no outbox statement found in profile."));
				
		IRI createdActivity = apRepository.save(modelAndSubject, Set.of(actorIri,outbox));
		
		Model savedModel = apRepository.readObject(createdActivity)
				.orElseThrow(() -> new RuntimeException("read activity after save failed !"))
				.getModel();
		
		return savedModel;
	}
	
	private void internalDelivery(IRI actorId, ModelAndSubject modelAndSubject) {
		log.debug("internalDelivery("+actorId+", "+modelAndSubject.getSubject()+")");
		Set<IRI> extractInternalReceivers = receiverExtractor.extractInternalReceivers(actorId, modelAndSubject).stream()
				.map(this::normalize)
				.collect(Collectors.toSet());
		Set<IRI> contexts = new HashSet<>();		
		contexts.addAll(extractInternalReceivers);
		contexts.addAll(extractInternalReceivers.stream().map(receiver->iri(receiver+"/inbox")).collect(Collectors.toSet()));
		 
		Model model = modelAndSubject.getModel();
		model.remove(modelAndSubject.getSubject(), AS.bto, null);
		model.remove(modelAndSubject.getSubject(), AS.bcc, null);
				
		ModelAndSubject modelAndSubjectWithoutBlindCopyReceivers = new ModelAndSubject(modelAndSubject.getSubject(), model);	
				
		getApRepository(actorId)
			.save(modelAndSubjectWithoutBlindCopyReceivers, contexts);
		ModelLogger.trace(log, modelAndSubjectWithoutBlindCopyReceivers.getModel(), "modelAndSubjectWithoutBlindCopyReceivers: ");
		log.debug("contexts: " + contexts);
	}

	/**
	 * Normalized the actor id, see: {@link IdentityProvider}{@link #normalize(IRI)}, expect the receiver is a {@link #isSpecialReceiver(IRI)}.
	 * @param receiver The receiver to normalize. 
	 * @return the normalized receiver IRI. 
	 */
	private IRI normalize(IRI receiver) {
		if(!isSpecialReceiver(receiver)) {						
			return identityProvider.normalize(receiver);
		} else {
			return receiver;
		}
	}

	/**
	 * Checks if the receiver is a special receiver, that is not known by the identityProvider like {@link AS#Public}
	 * @param receiver The receiver to check.
	 * @return True, if it is a special receiver, otherwise false.
	 */
	private boolean isSpecialReceiver(IRI receiver) {
		return AS.Public.equals(receiver);
	}
}
