package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.RdfTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Validates the {@link AS#object} property of an activity. The object must be
 * present, but only exact one object is possible. The object has to be derived
 * from {@link AS#Object} See <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/ContentBasedRouter.html">Content
 * Based Router</a>
 * 
 * We do not use a dead letter queue here, and notify the caller via an
 * exception. Since we cannot fix this error without information from the
 * author.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
@AllArgsConstructor
@Slf4j
class ContentBasedRouterByObjectTypeValidation extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentBasedRouterByObjectTypeValidation.class.getSimpleName());
	
	@Autowired
	private RdfTypeRepository rdfTypeRepository; 
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		Model model = in.getModel();
		checkObjectType(in.getActivityId(), model);
		exchange.setMessage( ApMessageBuilder.getInstance(exchange)
			.body(model)
			.build());
	}
	
	/**
	 * See description of {@link ContentBasedRouterByObjectTypeValidation}
	 * @param model The data send by a client  
	 * @param activity The {@link IRI} of the activity contained in the passed model
	 * @throws RuntimeException If there is no {@link AS#object}, if there is more than one {@link AS#object} and if the {@link AS#object} is not derived from {@link AS#Object}
	 */
	private void checkObjectType(IRI activity, Model model) {
		Set<IRI> objects = Models.getPropertyIRIs(model, activity, AS.object);
		if(objects.size()>1) {
			throw new RuntimeException("more than one As:object in the activity.");
		}
		IRI objectIri = objects.stream().findFirst().orElseThrow(()->new RuntimeException("No As:object in the activity"));
		throwExceptionIfNoAsObject(objectIri, model);
	}
	
	/**
	 * Checks, if the passed object is derived from AsObject.
	 * @param objectIri The subject iri of the object to check.
	 * @param model the model containing the object to check.
	 * @throws RuntimeException if T derived from AsObject.
	 */
	private void throwExceptionIfNoAsObject(IRI objectIri, Model model) {
		Set<IRI> objectTypes = Models.getPropertyIRIs(model, objectIri, RDF.TYPE);
		if(objectTypes.stream().filter(objectType->rdfTypeRepository.isSubclassOf(objectType, AS.Object)).count()<1) {
			String typesAsString = objectTypes.stream().map(IRI::stringValue).collect(Collectors.joining(","));
			String msg = "The objects ("+objectIri+") types in the activity is '" + typesAsString + "' but it has to be a/derived from " + AS.Object;
			ModelLogger.error(log, model, msg);			
			throw new RuntimeException(msg);
		}
	}
}
