package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.stereotype.Component;

/**
 * If there are statements, that are not referenced in the graph (orphan) we
 * delete them. See <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class MessageTranslatorRemoveOrphans extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslatorRemoveOrphans.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		Model model = in.getModel();
		exchange.setMessage( ApMessageBuilder.getInstance(exchange)
				.body(removeOrphans(in.getActivityId(), model))
				.build());
	}

	/**
	 * If there are statements, that are not referenced in the graph (orphan) we delete it.
	 * @param model The data send by a client  
	 * @param activity The {@link IRI} of the activity contained in the passed model
	 * @return The adjusted data
	 */
	private Model removeOrphans(IRI activity, Model model) {
		Set<IRI> subjects = getSubjectsOfAllObjects(activity, model);
		for (IRI subject : subjects) {
			Set<IRI> references = getSubjects(model.filter(null, null, subject));
			if(references.isEmpty()) {
				model.remove(subject, null, null);
			}
		}
		return model;
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model, that are not the activity.
	 * @param activityId activity to filter out.
	 * @param model the model containing the objects
	 * @return Set of all subjects (IRI) in the passed model, that are not the activity.
	 */
	private Set<IRI> getSubjectsOfAllObjects(IRI activityId, Model model) {
		Set<IRI> subjects = getSubjects(model);
		subjects.remove(activityId);
		return subjects;	
	}

	/**
	 * Build a set of all subjects (IRI) in the passed model.
	 * @param model to seacrh for subjects.
	 * @return Set of all subjects (IRI) in the passed model.
	 */
	private Set<IRI> getSubjects(Model model) {
		Set<IRI> subjects = model.stream().map(stmt->(IRI)stmt.getSubject()).collect(Collectors.toSet());
		return subjects;
	}
}
