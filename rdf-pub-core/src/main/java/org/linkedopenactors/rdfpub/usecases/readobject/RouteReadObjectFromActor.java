package org.linkedopenactors.rdfpub.usecases.readobject;

import org.apache.camel.builder.RouteBuilder;
import org.linkedopenactors.rdfpub.usecases.overlapping.ContentBasedRouterByActorMatchCurrentUser;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerProtocol;
import org.linkedopenactors.rdfpub.usecases.overlapping.OutMessageTranslater;
import org.springframework.stereotype.Component;

/**
 * Definition of the Apache Camel Route for the usecase. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class RouteReadObjectFromActor extends RouteBuilder {

    @Override
    public void configure() {
    	routeReadObjectFromActorHttp();
    	routeReadObjectFromActor();
    }
    
	private void routeReadObjectFromActorHttp() {
		rest().get("/{owner}/{area}/{objectId}")
			.route()
			.to("direct:readObjectFromActor")
			.endRest();
	}
		
	private void routeReadObjectFromActor() {
		this.from("direct:readObjectFromActor")
			.routeId("readObjectFromActor")			
			.process(MessageTranslatorRequestedUrlNormalizerProtocol.ID)
			.process(MessageTranslatorRequestedUrlNormalizerActor.ID)
			.process(MessageTranslaterDetermineActor.ID)
			.process(ContentBasedRouterByActorMatchCurrentUser.ID)
			.process(ContentEnricherReadObjectFromActor.ID)
			.process(OutMessageTranslater.ID)
			.end();
	}
}
