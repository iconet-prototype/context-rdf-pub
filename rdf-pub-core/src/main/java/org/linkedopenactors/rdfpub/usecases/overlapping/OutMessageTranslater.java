package org.linkedopenactors.rdfpub.usecases.overlapping;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.APException.ErrorType;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Translates message into different formats. If there is
 * a unsupported Accept Header in the request you will get a list of available
 * content types.
 */
@Slf4j
@Component
public class OutMessageTranslater extends MessageTranslaterToTurtle {

	public static String ID = StringUtils.uncapitalize(OutMessageTranslater.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Message message = exchange.getMessage();
		String acceptHeaderValue = message.getHeader(HttpHeaders.ACCEPT, String.class);
		RDFFormat rdfFormat = contentTypeHeaderMapping.get(acceptHeaderValue);
		log.debug("Interpret "+acceptHeaderValue+" as rdfFormat "+rdfFormat);
		
		if(rdfFormat==null) {
			String msg = "Error converting out message to Accept Format '"+acceptHeaderValue+"' Supported formats: "+getSupportedMimeTypes()+".";
			log.warn(msg);
			rdfFormat = RDFFormat.TURTLE;
			log.warn("We use the default RDFFormat: " + rdfFormat);
			acceptHeaderValue = "text/turtle";
		}
		
		if(!messageBodyHasText(exchange) ) {
			throw new APException("message is NULL!", ErrorType.internal_server_error);
		}
		String messageInRequestedFormat = message.getBody(String.class); 
		if(isNotTurtle(rdfFormat)) {
			try {
				messageInRequestedFormat = convertMessage(message, RDFFormat.TURTLE, rdfFormat);
			} catch (Exception e) {
				throw new APException(e.getMessage(), ErrorType.bad_request);
			}
		}
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.contentType(acceptHeaderValue)
				.body(messageInRequestedFormat)
				.build());
	}

	private boolean messageBodyHasText(Exchange exchange) {
		if( exchange.getMessage()==null ) {
			return false;
		}
		return StringUtils.hasText(exchange.getMessage().getBody(String.class));
	}
}
