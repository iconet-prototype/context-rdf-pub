package org.linkedopenactors.rdfpub.usecases.queryactorsoutbox;

import org.apache.camel.builder.RouteBuilder;
import org.linkedopenactors.rdfpub.usecases.overlapping.ContentBasedRouterByActorMatchCurrentUser;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerActor;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerProtocol;
import org.springframework.stereotype.Component;

/**
 * Definition of the Apache Camel Route for the usecase. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class RouteQueryActorsOutbox extends RouteBuilder {

    @Override
    public void configure() {
		routeSparqlHttpPost();
		routeSparqlHttpGet();
		routeSparql();
    }

    /**
     * Specifies the route for receiving a SPARQL query via HTTP POST.
     */
	private void routeSparqlHttpPost() {
		this.rest().post("/{owner}/{collection}/sparql")
		.route()
		.routeId("routeSparqlHttpPost")
		.to("direct:sparql")
		.endRest();
	}
	
    /**
     * Specifies the route for receiving a SPARQL query via HTTP GET.
     */
	private void routeSparqlHttpGet() {
		this.rest().get("/{owner}/{collection}/sparql")
			.route()
			.routeId("routeSparqlHttpGet")
			.to("direct:sparql")
			.endRest();
	}

	/**
	 * Specifies the SPARQL query route.
	 */
	private void routeSparql() {
		this.from("direct:sparql")
			.routeId("sparql")
			.process(MessageTranslatorRequestedUrlNormalizerProtocol.ID)
			.process(MessageTranslatorRequestedUrlNormalizerActor.ID)
			.process(MessageTranslaterDetermineActor.ID) 
			.process(ContentBasedRouterByActorMatchCurrentUser.ID)
			.process(ContentEnricherSparql.ID)
			.endRest();
	}
}
