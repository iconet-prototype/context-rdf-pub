/**
 * Provides information about the actor. Also known as actor profile
 */
package org.linkedopenactors.rdfpub.usecases.readactor;
