package org.linkedopenactors.rdfpub.usecases.readobject;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.StringWriter;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.APException.ErrorType;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import lombok.extern.slf4j.Slf4j;

/**
 * Extends the incoming message with the rdf statements that are stored in the
 * database for the requested url. Based on the <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/DataEnricher.html">Content
 * Enricher</a> pattern.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
@Slf4j
class ContentEnricherReadObjectFromActor extends AbstractApProcessor {
	
	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentEnricherReadObjectFromActor.class.getSimpleName());
	private static final String CAMEL_HTTP_URL = "CamelHttpUrl";

	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		ApRepository apRepository = getApRepository(in.getActorId());
		String requestedObjectUrlAsString = in.getRequestedUrl().stringValue();
		if(requestedObjectUrlAsString==null) {
			throw new APException("Header Property '"+CAMEL_HTTP_URL+"' missing.", ErrorType.bad_request);
		}
		Optional<ModelAndSubject> modelAndSubjectOptional = apRepository.readObject(iri(requestedObjectUrlAsString));
		Model model = modelAndSubjectOptional.map(modelAndSubject -> modelAndSubject.getModel())
				.orElseThrow(() -> new APException("'"+requestedObjectUrlAsString+"' not found", ErrorType.not_found));
		StringWriter modelAsTurtleStringWriter = new StringWriter();
		Rio.write(model, modelAsTurtleStringWriter, RDFFormat.TURTLE);
		log.trace("model("+requestedObjectUrlAsString+"): " + modelAsTurtleStringWriter.toString());
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.body(modelAsTurtleStringWriter)
			.build());
	}
}
