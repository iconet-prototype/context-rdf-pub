/**
 * Allows to search the outbox of the actor.
 * Querying an actor's database is not part of the activity pub specification,
 * but it is an important aspect therefore a query possibility of the outbox of
 * the actor is provided. The query language is
 * <a href="https://de.wikipedia.org/wiki/SPARQL">SPARQL</a>
 */
package org.linkedopenactors.rdfpub.usecases.queryactorsoutbox;
