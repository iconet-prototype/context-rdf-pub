package org.linkedopenactors.rdfpub.usecases.overlapping;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Throws exception if the current user does not match the actor in the
 * requested url. See <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/ContentBasedRouter.html">Content
 * Based Router</a>
 * 
 * We do not use a dead letter queue here, and notify the caller via an
 * exception. Since we cannot fix this error without information from the
 * author.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
@Slf4j
public class ContentBasedRouterByActorMatchCurrentUser extends AbstractApProcessor {

	public static String ID = StringUtils.uncapitalize(ContentBasedRouterByActorMatchCurrentUser.class.getSimpleName());
	private static final String CAMEL_HTTP_URL = "CamelHttpUrl";

	@Override
	public void process(Exchange exchange) {		
		ApMessage in = getIn(exchange);

		if(in.getRequestedUrl()==null) {
			throw new RuntimeException("Header Property '"+CAMEL_HTTP_URL+"' missing.");
		}
		String currentPreferedUserName = in.getCurrentPreferedUserName();
		String currentUserId = in.getCurrentUserId();
		String actorId = in.getActorId().getLocalName();
		if(!(actorId.equals(currentPreferedUserName) || actorId.equals(currentUserId))) {
			log.warn("("+getSpanAndTraceId()+") Forbidden - current user '"+currentUserId+"' is not allowed to request " + in.getRequestedUrl());
			addHttpStatusCodeAndTraceId(exchange, HttpStatus.FORBIDDEN);		
		}		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(in.getBody(String.class))
				.build());
	}
}
