package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.util.Set;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

/**
 * Set or replace the updated date in each object with the current dateTime.  
 * See <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html">MessageTranslator</a>
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
@Slf4j
class MessageTranslaterSetUpdateDate extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(MessageTranslaterSetUpdateDate.class.getSimpleName());
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		
		Model model = in.getModel();
		IRI activityIri = in.getActivityId();
		IRI actorIri = in.getActorId();
		log.debug("update activity: " + activityIri);
		getApRepository(actorIri).processUpdateOutboxActivity(activityIri, getValidatedObjectIri(activityIri, model),
				model);
	}
	
	public void updateActivity(IRI activityIri, IRI actorIri, Model model) {
		log.debug("update activity: " + activityIri);
		getApRepository(actorIri).processUpdateOutboxActivity(activityIri, getValidatedObjectIri(activityIri, model),
				model);
	}

	/**
	 * Determines the value of the {@link AS#object} property of the passed activity.  
	 * @param activityIri The activity to search for.
	 * @param model The model in which to search.
	 * @throws RuntimeException if there is not exact one value of the {@link AS#object} property. 
	 * @return The value of the {@link AS#object} property.
	 */
	private IRI getValidatedObjectIri(IRI activityIri, Model model) {
		Set<IRI> objects = Models.getPropertyIRIs(model, activityIri, AS.object);
		if(objects.size()!= 1) {
			throw new RuntimeException("exact one object expected! But was " + objects.size() + " - " + objects); // TODO allow mor ethan one ??
		}
		return objects.iterator().next();
	}
}
