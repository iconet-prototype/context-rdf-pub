package org.linkedopenactors.rdfpub.usecases.overlapping;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.linkedopenactors.rdfpub.repository.IdentityProviderUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Corrects user/actor.
 * <ul>
 *   <li>If the preferredUsername was included, it is replaced by the userId.</li>   
 * </ul>
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
public class MessageTranslatorRequestedUrlNormalizerActor extends AbstractApProcessor {
	
	public static String ID = StringUtils.uncapitalize(MessageTranslatorRequestedUrlNormalizerActor.class.getSimpleName());
	
	@Value( "${app.context}" )
	private String context;

	@Value( "${app.https}" )
	private Boolean https;

	@Autowired
	private IdentityProvider identityProvider;
	
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		IRI requstedUrlIri = in.getRequestedUrl();
		try {
			exchange.setMessage(ApMessageBuilder.getInstance(exchange)
					.headers(in.getHeaders())
					.requestedUrl(normalizeActor(requstedUrlIri.stringValue()))
					.body(in.getBody(String.class))
					.build()
					);
		} catch (MalformedURLException e) {
			throw new RuntimeException("in URL '"+requstedUrlIri.stringValue()+"' is invalid", e);
		}
	}

	/**
	 * If the preferredUsername was included, it is replaced by the userId.
	 * @param requstedUrl the url that was requested by the client
	 * @return the requestedUrl, but if the preferredUsername was included, it was replaced with the userId.
	 * @throws MalformedURLException if the requstedUrl is not a valid {@link URL} 
	 */
	private IRI normalizeActor(String requstedUrl) throws MalformedURLException {
		String actorName = MessageTranslaterUrl.extractActorName(context, requstedUrl, MessageTranslaterUrl.extractBaseUrl(context,  https,  requstedUrl));
		List<IdentityProviderUser> users = identityProvider.search(actorName);
		if(users.size()!=1) {
			throw new RuntimeException("identityProvider.search("+actorName+") return not exact one user but: " + users);
		}			
		String userId = users.get(0).getUserId();
		
		IRI normalizedIri = iri(requstedUrl.replace(actorName, userId));
		return normalizedIri;
	}
}
