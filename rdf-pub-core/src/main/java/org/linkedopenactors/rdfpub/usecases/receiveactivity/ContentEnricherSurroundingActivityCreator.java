package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.RdfTypeRepository;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * See https://www.w3.org/TR/activitypub/#object-without-create For client to
 * server posting, it is possible to submit an object for creation without a
 * surrounding activity. The server MUST accept a valid [ActivityStreams] object
 * that isn't a subtype of Activity in the POST request to the outbox. The
 * server then MUST attach this object as the object of a Create Activity. For
 * non-transient objects, the server MUST attach an id to both the wrapping
 * Create and its wrapped Object.
 */
@Component
@AllArgsConstructor
@Slf4j
class ContentEnricherSurroundingActivityCreator extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentEnricherSurroundingActivityCreator.class.getSimpleName());

	private RdfTypeRepository rdfTypeRepository;
	
	@Override
	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		Model message = in.getModel();
		ModelAndSubject result = surround(in.getActorId(), message);
		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.activityIri(result.getSubject())
			.body(result.getModel())
			.build());
	}
	
	/**
	 * See description of {@link ContentEnricherSurroundingActivityCreator}
	 * @param actorIri The actor, that was sending the data (hopefully an activity, an object or both)
	 * @param model the data send by the actor
	 * @return If the data was an object without an activity, the data with a surrounding activity, otherwise the unchanged data.
	 */
	private ModelAndSubject surround(IRI actorIri, Model model) {
		ModelAndSubject result;
		Optional<IRI> activityIRIOptional = extractActivityIRI(model);			
		if(activityIRIOptional.isEmpty()) {	
			log.trace("surroundObjectsWithActivityIfNecessary: It's necessary!");			
			ModelAndSubject surroundingActivityAndSubject = createSurroundingActivity(actorIri, model);
			Model createdActivityModel = surroundingActivityAndSubject.getModel();
			model.addAll(createdActivityModel);
			result = new ModelAndSubject(surroundingActivityAndSubject.getSubject(), model);
		} else  {
			result = new ModelAndSubject(activityIRIOptional.get(), model);
		}
		return result;
	}
	
	/**
	 * Searches the statement of the activity and return it's subject.
	 * @throws RuntimeException if there is more than one activity.
	 * @param model the graph that must contain an activity
	 * @return The subject (IRI) of the contained activity or {@link Optional#empty()} if there is no {@link AS#Activity} in the model.
	 */
	private Optional<IRI> extractActivityIRI(Model model) {
		Optional<IRI> result = Optional.empty();
		List<IRI> activities = getContainedTypeIris(model,AS.Activity);
		log.trace("extractActivityIRI - getContainedTypeIris: " + activities);
		if(activities.size()>1) {
			log.error("More than one activity is not allowed in one request!");
			throw new RuntimeException("More than one activity is not allowed in one request!");			
		} else if(activities.size()==1) {
			return Optional.of(activities.get(0));
		} else {
			ModelLogger.trace(log, model, "extractActivityIRI does not find any activities in the model");
			log.trace("model does not contain activity. ContainedTypeIris: " + activities);
		}
		return result;
	}

	/**
	 * Creates a surrounding activity if the client-app was not sending one.
	 * @param asObjects The data send from the client
	 * @param actorIri The IRI of the actor that was sending the activity
	 * @return A newly created surrounding activity with it's subject IRI.
	 */
	private ModelAndSubject createSurroundingActivity(IRI actorIri, Model asObjects) {
		IRI activityIri = Values.iri("http://isReplacedAnyway");
		ModelBuilder mb = new ModelBuilder();
		mb.subject(activityIri)
				.add(RDF.TYPE, AS.Create)
				.add(AS.name, Values.literal("Object(s) '"+getNamesOfAllAsObjects(asObjects)+"' created."))
				.add(AS.summary, Values.literal("This activity wraps an object that was created without a surrounding activity."))
				.add(AS.actor, actorIri)
				.add(AS.published, Values.literal(Instant.now().toString()));
		IRI rootObject = findRoot(asObjects);		
		mb.add(AS.object, rootObject);		 
		return new ModelAndSubject(activityIri, mb.build());
	}
	
	/**
	 * Checks, if the passed object is derived from AsObject.
	 * @param objectIri The subject iri of the object to check.
	 * @param model the model containing the object to check.
	 * @throws RuntimeException if T derived from AsObject.
	 */
	private void throwExceptionIfNoAsObject(IRI objectIri, Model model) {
		Set<IRI> objectTypes = Models.getPropertyIRIs(model, objectIri, RDF.TYPE);
		if(objectTypes.stream().filter(objectType->rdfTypeRepository.isSubclassOf(objectType, AS.Object)).count()<1) {
			String typesAsString = objectTypes.stream().map(IRI::stringValue).collect(Collectors.joining(","));
			String msg = "The objects types in the activity is '" + typesAsString + "' but it has to be a/derived from " + AS.Object;
			ModelLogger.error(log, model, msg);			
			throw new RuntimeException(msg);
		}
	}

	/**
	 * Searches the 'root' object, that is not referenced by another one.
	 * @throws RuntimeException if there is not exact one 'root' object.   
	 * @param model The data around the activity
	 * @return The subject of the activity
	 */
	private IRI findRoot(Model model) {		
		Set<IRI> subjects = model.stream().map(stmt->(IRI)stmt.getSubject()).collect(Collectors.toSet());
		Set<IRI> unreferenced = new HashSet<>(); 
		for (IRI subject : subjects) {
			Set<IRI> references = model.filter(null, null, subject)
					.stream()
					.map(stmt->(IRI)stmt.getSubject())
					.collect(Collectors.toSet());
			if(references.isEmpty()) {
				unreferenced.add(subject);
			}
		}
		if(unreferenced.size()!= 1) {
			throw new RuntimeException("unable to determine exact one 'root' object, that can be added to the activity. Found: " + unreferenced.size());
		}		
		IRI root = unreferenced.stream().findFirst().get();
		throwExceptionIfNoAsObject(root, model);
		return root;
	}

	/**
	 * @param model The model to search for {@link AS#name} properties
	 * @return All {@link AS#name} properties of the objects in the passed model as comma seperated string.
	 */
	private String getNamesOfAllAsObjects(Model model) {		
		return model.stream()
				.map(stmt->(IRI)stmt.getSubject())
				.map(objectIri->Models.getPropertyLiteral(model, objectIri, AS.name))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(Literal::stringValue)
				.collect(Collectors.joining(", "));
	}

	/**
	 * Filter all subjects (IRI) with predicate RDF.TYPE equals the passed type.  
	 * @param model model to filter.
	 * @param type type to search. 
	 * @return All Objects with predicate RDF.type that are a subclass of the passed type.
	 */
	private List<IRI> getContainedTypeIris(Model model, IRI type) {
		List<IRI> result = new ArrayList<>();
		model.filter(null, RDF.TYPE, null).forEach(stmt -> {
			boolean isSublassOfPassedType = rdfTypeRepository.isSubclassOf(Values.iri(stmt.getObject().stringValue()), type);
			if (isSublassOfPassedType) {
				result.add((IRI) stmt.getSubject());
			}
		});
		return result;
	}
}
