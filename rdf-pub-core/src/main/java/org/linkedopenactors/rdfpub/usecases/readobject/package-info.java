/**
 * Provides information about a object. This may be a <a href=
 * "https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object">Object</a>,
 * but can also be some other rdf object.
 */
package org.linkedopenactors.rdfpub.usecases.readobject;
