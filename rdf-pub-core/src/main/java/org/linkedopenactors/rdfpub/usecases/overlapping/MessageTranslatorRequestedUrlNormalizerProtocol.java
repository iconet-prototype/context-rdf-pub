package org.linkedopenactors.rdfpub.usecases.overlapping;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Corrects protocol
 * <ul>
 *   <li>Corrects the protocol depending on flag {@link MessageTranslatorRequestedUrlNormalizerProtocol#https} to http or https.</li>
 * </ul>
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
public class MessageTranslatorRequestedUrlNormalizerProtocol extends AbstractApProcessor {
	
	public static String ID = StringUtils.uncapitalize(MessageTranslatorRequestedUrlNormalizerProtocol.class.getSimpleName());
	
	@Value( "${app.https}" )
	private Boolean https;

	public void process(Exchange exchange) {
		ApMessage in = getIn(exchange);
		IRI requstedUrlIri = in.getRequestedUrl();
		try {
			exchange.setMessage(ApMessageBuilder.getInstance(exchange)
					.headers(in.getHeaders())
					.requestedUrl(normalizeProtocol(requstedUrlIri))
					.body(in.getBody(String.class))
					.build()
					);
		} catch (MalformedURLException e) {
			throw new RuntimeException("in URL '"+requstedUrlIri.stringValue()+"' is invalid", e);
		}
	}
	
	/**
	 * Corrects the protocol depending on flag {@link MessageTranslatorRequestedUrlNormalizerProtocol#https} to http or https.
	 * @param requstedUrlIri the url that was requested by the client
	 * @return Depending on flag {@link MessageTranslatorRequestedUrlNormalizerProtocol#https} the protocol is corrected to http or https.
	 * @throws MalformedURLException if the requstedUrl is not a valid {@link URL}
	 */
	private IRI normalizeProtocol(IRI requstedUrlIri) throws MalformedURLException {
		URL url = new URL(requstedUrlIri.stringValue());
		String port = url.getPort()!=-1 ? ":" + url.getPort() : "";
		String base = https ? "https" : "http";
		String normalizedUrl= base + "://"
				+ url.getHost()
				+ port
				+ url.getPath();
		return iri(normalizedUrl);
	}
}
