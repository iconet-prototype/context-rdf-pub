package org.linkedopenactors.rdfpub.usecases.readactor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.linkedopenactors.rdfpub.camel.AbstractApProcessor;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.springframework.stereotype.Component;

/**
 * Extends the incoming message with the rdf statements that are stored in the
 * database for the actor's id (requested url). Based on the <a href=
 * "https://www.enterpriseintegrationpatterns.com/patterns/messaging/DataEnricher.html">Content
 * Enricher</a> pattern.
 * 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Component
class ContentEnricherReadActor extends AbstractApProcessor {

	/**
	 * The id of this {@link Processor} 
	 */
	public static String ID = StringUtils.uncapitalize(ContentEnricherReadActor.class.getSimpleName());
	
	public void process(Exchange exchange) {
		ApRepository actorsRepository = getApRepository(getIn(exchange).getActorId());
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(actorsRepository.getProfile())
				.build());
	}
}
