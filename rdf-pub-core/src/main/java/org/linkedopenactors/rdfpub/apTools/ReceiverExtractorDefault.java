package org.linkedopenactors.rdfpub.apTools;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.linkedopenactors.rdfpub.repository.IdentifyRecipients;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;

@Component
public class ReceiverExtractorDefault implements ReceiverExtractor, IdentifyRecipients {

	@Override
	public Set<IRI> extractInternalReceivers(IRI actorId, ModelAndSubject modelAndSubject) {
		return extractReceivers(modelAndSubject).stream()
				.filter(receiverId -> sameInstance(actorId, receiverId) || receiverId.equals(AS.Public))
				.collect(Collectors.toSet());
	}

	/**
	 * Checks, if the passed receiver has the same namespace as the actor that was creating the activity.
	 * @param actorId The actor that was creating the activity.
	 * @param receiverId The receiver to check.
	 * @return True, if the receiver has the same namespace as the actor.
	 */
	private boolean sameInstance(IRI actorId, IRI receiverId) {
		return receiverId.stringValue().startsWith(actorId.getNamespace());
	}

	private Set<IRI> extractReceivers(ModelAndSubject modelAndSubject) {
		return extractReceiverStatements(modelAndSubject)
				.stream()
				.map(receiverStatement -> iri(receiverStatement.getObject().stringValue()))
				.collect(Collectors.toSet());
	}
	
	private Set<Statement> extractReceiverStatements(ModelAndSubject modelAndSubject) {
		IRI subject = modelAndSubject.getSubject();
		Model model = modelAndSubject.getModel();
		
		Set<Statement> receiverStatements = new HashSet<>();		
		receiverStatements.addAll(model.filter(subject, AS.to, null));
		receiverStatements.addAll(model.filter(subject, AS.bto, null));
		receiverStatements.addAll(model.filter(subject, AS.cc, null));
		receiverStatements.addAll(model.filter(subject, AS.bcc, null));
		receiverStatements.addAll(model.filter(subject, AS.audience, null));
		return receiverStatements;
	}

	@Override
	public Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject) {
		return extractInternalReceivers(actorId, modelAndSubject);
	}
}
