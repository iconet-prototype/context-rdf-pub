package org.linkedopenactors.rdfpub.apTools;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

public interface ReceiverExtractor {

	Set<IRI> extractInternalReceivers(IRI actorId, ModelAndSubject modelAndSubject);

}