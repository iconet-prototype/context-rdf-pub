package org.linkedopenactors.rdfpub.http;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * A servlet filter to log request and response
 * The logging implementation is pretty native and for demonstration only
 * @author hemant
 *
 */
@Component
@Order(2)
public class RequestResponseLoggingFilter implements Filter {

	private final static Logger LOG = LoggerFactory.getLogger(RequestResponseLoggingFilter.class);

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		LOG.info("Initializing filter :{}", this);
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		LOG.info("Logging Request  {} : {}", req.getMethod(), req.getRequestURI());
		for (Iterator<String> iterator = req.getHeaderNames().asIterator(); iterator.hasNext();) {
			String headerName = iterator.next();
			LOG.trace("request header: " + headerName + " - " + req.getHeader(headerName));
		}
		
		chain.doFilter(request, response);
		String status = HttpStatus.valueOf(res.getStatus()).toString();
		LOG.info("Logging Response : status: {}, contentType: {}", status, res.getContentType());
		for (String headerName : res.getHeaderNames()) {
			LOG.trace("response header: " + headerName + " - " + res.getHeader(headerName));
		}		
	}

	@Override
	public void destroy() {
		LOG.warn("Destructing filter :{}", this);
	}
}