/**
 * The base package of the rdf-pub server implementation. The rdf-pub server is a <a href="https://activitypub.rocks/">activity-pub</a> server.
 * The implementation follows the <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/PipesAndFilters.html">Pipes &amp; Filters</a> pattern.
 * 
 */
package org.linkedopenactors.rdfpub;
