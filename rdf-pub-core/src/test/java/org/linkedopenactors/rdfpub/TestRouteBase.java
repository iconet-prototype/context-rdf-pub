package org.linkedopenactors.rdfpub;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.camel.ApMessage;
import org.linkedopenactors.rdfpub.camel.SimpleApMessage;
import org.linkedopenactors.rdfpub.config.ApConfig;
import org.linkedopenactors.rdfpub.identity.CurrentUser;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.repository.ApRepositoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@CamelSpringBootTest
@EnableAutoConfiguration
@SpringBootTest(
    properties = { "camel.springboot.name=customName" })
@ComponentScan(basePackages = {"org.linkedopenactors.rdfpub.repository", 
		"org.linkedopenactors.rdfpub"})
@ActiveProfiles(ApConfig.PROFILE_UNIT_TEST)
public class TestRouteBase 
{	
	@Autowired
	protected List<TestUser> users;

	@Autowired
	protected ApRepositoryManager apRepositoryManager;

	@Autowired
	protected CurrentUser currentUser;
	
	@Autowired
	protected CamelContext camelContext;

	protected IRI getCurrentActorIri() {
		IRI actorIri = iri("http://localhost:8080/camel/" + currentUser.getUserId().orElseThrow());
		apRepositoryManager.createActorsRepository4Person(actorIri);
		return actorIri;
	}
	
	protected IRI getOutbox(IRI actorIri) {
		return iri(actorIri.stringValue() + "/outbox");
	}
	
	protected IRI getPublicCollection(IRI actorIri) {
		return iri(actorIri.getNamespace() + "asPublic");
	}

	protected ApMessage getMessage(Exchange exchange) {
		return new SimpleApMessage(exchange.getMessage());
	}
	
	protected IRI getCurrentUserIri() {
		return iri("http://localhost:8080/camel/"+ currentUser.getUserId().orElseThrow());	
	}
	
	protected IRI getRandomActorIri() {
		assertNotNull(users);
		TestUser randomUser = users.get(getRandomInt(users.size()-1));
		while(currentUser.getUserId().orElseThrow().equals(randomUser.getUserId())) {
			randomUser = users.get(getRandomInt(users.size()-1));
		}		
		return iri("http://localhost:8080/camel/"+ randomUser.getUserId());
	}
	
	private int getRandomInt(int max) {
	      int min = 0;
	      return (int)Math.floor(Math.random()*(max-min+1)+min);
	    }
	
	protected ApRepository createRepo(IRI actorIri) {
		return apRepositoryManager.createActorsRepository4Person(actorIri);	
	}	
}
