package org.linkedopenactors.rdfpub;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.overlapping.InMessageTranslaterToTurtle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.DirtiesContext;

public class TestInMessageTranslaterToTurtle extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorInMsgTranslatorToTurtleRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(InMessageTranslaterToTurtle.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutContentType() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertTrue(responseExchange.getException().getMessage().startsWith("Unsuported Content type. use one of '"));
	}
	
	@DirtiesContext
	@Test
	public void testUnsupportedContentType() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setHeader(HttpHeaders.CONTENT_TYPE, "*/*");
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertTrue(responseExchange.getException().getMessage().startsWith("Unsuported Content type. use one of '"));
	}
	
	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setHeader(HttpHeaders.CONTENT_TYPE, "text/turtle");

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertTrue(responseExchange.getException().getMessage().startsWith("Error converting 'Turtle' Message to 'Turtle' Did you specify the correct content type as http header? SupportedMimeTypes: "));
	}
	
	@DirtiesContext
	@Test
	public void testWitWrongMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setHeader(HttpHeaders.CONTENT_TYPE, "text/turtle");
		inMessage.setBody("qasdf qwefqwfeqw qwefqwef");
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertTrue(responseExchange.getException().getMessage().startsWith("Error converting 'Turtle' Message to 'Turtle' Did you specify the correct content type as http header? SupportedMimeTypes: "));
	}

	@DirtiesContext
	@Test
	public void testWitJsonLd() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setHeader(HttpHeaders.CONTENT_TYPE, "application/ld+json");
		inMessage.setBody("[ {\n"
				+ "  \"@id\" : \"http://localhost:8080/camel/test/tester/12345\",\n"
				+ "  \"@type\" : [ \"https://www.w3.org/ns/activitystreams#Object\" ],\n"
				+ "  \"https://www.w3.org/ns/activitystreams#name\" : [ {\n"
				+ "    \"@value\" : \"Max\"\n"
				+ "  } ]\n"
				+ "} ]");
		Exchange responseExchange = template.send(exchange);
		
		assertEquals(responseExchange.getMessage().getHeader(HttpHeaders.CONTENT_TYPE), "text/turtle");
		String turtle = "<http://localhost:8080/camel/test/tester/12345> a <https://www.w3.org/ns/activitystreams#Object>;\n"
				+ "  <https://www.w3.org/ns/activitystreams#name> \"Max\" .";
		assertEquals(turtle, responseExchange.getMessage().getBody().toString().trim());
	}
}
