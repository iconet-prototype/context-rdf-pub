package org.linkedopenactors.rdfpub;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.charset.StandardCharsets;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.linkedopenactors.rdfpub.usecases.overlapping.OutMessageTranslater;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.DirtiesContext;

class TestOutMessageTranslater extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorOutMsgTranslatorRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(OutMessageTranslater.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutAcceptHeader() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL! (internal_server_error/500)", responseExchange.getException().getMessage());
		// ^ because a default content-type is used !
	}

	@DirtiesContext
	@Test
	public void testWithWrongAcceptHeader() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getMessage().setHeader(HttpHeaders.ACCEPT, "*/*");
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL! (internal_server_error/500)", responseExchange.getException().getMessage());
		// ^ because a default content-type is used !
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getMessage().setHeader(HttpHeaders.ACCEPT, "application/ld+json");
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL! (internal_server_error/500)", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithWrongMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getMessage().setHeader(HttpHeaders.ACCEPT, "application/ld+json");
		exchange.getMessage().setBody("ölkjjh piuh poi gb");
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("Expected ':', found ' ' [line 1] (bad request/400)", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithJsonLdMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getMessage().setHeader(HttpHeaders.ACCEPT, "application/ld+json");
		exchange.getMessage().setBody("<http://localhost:8080/camel/test/tester/12345> a <https://www.w3.org/ns/activitystreams#Object>;\n"
				+ "  <https://www.w3.org/ns/activitystreams#name> \"Max\" .\n"
				+ "");
		
		Exchange responseExchange = template.send(exchange);
		
		String expected = new String(
				new ClassPathResource("TestProcessorOutMsgTranslator.jsonld").getInputStream().readAllBytes(),
				StandardCharsets.UTF_8);

		assertEquals(expected.trim(), responseExchange.getMessage().getBody(String.class).trim());
	}
}
