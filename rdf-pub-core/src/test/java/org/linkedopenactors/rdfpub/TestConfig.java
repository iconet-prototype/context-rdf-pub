package org.linkedopenactors.rdfpub;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.linkedopenactors.rdfpub.identity.CurrentUser;
import org.linkedopenactors.rdfpub.repository.IdentityProvider;
import org.linkedopenactors.rdfpub.repository.IdentityProviderUser;
import org.slf4j.Logger;
import org.slf4j.event.Level;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
class TestConfig {

	@Bean
	public List<TestUser> getTestUsers() {
		return List.of(new TestUser("0815", "Max")
				,new TestUser("0816", "Udo"));
	}
	
	@Bean
	@ConditionalOnProperty(name="app.unittest", havingValue="true")
	public IdentityProvider getIdentityProvider() {			
			return new IdentityProvider() {
				
				@Override
				public List<IdentityProviderUser> search(IRI actorId) {
					return search(actorId.getLocalName());
				}

				@Override
				public void logIdentityProviderProperties(Logger logger, Level level) {
					logIdentityProviderProperties(logger, level, "logIdentityProviderProperties");
				}

				@Override
				public void logIdentityProviderProperties(Logger logger, Level level, String msgParam) {
					String msg = msgParam + " - DUMMY";
					switch (level) {
					case INFO:
						logger.info(msg);			
						break;

					case DEBUG:
						logger.debug(msg);			
						break;

					case TRACE:
						logger.trace(msg);			
						break;

					case WARN:
						logger.warn(msg);			
						break;

					case ERROR:
						logger.error(msg);			
						break;

					default:
						logger.trace(msg);
						break;
					}
				}

				@Override
				public IRI normalize(IRI actorId) {
					return getTestUsers().stream().filter(user->user.getUserId().equals(actorId.getLocalName())
								|| user.getPreferredUserName().equals(actorId.getLocalName()))
							.map(user->user.getUserId())
							.map(userId->actorId.getNamespace() + userId)
							.map(Values::iri)
							.findFirst()
							.orElseThrow(()->new RuntimeException("IdentityProvider did not find exact one user for actor iri '"+actorId+"' but :[]"));
				}

				@Override
				public List<IdentityProviderUser> search(String actorName) {
					if(!StringUtils.hasText(actorName)) {
						log.warn("search was called with empty name!");	
						return Collections.emptyList();
					}
					List<TestUser> preferedUserNames = getTestUsers().stream().filter(user->user.getPreferredUserName().equals(actorName)).collect(Collectors.toList());
					if(!preferedUserNames.isEmpty()) {
						return List.of(new IdentityProviderUser( preferedUserNames.get(0).getUserId(), preferedUserNames.get(0).getPreferredUserName())); 
					}
					List<TestUser> userIds = getTestUsers().stream().filter(user->user.getUserId().equals(actorName)).collect(Collectors.toList());
					if(!userIds.isEmpty()) {
						return List.of(new IdentityProviderUser( userIds.get(0).getUserId(), userIds.get(0).getPreferredUserName())); 
					}					
					return List.of(new IdentityProviderUser( TestProcessorBase.actorIri.getLocalName(), "0820"));
				}

				@Override
				public Optional<IdentityProviderUser> getUser(IRI actorId) {
					List<IdentityProviderUser> users = search(actorId);
					if(users.size()>1) {
						throw new RuntimeException("more than one user for " + actorId);
					} else {
						return users.stream().findFirst();
					}
				}
			};
	}
	
	@Bean
	@ConditionalOnProperty(name="app.unittest", havingValue="true")	   
	public CurrentUser getCurrentUserMock() {
		TestUser user = getTestUsers().get(0);
		return new CurrentUser() {
			
			@Override
			public Optional<String> getUserId() {
				return Optional.ofNullable(user.getUserId());
			}
			
			@Override
			public Optional<String> getPreferedUserName() {
				return Optional.ofNullable(user.getPreferredUserName());
			}
		};
	}
}