package org.linkedopenactors.rdfpub;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslaterDetermineActor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

class TestMessageTranslaterDetermineActor extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorDetemineActorRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslaterDetermineActor.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutRequestedUrl() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getIn().setHeader("actorIri", iri("http://example.org/tester"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'CamelHttpUrl'. headers: {actorIri=http://example.org/tester}", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithoutUnknownActor() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getIn().setHeader(ActivityMessageHeaders.REQUESTED_URL, iri("http://localhost:8080/camel/max/018277"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("IdentityProvider did not find exact one user for actor iri 'http://localhost:8080/camel/max' but :[]", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testSuccessWithUserId() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		TestUser user = this.users.get(0);
		exchange.getIn().setHeader(ActivityMessageHeaders.REQUESTED_URL, iri("http://localhost:8080/camel/"+user.getUserId()+"/018277"));
		
		Exchange responseExchange = template.send(exchange);
	
		assertNull(responseExchange.getException());
		IRI activityOwner = (IRI)responseExchange.getMessage().getHeader(ActivityMessage.ACTOR_IRI);
		assertNotNull(activityOwner);
		assertTrue(activityOwner.stringValue().endsWith(user.getUserId()));		
	}
	
	@DirtiesContext
	@Test
	public void testSuccessWithPreferedUserName() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		TestUser user = this.users.get(0);
		exchange.getIn().setHeader(ActivityMessageHeaders.REQUESTED_URL, iri("http://localhost:8080/camel/"+user.getPreferredUserName()+"/018277"));
		
		Exchange responseExchange = template.send(exchange);
	
		assertNull(responseExchange.getException());
		IRI activityOwner = (IRI)responseExchange.getMessage().getHeader(ActivityMessage.ACTOR_IRI);
		assertNotNull(activityOwner);
		assertTrue(activityOwner.stringValue().endsWith(user.getUserId()));		
	}
}
