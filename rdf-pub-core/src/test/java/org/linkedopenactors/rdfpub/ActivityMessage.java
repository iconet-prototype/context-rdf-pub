package org.linkedopenactors.rdfpub;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Optional;

import org.apache.camel.Message;
import org.apache.camel.support.DefaultMessage;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActivityMessage extends DefaultMessage {

	public static final String ACTOR_IRI = "actorIri";
	
	public ActivityMessage(Message message) {
		super(message.getExchange());
		this.setHeaders(message.getHeaders());
		this.setBody(message.getBody());
	}

	public void setActorIri(IRI actorIri) {
		setHeader(ACTOR_IRI, actorIri);
	}

	public IRI getActorIri() {		
		return getMandatoryIriProperty(ACTOR_IRI);
	}
	
	public void setActivity(IRI activity) {
		setHeader(ActivityMessageHeaders.ACTIVITY_IRI, activity);
	}
	
	public IRI getActivity() {		
		return getMandatoryIriProperty(ActivityMessageHeaders.ACTIVITY_IRI);
	}

	private <T> Optional<T> getProperty(String name, Class<T> type) {
		return Optional.ofNullable(getHeader(name, type));
	}

	public Optional<Boolean> getBooleanProperty(String name) {
		return getProperty(name, Boolean.class);
	}	

	private <T> T getMandatoryProperty(String name, Class<T> type) {
		return Optional.ofNullable(getHeader(name, type)).orElseThrow(()->new RuntimeException("Message did not define header '" + name + "'. headers: " + getHeaders()));
	}

	public IRI getMandatoryIriProperty(String name) {
		return getMandatoryProperty(name, IRI.class);
	}

	public void setBody(Model model) {
		StringWriter modelTurtleWriter = new StringWriter();
		Rio.write(model, modelTurtleWriter, RDFFormat.TURTLE);
		this.setBody(modelTurtleWriter.toString());
	}
	
	public Model getModel() {
		String messageBody = getBody(String.class);
		if(messageBody==null) {
			throw new RuntimeException("message is NULL");
		}
		StringReader sr = new StringReader(messageBody);
		try {
			return Rio.parse(sr, RDFFormat.TURTLE);
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			log.error(messageBody);
			throw new RuntimeException("error parsing message body into Turtle RDF Model", e);
		}
	}

	public void setType(IRI activityType) {
		setHeader(ActivityMessageHeaders.ACTIVITY_TYPE_IRI, activityType);		
	}
	
	public IRI getActivityType() {		
		return getMandatoryIriProperty(ActivityMessageHeaders.ACTIVITY_TYPE_IRI);
	}
	
	public void setSaved(boolean saved) {
		setHeader(ActivityMessageHeaders.ACTIVITY_SAVED, saved);
	}
	
	public boolean isSaved() {		
		return getBooleanProperty(ActivityMessageHeaders.ACTIVITY_SAVED).orElse(false);
	}
}
