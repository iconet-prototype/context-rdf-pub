package org.linkedopenactors.rdfpub;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;
import org.linkedopenactors.rdfpub.usecases.overlapping.ContentBasedRouterByActorMatchCurrentUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

class TestContentBasedRouterByActorMatchCurrentUser extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorActorMatchCurrentUserRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentBasedRouterByActorMatchCurrentUser.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutActorIri() throws Exception {
		createRepo(actorIri);		
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.currentPreferedUserName("max")
				.currentUserId(actorIri.getLocalName())
				.build());
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {CamelHttpUrl=http://example.org/"+actorIri.getLocalName()+"/inbox, currentPreferedUserName=max, currentUserId="+actorIri.getLocalName()+"}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutRequestedUrl() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.build());
		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'CamelHttpUrl'. headers: {actorIri="+actorIri+", collection=inbox}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutCurrentPreferedUserName() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.build());
		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'currentPreferedUserName'. headers: {actorIri="+actorIri+", CamelHttpUrl="+actorIri+"/inbox, collection=inbox}", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithoutCurrentUserId() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.currentPreferedUserName("testUser")
				.build());
		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'currentUserId'. headers: {actorIri="+actorIri+", CamelHttpUrl="+actorIri+"/inbox, collection=inbox, currentPreferedUserName=testUser}", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithoutWrongCurrentUserId() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		String currentUserId = "0815";
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.currentPreferedUserName("testUser")
				.currentUserId(currentUserId)
				.build());
		Exchange responseExchange = template.send(exchange);
		assertEquals(403, responseExchange.getMessage().getHeader(Exchange.HTTP_RESPONSE_CODE));
		assertEquals("Forbidden", responseExchange.getMessage().getHeader(Exchange.HTTP_RESPONSE_TEXT));
	}

	@DirtiesContext
	@Test
	public void testSuccessWithCurrentUserId() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.currentPreferedUserName("testUser")
				.currentUserId(actorIri.getLocalName())
				.build());
		Exchange responseExchange = template.send(exchange);

		assertNull(responseExchange.getException());
	}

	@DirtiesContext
	@Test
	public void testSuccessWithCurrentPreferedUserName() throws Exception {
		createRepo(actorIri);		

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.collection(AvailableCollection.inbox.name())
				.requestedUrl(iri(actorIri.stringValue() + "/inbox"))
				.currentPreferedUserName(actorIri.getLocalName())
				.currentUserId("testUser")
				.build());
		Exchange responseExchange = template.send(exchange);

		assertNull(responseExchange.getException());
	}
}
