package org.linkedopenactors.rdfpub.usecases.readactor;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.StringReader;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

class TestContentEnricherReadActor extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorGetActorsProfileRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentEnricherReadActor.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(actorIri);
		Exchange responseExchange = template.send(exchange);
		Model m = Rio.parse(new StringReader(responseExchange.getMessage().getBody().toString()), RDFFormat.TURTLE);
		IRI inbox = Models.getPropertyIRI(m, actorIri, AS.inbox).orElseThrow();
		IRI outbox = Models.getPropertyIRI(m, actorIri, AS.outbox).orElseThrow();
		IRI type = Models.getPropertyIRI(m, actorIri, RDF.TYPE).orElseThrow();
		assertEquals(iri(actorIri.stringValue() + "/inbox"), inbox);
		assertEquals(iri(actorIri.stringValue() + "/outbox"), outbox);
		assertEquals(AS.Person, type);
	}
}
