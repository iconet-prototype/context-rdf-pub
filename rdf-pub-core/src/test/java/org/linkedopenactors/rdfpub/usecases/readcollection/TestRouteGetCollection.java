package org.linkedopenactors.rdfpub.usecases.readcollection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestRouteBase;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestRouteGetCollection extends TestRouteBase {	
	
	@Produce(value = "direct:readCollection")
	protected ProducerTemplate template;
	
	@DirtiesContext
	@Test
	public void testGetOutboxWithWrongUser() throws Exception {
		IRI actorIri = getRandomActorIri();
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.requestedUrl(getOutbox(actorIri))
				.collection(getOutbox(actorIri).getLocalName())
				.build());

		Exchange responseExchange = template.send(exchange);
		assertEquals(403, responseExchange.getMessage().getHeader(Exchange.HTTP_RESPONSE_CODE));
		assertEquals("Forbidden", responseExchange.getMessage().getHeader(Exchange.HTTP_RESPONSE_TEXT));
	}
	
	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		IRI actorIri = getCurrentActorIri();
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.requestedUrl(getOutbox(actorIri))
				.collection(getOutbox(actorIri).getLocalName())
				.build());

		Exchange responseExchange = template.send(exchange);
	
		assertNull(responseExchange.getException());
				
		Model model = getMessage(responseExchange).getModel();
		ModelLogger.debug(log, model, "response: ");
		Model subjectModel = model.filter(null, RDF.TYPE, AS.OrderedCollectionPage);
		assertEquals(1, subjectModel.stream().count());
		IRI collectionSubject = (IRI)subjectModel.stream().findFirst().orElseThrow().getSubject();
		
		assertEquals("collection: outbox; pageSize: 100; startIndex: 0", 
				Models.getPropertyLiteral(model, collectionSubject, AS.summary).orElseThrow().stringValue());

		assertEquals(getOutbox(actorIri), 
				Models.getPropertyIRI(model, collectionSubject, AS.partOf).orElseThrow());
	}
}
