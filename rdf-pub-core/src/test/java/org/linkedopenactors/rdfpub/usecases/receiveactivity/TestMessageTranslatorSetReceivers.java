package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestMessageTranslatorSetReceivers extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessSetReceiversRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorSetReceivers.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(iri("http://example.com/actor"));
		inMessage.setActivity(iri("http://example.com/activity"));

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		IRI subjectOfAsObject1 = iri("http://example.org/testObject1");		
		IRI subjectOfAsObject2 = iri("http://example.org/testObject2");
		IRI subjectOfAsObject3 = iri("http://example.org/testObject3");
		IRI subjectOfAsObject4 = iri("http://example.org/testObject4");
		IRI cc = iri("http://example.org/testCC");
		IRI bcc = iri("http://example.org/testBCC");
		IRI to = iri("http://example.org/testTO");
		IRI bto = iri("http://example.org/testBTO");
		IRI audience = iri("http://example.org/testAudience");
		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, subjectOfAsObject1)
					.add(AS.cc, cc)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
					.add(AS.object, subjectOfAsObject2)
					.add(AS.bcc, bcc)
				.subject(subjectOfAsObject2)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.add(AS.name, "Max")
					.add(AS.to, to)
				.subject(subjectOfAsObject3)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.add(AS.name, "Max")
					.add(AS.bto, bto)
				.subject(subjectOfAsObject4)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.add(AS.name, "Max")
					.add(AS.audience, audience)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);		
		inMessage.setBody(model);
		inMessage.setActivity(iri("http://example.com/activity"));

		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());

		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());

		assertEquals(to.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfActivity, AS.to).orElseThrow().stringValue());
		assertEquals(to.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.to).orElseThrow().stringValue());
		assertEquals(to.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.to).orElseThrow().stringValue());
		assertEquals(to.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject3, AS.to).orElseThrow().stringValue());
		assertEquals(to.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject4, AS.to).orElseThrow().stringValue());

		assertEquals(bto.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfActivity, AS.bto).orElseThrow().stringValue());
		assertEquals(bto.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.bto).orElseThrow().stringValue());
		assertEquals(bto.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.bto).orElseThrow().stringValue());
		assertEquals(bto.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject3, AS.bto).orElseThrow().stringValue());
		assertEquals(bto.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject4, AS.bto).orElseThrow().stringValue());

		assertEquals(cc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfActivity, AS.cc).orElseThrow().stringValue());
		assertEquals(cc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.cc).orElseThrow().stringValue());
		assertEquals(cc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.cc).orElseThrow().stringValue());
		assertEquals(cc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject3, AS.cc).orElseThrow().stringValue());
		assertEquals(cc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject4, AS.cc).orElseThrow().stringValue());

		assertEquals(bcc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfActivity, AS.bcc).orElseThrow().stringValue());
		assertEquals(bcc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.bcc).orElseThrow().stringValue());
		assertEquals(bcc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.bcc).orElseThrow().stringValue());
		assertEquals(bcc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject3, AS.bcc).orElseThrow().stringValue());
		assertEquals(bcc.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject4, AS.bcc).orElseThrow().stringValue());

		assertEquals(audience.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfActivity, AS.audience).orElseThrow().stringValue());
		assertEquals(audience.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.audience).orElseThrow().stringValue());
		assertEquals(audience.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.audience).orElseThrow().stringValue());
		assertEquals(audience.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject3, AS.audience).orElseThrow().stringValue());
		assertEquals(audience.stringValue(), Models.getPropertyIRI(out.getModel(), subjectOfAsObject4, AS.audience).orElseThrow().stringValue());
	}
}
