package org.linkedopenactors.rdfpub.usecases.readobject;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

class TestContentEnricherReadObjectFromActor extends TestProcessorBase {

	@Produce(value = "direct:start2")
	protected ProducerTemplate templateSave;

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorGetActorsObjectRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentEnricherReadObjectFromActor.ID).to("mock:result");
					
					from("direct:start2").routeId("saveRoute").process("sharedDataBaseSaveActivity").to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutActor() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutCamelHttpUrlHeaderProperty() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getIn().setHeader(ActivityMessage.ACTOR_IRI, actorIri);
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'CamelHttpUrl'. headers: {actorIri="+actorIri.stringValue()+"}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testNotFound() throws Exception {
		createRepo(actorIri);

		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getIn().setHeader(ActivityMessage.ACTOR_IRI, actorIri);
		exchange.getIn().setHeader("CamelHttpUrl", iri("http://example.org/GibtEsNicht"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("'http://example.org/GibtEsNicht' not found (not found/404)", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSaveAndReadSaved() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.getIn().setHeader(ActivityMessage.ACTOR_IRI, actorIri);
		IRI createObjectIri = createObject(actorIri);
		exchange.getIn().setHeader("CamelHttpUrl", createObjectIri);
		Exchange responseExchange = template.send(exchange);
		
		ActivityMessage outMessage = new ActivityMessage(responseExchange.getMessage());
		assertEquals(actorIri, outMessage.getActorIri());
		assertEquals(1, outMessage.getModel().filter(createObjectIri, RDF.TYPE, AS.Update).size());
		assertEquals(1, outMessage.getModel().filter(createObjectIri, AS.object, null).size());
	}
	
	public IRI createObject(IRI actorIri) throws Exception {		
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		IRI subjectOfAsObject1 = iri("http://example.org/testObject1");		
		
		Model model = new ModelBuilder()
				.setNamespace("test", "http://example.org")
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, subjectOfAsObject1)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();

		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setActivity(subjectOfActivity);
		inMessage.setActorIri(actorIri);
		Exchange responseExchange = templateSave.send(exchange);
		
		assertNull(responseExchange.getException());
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertTrue(out.isSaved());
		return out.getActivity();
	}
	
}
