package org.linkedopenactors.rdfpub.usecases.readcollection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestRouteBase;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestRouteGetCollectionPublic extends TestRouteBase {	
	
	@Produce(value = "direct:readCollectionPublic")
	protected ProducerTemplate template;
	
	@DirtiesContext
	@Test
	public void testPublicCollection() throws Exception {
		IRI actorIri = getCurrentActorIri();
		Exchange exchange = new DefaultExchange(camelContext);
		IRI publicCollectionIri = getPublicCollection(actorIri);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.requestedUrl(publicCollectionIri)
				.collection(publicCollectionIri.getLocalName())
				.build());

		Exchange responseExchange = template.send(exchange);
		assertNull(responseExchange.getException());
				
		Model model = getMessage(responseExchange).getModel();
		ModelLogger.debug(log, model, "response: ");
		Model subjectModel = model.filter(null, RDF.TYPE, AS.OrderedCollectionPage);
		assertEquals(1, subjectModel.stream().count());
		IRI collectionSubject = (IRI)subjectModel.stream().findFirst().orElseThrow().getSubject();
		
		assertEquals("collection: asPublic; pageSize: 100; startIndex: 0", 
				Models.getPropertyLiteral(model, collectionSubject, AS.summary).orElseThrow().stringValue());

		assertEquals(publicCollectionIri, 
				Models.getPropertyIRI(model, collectionSubject, AS.partOf).orElseThrow());
	}
}
