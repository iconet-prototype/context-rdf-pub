package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

class TestMessageTranslatorRemoveOrphans extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorRemoveOrphansRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorRemoveOrphans.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActivityIri() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'activityIri'. headers: {}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		String subjectOfAsObject2 = "http://example.org/testObject2";
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.subject(subjectOfAsObject2)
					.add(RDF.TYPE, AS.Object)
					.add(AS.altitude, "something")
					.add(AS.name, "something")
					.add(AS.author, "something")
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertEquals(subjectOfActivity, out.getActivity().stringValue());
		assertTrue(Models.getProperty(out.getModel(), iri(subjectOfAsObject2), RDF.TYPE).isEmpty());
		assertEquals(model.size()-4, out.getModel().size());
	}
}
