package org.linkedopenactors.rdfpub.usecases.readcollection;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.StringReader;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

class TestContentEnricherReadCollection extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorGetCollectionRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentEnricherReadCollection.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutActorIri() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);

		String requestedUrl = actorIri.stringValue()+"/outbox";
		String collection = "outbox";
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.requestedUrl(iri(requestedUrl))
				.collection(collection)
				.build());

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {CamelHttpUrl="+requestedUrl+", collection="+collection+"}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutHttpUrl() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.build());

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		
		assertEquals("Message did not define header 'CamelHttpUrl'. headers: {actorIri="+actorIri.stringValue()+"}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutCollection() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.requestedUrl(iri("http://example.com/activity1/outbox"))
				.build());

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'collection'. headers: {actorIri="+actorIri.stringValue()+", CamelHttpUrl=http://example.com/activity1/outbox}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithWrongCollection() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.actorIri(actorIri)
				.requestedUrl(iri("http://example.com/activity1/outbox"))
				.collection("WrongValue")
				.build());

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("Header Property 'collection' is wrong. Possible values (inbox,outbox,asPublic) ->  (bad request/400)", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		createRepo(actorIri);		
		IRI requestedUrl = iri(actorIri.stringValue()+"/activity1/outbox");
		Exchange exchange = new DefaultExchange(camelContext);
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
			.actorIri(actorIri)
			.requestedUrl(requestedUrl)
			.collection(AvailableCollection.inbox.name())
			.build());
		
		Exchange responseExchange = template.send(exchange);
		
		assertEquals("{actorIri="+actorIri.stringValue()+", CamelHttpUrl="+requestedUrl+", collection=inbox}", responseExchange.getMessage().getHeaders().toString());
		
		String body = responseExchange.getMessage().getBody().toString();
		Model responseModel = Rio.parse(new StringReader(body), RDFFormat.TURTLE);
		IRI subject = iri(actorIri.stringValue()+"/inbox?pageSize=100&startIndex=0");
		assertEquals(AS.OrderedCollectionPage, Models.getPropertyIRI(responseModel, subject, RDF.TYPE).orElseThrow());
		assertEquals("collection: inbox; pageSize: 100; startIndex: 0", Models.getPropertyLiteral(responseModel, subject, AS.summary).orElseThrow().stringValue());
		assertEquals(actorIri.stringValue()+"/inbox", Models.getPropertyIRI(responseModel, subject, AS.partOf).orElseThrow().stringValue()); 
		assertEquals("0", Models.getPropertyLiteral(responseModel, subject, AS.totalItems).orElseThrow().stringValue());
	}
}
