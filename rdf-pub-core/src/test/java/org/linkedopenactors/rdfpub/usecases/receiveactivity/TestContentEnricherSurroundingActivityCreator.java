package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.StringWriter;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.camel.CamelExecutionException;
import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class TestContentEnricherSurroundingActivityCreator extends TestProcessorBase {

    @Configuration
    static class TestConfig {

    @Bean
    RoutesBuilder testProcessorSurroundingActivityCreatorRroute() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
            	from("direct:start").routeId("testRoute").process(ContentEnricherSurroundingActivityCreator.ID).to("mock:result");
            	}
            };
        }
    }

	
	@DirtiesContext
	@Test
	public void testNoRdfTurtle() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody("test input message");

		Exchange responseExchange = template.send(exchange);
		assertEquals(RDFParseException.class, responseExchange.getException().getCause().getClass());
		assertEquals("Expected ':', found ' ' [line 1]", responseExchange.getException().getCause().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testNoActor() throws Exception {
		Model model = new ModelBuilder()
				.setNamespace(new SimpleNamespace("test", "http://example.org/test"))
				.subject("test:sampleObject")
				.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.add(SCHEMA_ORG.name, literal("TestName"))
				.build();
		StringWriter modelTurtleWriter = new StringWriter();
		Rio.write(model, modelTurtleWriter, RDFFormat.TURTLE);
		CamelExecutionException thrown = Assertions.assertThrows(CamelExecutionException.class, () -> {
			template.sendBody(modelTurtleWriter.toString());
			});
			
		assertEquals(RuntimeException.class, thrown.getCause().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {}", thrown.getCause().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testNoAsObject() throws Exception {
		String subjectOfAsObject = "test:sampleObject";
		Model model = new ModelBuilder()
				.setNamespace(new SimpleNamespace("test", "http://example.org/test"))
				.subject(subjectOfAsObject)
				.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.add(SCHEMA_ORG.name, literal("TestName"))
				.build();
		
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
				
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("The objects types in the activity is 'https://schema.org/Organization' but it has to be a/derived from https://www.w3.org/ns/activitystreams#Object", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testSuccessPlainObject() throws Exception {
		String subjectOfAsObject = "http://example.org/test";
		Model model = new ModelBuilder()
				.subject(subjectOfAsObject)
				.add(RDF.TYPE, AS.Object)
				.add(SCHEMA_ORG.name, literal("TestName"))
				.build();
		
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
				
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		
		Model types = out.getModel().filter(null, RDF.TYPE, null);
		StringWriter sw = new StringWriter();
		Rio.write(types, sw, RDFFormat.TURTLE);
		
		Set<Statement> type = types.stream().filter(stmt->!stmt.getSubject().stringValue().equals(subjectOfAsObject)).collect(Collectors.toSet());
		assertEquals(1, type.size());
		assertEquals(type.stream().findFirst().get().getObject().stringValue(), AS.Create.stringValue());
	}

	@DirtiesContext
	@Test
	public void testTwoRootObjects() throws Exception {
		String subjectOfAsObject1 = "http://example.org/test1";
		String subjectOfAsObject2 = "http://example.org/test2";
		Model model = new ModelBuilder()
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, literal("TestName"))
				.subject(subjectOfAsObject2)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, literal("TestName2"))
				.build();
		
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
	
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("unable to determine exact one 'root' object, that can be added to the activity. Found: 2", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccessWithActivty() throws Exception {
		String subjectOfAsObject = "http://example.org/test";
		String subjectOfActivity = "http://example.org/testActivity";
		Model model = new ModelBuilder()
				.subject(subjectOfAsObject)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, literal("TestName"))
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, iri(subjectOfAsObject))					
				.build();
		
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
				
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		
		Model types = out.getModel().filter(null, RDF.TYPE, null);
		StringWriter sw = new StringWriter();
		Rio.write(types, sw, RDFFormat.TURTLE);
		
		Set<Statement> type = types.stream().filter(stmt->!stmt.getSubject().stringValue().equals(subjectOfAsObject)).collect(Collectors.toSet());
		assertEquals(1, type.size());
		assertEquals(type.stream().findFirst().get().getObject().stringValue(), AS.Create.stringValue());
	}

	@DirtiesContext
	@Test
	public void testWithTwoActivty() throws Exception {
		String subjectOfAsObject = "http://example.org/test";
		String subjectOfActivity = "http://example.org/testActivity";
		Model model = new ModelBuilder()
				.subject(subjectOfAsObject)
					.add(RDF.TYPE, AS.Object)
					.add(SCHEMA_ORG.name, literal("TestName"))
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, iri(subjectOfAsObject))					
					.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject))					
				.build();
		
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);

		inMessage.setActorIri(iri("http://example.org/testActor"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("More than one activity is not allowed in one request!", responseExchange.getException().getMessage());
	}
}
