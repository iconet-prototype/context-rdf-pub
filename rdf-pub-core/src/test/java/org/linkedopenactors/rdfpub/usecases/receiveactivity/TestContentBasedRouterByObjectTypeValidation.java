package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestContentBasedRouterByObjectTypeValidation extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorObjectTypeValidationRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentBasedRouterByObjectTypeValidation.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		Exchange responseExchange = template.send(exchange);
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithoutActivityIri() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'activityIri'. headers: {}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutAsObject() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri("http://example.org/testActivity"));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("No As:object in the activity", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithTwoAsObject() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";
		String subjectOfAsObject2 = "http://example.org/testObject2";
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
					.add(AS.object, iri(subjectOfAsObject2))
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("more than one As:object in the activity.", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testAsObjectWithoutType() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("The objects (http://example.org/testObject1) types in the activity is '' but it has to be a/derived from https://www.w3.org/ns/activitystreams#Object", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWrongObjectType() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("The objects (http://example.org/testObject1) types in the activity is 'https://schema.org/Organization' but it has to be a/derived from https://www.w3.org/ns/activitystreams#Object", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setBody(model);		
		
		template.send(exchange);
	}
}
