package org.linkedopenactors.rdfpub.usecases.queryactorsoutbox;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.usecases.APException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
	
import de.naturzukunft.rdf4j.vocabulary.AS;

class TestContentEnricherSparql extends TestProcessorBase {

	@Produce(value = "direct:start2")
	protected ProducerTemplate templateSave;

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorSparqlRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(ContentEnricherSparql.ID).to("mock:result");

					from("direct:start2").routeId("saveRoute").process("sharedDataBaseSaveActivity").to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutActor() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody("asd");
		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {}",
				responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutSparqlQuery() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(iri("http://example.com/actor"));
		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(APException.class, responseExchange.getException().getClass());
		assertEquals("sparqlQuery is mandatory. (bad request/400)", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testMalformedQueryException() throws Exception {
		createRepo(actorIri);
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(actorIri);
		inMessage.setHeader(ActivityMessageHeaders.COLLECTION, "outbox");
		inMessage.setBody("asd");

		Exchange responseExchange = template.send(exchange);

		assertNotNull(responseExchange.getException());
		assertEquals(MalformedQueryException.class, responseExchange.getException().getClass());
		assertTrue(responseExchange.getException().getMessage().startsWith("Encountered \" \"as\""));
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		createRepo(actorIri);
		IRI subjectOfActivity = iri(actorIri.getNamespace() + "/testActivity");
		IRI subjectOfAsObject1 = iri(actorIri.getNamespace() + "/testObject1");

		Model model = new ModelBuilder().setNamespace("test", "http://example.org").subject(subjectOfActivity)
				.add(RDF.TYPE, AS.Create).add(AS.object, subjectOfAsObject1).subject(subjectOfAsObject1)
				.add(RDF.TYPE, AS.Object).build();

		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setHeader(ActivityMessageHeaders.COLLECTION, "outbox");
		inMessage.setActivity(subjectOfActivity);
		inMessage.setActorIri(actorIri);
		Exchange responseExchange = templateSave.send(exchange);		
		String location = responseExchange.getMessage().getHeader(ActivityMessageHeaders.LOCATION).toString();
				
		exchange = new DefaultExchange(camelContext);
		inMessage = createInMessage(exchange);
		inMessage.setHeader(ActivityMessageHeaders.COLLECTION, "outbox");
		inMessage.setActorIri(actorIri);
		inMessage.setBody("SELECT DISTINCT * "
				+ "WHERE {?subject <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>/rdfs:subClassOf* <" + AS.Object
				+ ">  . }");

		responseExchange = template.send(exchange);

		assertTrue(responseExchange.getMessage().getBody().toString().contains(subjectOfAsObject1.stringValue()));
	}
}
