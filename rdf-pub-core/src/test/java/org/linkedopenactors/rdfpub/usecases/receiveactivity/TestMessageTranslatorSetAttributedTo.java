package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestMessageTranslatorSetAttributedTo extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorSetAttributedToRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorSetAttributedTo.ID).to("mock:result");
				}
			};
		}
	}

    @DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(iri("http://example.com/actor"));
		inMessage.setActivity(iri("http://example.com/activity"));

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActor() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActivityIri() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		inMessage.setActorIri(iri("http://example.com/actor"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'activityIri'. headers: {actorIri=http://example.com/actor}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		IRI subjectOfAsObject1 = iri("http://example.org/testObject1");		
		IRI subjectOfAsObject2 = iri("http://example.org/testObject2");
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, subjectOfAsObject1)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
					.add(AS.object, subjectOfAsObject2)
				.subject(subjectOfAsObject2)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.add(AS.name, "Max")
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		IRI actorIri = iri("http://example.com/actor");
		inMessage.setActorIri(actorIri);
		inMessage.setActivity(subjectOfActivity);
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());

		Optional<IRI> attributedTo = Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, AS.attributedTo);
		assertTrue(attributedTo.isPresent());
		assertEquals(attributedTo.get(), actorIri);

		attributedTo = Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, AS.attributedTo);
		assertTrue(attributedTo.isPresent());
		assertEquals(attributedTo.get(), actorIri);
	}
}
