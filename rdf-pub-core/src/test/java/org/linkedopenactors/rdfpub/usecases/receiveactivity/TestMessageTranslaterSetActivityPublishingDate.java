package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

public class TestMessageTranslaterSetActivityPublishingDate extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorSetPublishingDateRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslaterSetActivityPublishingDate.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(iri("http://example.com/actor"));
		inMessage.setActivity(iri("http://example.com/activity"));

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutzPublishingDated() throws Exception {
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))					
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setActivity(subjectOfActivity);
		
		Instant beforeSend = Instant.now();
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		Instant pubDate = Instant.parse(Models.getPropertyLiteral(out.getModel(), subjectOfActivity, AS.published).orElseThrow().stringValue());
		assertTrue(Instant.now().isAfter(pubDate));
		assertTrue(beforeSend.isBefore(pubDate));
	}

	@DirtiesContext
	@Test
	public void testWithWrongPublishingDated() throws Exception {
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))					
					.add(AS.published, literal(Instant.now().minusSeconds(60*10).toString()))
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setActivity(subjectOfActivity);
		
		Instant beforeSend = Instant.now();
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		Instant pubDate = Instant.parse(Models.getPropertyLiteral(out.getModel(), subjectOfActivity, AS.published).orElseThrow().stringValue());
		assertTrue(Instant.now().isAfter(pubDate));
		assertTrue(beforeSend.isBefore(pubDate));
	}

}