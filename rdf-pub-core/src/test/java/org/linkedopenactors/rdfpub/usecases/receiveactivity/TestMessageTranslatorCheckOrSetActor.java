package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.linkedopenactors.rdfpub.TestUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;

public class TestMessageTranslatorCheckOrSetActor extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorCheckOrSetActorRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorCheckOrSetActor.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActorIri(iri("http://example.com/actor"));
		inMessage.setActivity(iri("http://example.com/activity"));

		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActor() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'actorIri'. headers: {}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActivityIri() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		inMessage.setActorIri(iri("http://example.com/actor"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'activityIri'. headers: {actorIri=http://example.com/actor}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutActorInTheModel() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		IRI actorIri = iri("http://example.com/actor");
		inMessage.setActorIri(actorIri);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertEquals(subjectOfActivity, out.getActivity().stringValue());
		Optional<IRI> actorOptional = Models.getPropertyIRI(out.getModel(), iri(subjectOfActivity), AS.actor);
		assertTrue(actorOptional.isPresent());
		assertEquals(actorIri, actorOptional.get());
	}
	
	@DirtiesContext
	@Test
	public void testWithWrongActorInTheModel() throws Exception {
		IRI testActor1PreferedUserName = iri(actorIri.getNamespace() + users.get(0).getPreferredUserName());
		IRI testActor1UserId = iri(actorIri.getNamespace() + users.get(0).getUserId());
		IRI testActor2UserId = iri(actorIri.getNamespace() + users.get(1).getUserId());
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
					.add(AS.actor, testActor1PreferedUserName)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setActorIri(testActor2UserId);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("The actor in the activity ("+testActor1UserId.stringValue()+") does not match the actor of the outbox ("+testActor2UserId+")", responseExchange.getException().getMessage());
	}
	
	@DirtiesContext
	@Test
	public void testWithPreferredUsernameInTheModel() throws Exception {
		TestUser testActor = users.get(0);
		IRI testActorPreferedUserName = iri(actorIri.getNamespace() + testActor.getPreferredUserName());
		IRI testActorUserId = iri(actorIri.getNamespace() + testActor.getUserId());
		String subjectOfActivity = actorIri.getNamespace() + "testActivity";
		String subjectOfAsObject1 = actorIri.getNamespace() + "testObject1";	
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
					.add(AS.actor, testActorPreferedUserName)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));
		inMessage.setActorIri(testActorUserId);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
	}

	@DirtiesContext
	@Test
	public void testCorrectActivity() throws Exception {
		String subjectOfActivity = "http://example.org/testActivity";
		String subjectOfAsObject1 = "http://example.org/testObject1";
		IRI testActorUserId = iri(actorIri.getNamespace() + users.get(0).getUserId());
		
		Model model = new ModelBuilder()
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, iri(subjectOfAsObject1))
					.add(AS.actor, testActorUserId)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri(subjectOfActivity));		
		inMessage.setActorIri(testActorUserId);
		inMessage.setBody(model);		
		
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertNotNull(out.getActorIri());
		assertEquals(subjectOfActivity, out.getActivity().stringValue());		
		assertEquals(model.size(), out.getModel().size());
	}
}
