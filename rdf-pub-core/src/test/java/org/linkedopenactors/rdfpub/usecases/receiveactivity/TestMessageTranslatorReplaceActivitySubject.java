package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestMessageTranslatorReplaceActivitySubject extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorReplaceActivitySubjectRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorReplaceActivitySubject.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testWithoutActivityIri() throws Exception {
		Model model = new ModelBuilder().build();
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);		
		inMessage.setActorIri(iri("http://example.com/actor"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("Message did not define header 'activityIri'. headers: {actorIri=http://example.com/actor}", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testWithoutMessage() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setActivity(iri("http://example.com/activity"));
		inMessage.setActorIri(iri("http://example.com/actor"));
		Exchange responseExchange = template.send(exchange);
		
		assertNotNull(responseExchange.getException());
		assertEquals(RuntimeException.class, responseExchange.getException().getClass());
		assertEquals("message is NULL", responseExchange.getException().getMessage());
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		IRI actorIri = iri("http://example.org/testActor");
		IRI subjectOfActivity = iri("http://example.org/testActivity");
		IRI subjectOfAsObject1 = iri("http://example.org/testObject1");		
		IRI subjectOfAsObject2 = iri("http://example.org/testObject2");
		
		IRI anotherReference = iri("http://example.org/anotherReference");
		IRI reference = iri("http://example.org/reference");
		Model model = new ModelBuilder()
				.setNamespace("test", "http://example.org")
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, subjectOfAsObject1)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
					.add(AS.object, subjectOfAsObject2)
					.add(reference, subjectOfActivity)
				.subject(subjectOfAsObject2)
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.add(AS.name, "Max")
					.add(anotherReference, subjectOfActivity)
				.build();

		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setActivity(subjectOfActivity);
		inMessage.setActorIri(actorIri);
		
		Exchange responseExchange = template.send(exchange);
				
		assertNull(responseExchange.getException());
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		
		Model m = out.getModel().filter(null, RDF.TYPE, AS.Update);
		assertEquals(1, m.size());
		IRI newSubject = (IRI)m.iterator().next().getSubject();
		assertEquals(newSubject, Models.getPropertyIRI(out.getModel(), subjectOfAsObject1, reference).orElseThrow());
		assertEquals(newSubject, Models.getPropertyIRI(out.getModel(), subjectOfAsObject2, anotherReference).orElseThrow());
	}
}
