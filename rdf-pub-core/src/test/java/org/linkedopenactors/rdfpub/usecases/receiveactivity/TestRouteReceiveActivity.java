package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestRouteBase;
import org.linkedopenactors.rdfpub.camel.ApMessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;

public class TestRouteReceiveActivity extends TestRouteBase {	
	
	@Produce(value = "direct://receiveActivity")
	protected ProducerTemplate template;

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
		IRI actorIri = getRandomActorIri();
		IRI subjectOfActivity = iri(actorIri.getNamespace() + "/testActivity");
		IRI subjectOfAsObject1 = iri(actorIri.getNamespace() + "/testObject1");		
		
		Model model = new ModelBuilder()
				.setNamespace("test", "http://localhost:8080")
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, subjectOfAsObject1)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();

		Exchange exchange = new DefaultExchange(camelContext);
		
		exchange.setMessage(ApMessageBuilder.getInstance(exchange)
				.body(model)
				.activityIri(subjectOfActivity)
				.actorIri(actorIri)
				.requestedUrl(getOutbox(actorIri))
				.build()
				);
		
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertTrue(out.isSaved());
		
		Optional<ModelAndSubject> activityOptional = apRepositoryManager.getActorsRepository(actorIri).readObject(out.getActivity());
		assertTrue(activityOptional.isPresent());		
		Model reReadModel = activityOptional.get().getModel();
		assertEquals(AS.Create,  reReadModel.filter(out.getActivity(), RDF.TYPE, null).stream().findFirst().get().getObject());
		IRI object = iri(reReadModel.filter(out.getActivity(), AS.object, null).stream().findFirst().get().getObject().stringValue());

		Optional<ModelAndSubject> objectOptional = apRepositoryManager.getActorsRepository(actorIri).readObject(object);
		assertTrue(objectOptional.isPresent());		
		Model reReadObjectModel = objectOptional.get().getModel();
		assertEquals(AS.Object, reReadObjectModel.filter(object, RDF.TYPE, null).stream().findFirst().get().getObject());
	}
}
