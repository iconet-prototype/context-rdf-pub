package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.ActivityMessage;
import org.linkedopenactors.rdfpub.TestProcessorBase;
import org.linkedopenactors.rdfpub.config.ApConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import de.naturzukunft.rdf4j.vocabulary.AS;

@ActiveProfiles(ApConfig.PROFILE_UNIT_TEST)
class TestSharedDataBaseSaveActivity extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testProcessorSaveActivityRoute() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(SharedDataBaseSaveActivity.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testSuccess() throws Exception {
				
		createRepo(actorIri);
		IRI subjectOfActivity = iri(actorIri.stringValue() + "/testActivity");
		IRI subjectOfAsObject1 = iri(actorIri.stringValue() + "/testObject1");		
		
		Model model = new ModelBuilder()
				.setNamespace("test", "http://example.org")
				.subject(subjectOfActivity)
					.add(RDF.TYPE, AS.Update)
					.add(AS.object, subjectOfAsObject1)
				.subject(subjectOfAsObject1)
					.add(RDF.TYPE, AS.Object)
				.build();

		Exchange exchange = new DefaultExchange(camelContext);
		ActivityMessage inMessage = createInMessage(exchange);
		inMessage.setBody(model);
		inMessage.setActivity(subjectOfActivity);
		inMessage.setActorIri(actorIri);
		Exchange responseExchange = template.send(exchange);
		
		assertNull(responseExchange.getException());
		
		ActivityMessage out = new ActivityMessage(responseExchange.getMessage());
		assertNotNull(out.getActivity());
		assertTrue(out.isSaved());
	}
}
