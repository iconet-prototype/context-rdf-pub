package org.linkedopenactors.rdfpub;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.List;
import java.util.UUID;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.DefaultMessage;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.rdfpub.config.ApConfig;
import org.linkedopenactors.rdfpub.repository.ApRepository;
import org.linkedopenactors.rdfpub.repository.ApRepositoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@CamelSpringBootTest
@EnableAutoConfiguration
@SpringBootTest(
    properties = { "camel.springboot.name=customName" })
@ComponentScan(basePackages = {"org.linkedopenactors.rdfpub.repository", 
		"org.linkedopenactors.rdfpub"})
@ActiveProfiles(ApConfig.PROFILE_UNIT_TEST)
public abstract class TestProcessorBase {

	protected static IRI actorIri = getRandomActorIri();
	
	@Autowired
	protected List<TestUser> users;
	
	@Autowired
	private ApRepositoryManager apRepositoryManager;

	@Autowired
	protected CamelContext camelContext;

	@Produce(value = "direct:start")
	protected ProducerTemplate template;

	protected ActivityMessage createInMessage(Exchange exchange) {		
		Message message = new DefaultMessage(exchange);
		ActivityMessage am = new ActivityMessage(message);
		exchange.setIn(am);
		return am;
	}
	
	protected static IRI getRandomActorIri() {
		return iri("http://example.org/"+ UUID.randomUUID());
	}
	
	protected ApRepository createRepo(IRI actorIri) {
		return apRepositoryManager.createActorsRepository4Person(actorIri);	
	}	
}
