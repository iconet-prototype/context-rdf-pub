package org.linkedopenactors.rdfpub;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.Exchange;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.camel.ActivityMessageHeaders;
import org.linkedopenactors.rdfpub.usecases.overlapping.MessageTranslatorRequestedUrlNormalizerProtocol;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest(properties = { "app.https=true" })
public class TestMessageTranslatorRequestedUrl_https extends TestProcessorBase {

	@Configuration
	static class TestConfig {

		@Bean
		RoutesBuilder testRequestedUrlNormalizerProtocolRoute_https() {
			return new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:start").routeId("testRoute").process(MessageTranslatorRequestedUrlNormalizerProtocol.ID).to("mock:result");
				}
			};
		}
	}

	@DirtiesContext
	@Test
	public void testSuccessWithUserId() throws Exception {
		Exchange exchange = new DefaultExchange(camelContext);
		TestUser user = this.users.get(0);
		String reqUrl = "http://localhost:8080/camel/"+user.getUserId()+"/018277";
		exchange.getIn().setHeader(ActivityMessageHeaders.REQUESTED_URL, iri(reqUrl));
		
		Exchange responseExchange = template.send(exchange);
	
		assertNull(responseExchange.getException());
		String requestedUrl = responseExchange.getMessage().getHeader(ActivityMessageHeaders.REQUESTED_URL).toString();
		assertNotNull(requestedUrl);
		assertEquals(requestedUrl, reqUrl.replace("http", "https"));		
	}
}
