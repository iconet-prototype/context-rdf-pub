# Query a object with it's attributes, labels and comments

If you have created a activity like the following:
```
<http://localhost:8080/camel/tester/outbox/activity_c4501677-6f49-46d7-9baa-0e234d56d79d>
  a <https://www.w3.org/ns/activitystreams#Create>;
  <https://www.w3.org/ns/activitystreams#published> "2021-12-06T14:58:14.922543Z";
  <https://www.w3.org/ns/activitystreams#name> "Object(s) 'Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering' created.";
  <https://www.w3.org/ns/activitystreams#actor> <http://localhost:8080/camel/tester>;
  <https://www.w3.org/ns/activitystreams#object> <http://localhost:8080/camel/tester/objects/object_daa5f684-03cd-4eab-96e4-61ca99345a4e>;
  <https://www.w3.org/ns/activitystreams#summary> "This activity wraps an object that was created without a surrounding activity." .
```

You can query it like that:

```
curl --location --request POST 'localhost:8080/camel/tester/sparql' \
--header 'Accept: application/sparql-results+json' \
--header 'Content-Type: text/plain' \
--data-raw 'SELECT * WHERE {
    <http://localhost:8080/camel/tester/outbox/activity_c4501677-6f49-46d7-9baa-0e234d56d79d> ?property ?value .
    ?property rdfs:label ?label .
    ?property rdfs:comment ?comment .
} LIMIT 100'
```

Then you get the labels and comments of the RDF type. That is possible, because the Repository that is used from the SPARQL endpoint is a federated repository of the actors repository containing activities and objects and a rdftype repository containing RDF Schema information. 

TODO Link to the description of the RDF Type Repository

```
{
  "head" : {
    "vars" : [
      "property",
      "value",
      "label",
      "comment"
    ]
  },
  "results" : {
    "bindings" : [
      {
        "property" : {
          "type" : "uri",
          "value" : "https://www.w3.org/ns/activitystreams#actor"
        },
        "comment" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "Subproperty of as:attributedTo that identifies the primary actor"
        },
        "label" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "actor"
        },
        "value" : {
          "type" : "uri",
          "value" : "http://localhost:8080/camel/tester"
        }
      },
      {
        "property" : {
          "type" : "uri",
          "value" : "https://www.w3.org/ns/activitystreams#published"
        },
        "comment" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "Specifies the date and time the object was published"
        },
        "label" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "published"
        },
        "value" : {
          "type" : "literal",
          "value" : "2021-12-06T14:58:14.922543Z"
        }
      },
      {
        "property" : {
          "type" : "uri",
          "value" : "https://www.w3.org/ns/activitystreams#summary"
        },
        "comment" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "A short summary of the object"
        },
        "label" : {
          "xml:lang" : "en",
          "type" : "literal",
          "value" : "summary"
        },
        "value" : {
          "type" : "literal",
          "value" : "This activity wraps an object that was created without a surrounding activity."
        }
      }
    ]
  }
}
```

