# Start the docker image locally

1. [download docker-compose.yml](https://gitlab.com/linkedopenactors/rdf-pub/-/raw/develop/add_sparql/devEnv/guest/docker-compose.yml?inline=false)
2. goto the directory where the downloaded docker-compose.yml is located.
3. call `docker-compose up`
4. If the volume does not exist yet, you should get a message like:  
ERROR: Volume rdf4jRepos declared as external, but could not be found. Please create the volume manually using `docker volume create --name=rdf4jRepos` and try again.  
After executing `docker volume create --name=rdf4jRepos` you can go on.
5. After docker-compose is started, you can verify the running server with:  
http://localhost:8080/actuator/health  
http://localhost:8080/actuator/info
6. Next step is to create a ActivityPub Object. To do so use the following curl command:
```
curl --location --request POST 'localhost:8080/camel/tester' \
--header 'Content-Type: text/turtle' \
--data-raw '@prefix schema: <https://schema.org/> .
@prefix as: <https://www.w3.org/ns/activitystreams#> .
@prefix kvm_loa: <http://localhost:8090/kvm_loa/> .

kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a a schema:CreativeWork, as:Object;
  schema:dateCreated "1970-01-19T23:52:48.511";
  schema:about kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_organisation;
  schema:license "CC0-1.0";
  schema:dateModified "2021-12-04T18:19:19.951277";
  schema:description "Verbrauchsgemeinschaft für solidarischen und gesegelten Kaffee. Hier kannst du ökologisch und sozial anspruchsvollen Kaffee bestellen und dich mit anderen Enthusiast*innen von gutem und nachhaltigen Kaffee austauschen. Bestelle schnell und einfach mit Klick auf unsere Homepage.";
  schema:version "0";
  schema:name "Teikei Gemeinschaft München Trudering";
  schema:identifier "9d317daca74246d4be41b1a37e30ee2a";
  as:name "Teikei Gemeinschaft München Trudering";
  schema:keywords "teikei,teikei-gemeinschaft" .


kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_organisation a schema:Organization, as:Object;
  schema:contactPoint kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_contactPoint;
  schema:name "Teikei Gemeinschaft München Trudering";
  schema:location kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_place .
  
kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_contactPoint a schema:ContactPoint, as:Object;
  schema:email "muenchen-trudering@teikei.community";
  schema:telephone "" .

kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_place a as:Object, schema:Place;
  schema:longitude 1.1676090784220067E1;
  schema:address kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_postalAddress;
  schema:latitude 4.813284429168927E1 .

kvm_loa:V0_9d317daca74246d4be41b1a37e30ee2a_postalAddress a schema:PostalAddress,
    as:Object;
  schema:streetAddress """Linnenbrüggerstrasse  13
""";
  schema:postalCode "81829";
  schema:addressLocality "München" .
'
```
7. you get a response like: 
```
<http://localhost:8080/camel/tester/outbox/activity_9f7c2f57-740f-4335-9e02-dc3eb8b51104>
  a <https://www.w3.org/ns/activitystreams#Create>;
  <https://www.w3.org/ns/activitystreams#actor> <http://localhost:8080/camel/tester>;
  <https://www.w3.org/ns/activitystreams#summary> "This activity wraps an object that was created without a surrounding activity.";
  <https://www.w3.org/ns/activitystreams#object> <http://localhost:8080/camel/tester/objects/object_203772d5-39f3-4154-96db-8d4aed8c7901>;
  <https://www.w3.org/ns/activitystreams#name> "Object(s) 'Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering, Teikei Gemeinschaft München Trudering' created.";
  <https://www.w3.org/ns/activitystreams#published> "2021-12-04T18:41:18.055561Z" .
```
8. take the subject of the created activity in this case `http://localhost:8080/camel/tester/outbox/activity_9f7c2f57-740f-4335-9e02-dc3eb8b51104` and request it with the following curl command:
```
curl --location --request GET 'http://localhost:8080/camel/tester/outbox/activity_9f7c2f57-740f-4335-9e02-dc3eb8b51104' \
--header 'Accept: text/turtle'
```
9. To get the data of the created object, use the subject `http://localhost:8080/camel/tester/objects/object_203772d5-39f3-4154-96db-8d4aed8c7901` in the curl command. it is the literal value of the statement  

`<http://localhost:8080/camel/tester/outbox/activity_9f7c2f57-740f-4335-9e02-dc3eb8b51104> <https://www.w3.org/ns/activitystreams#object> <http://localhost:8080/camel/tester/objects/object_203772d5-39f3-4154-96db-8d4aed8c7901>;`  
  
More readable  
```
@prefix outbox: <http://localhost:8080/camel/tester/outbox/> .
@prefix objects: <http://localhost:8080/camel/tester/objects/> .
@prefix as: <https://www.w3.org/ns/activitystreams#> .

outbox:activity_9f7c2f57-740f-4335-9e02-dc3eb8b51104 as:object objects:object_203772d5-39f3-4154-96db-8d4aed8c7901;
```
10. Follow the other references (which can be read out in response):  
about: http://localhost:8080/camel/tester/objects/object_f3e1352d-0154-48f5-b6a2-7c1dc8b24b19  
location http://localhost:8080/camel/tester/objects/object_af29a131-321b-48d8-93ca-f07f359a4979  
contactPoint http://localhost:8080/camel/tester/objects/object_092012ce-a0e9-4bb5-b52c-146850175514  
address http://localhost:8080/camel/tester/objects/object_538a4a87-184e-4f4f-8af4-68314ffb43ce  
11. If you change the Accept Header to `--header 'Accept: application/ld+json'` you get json-ld as response.

## NOTE
The download lonk above will be a link for the future. fr now use 
https://gitlab.com/linkedopenactors/rdf-pub/-/raw/feature/add_sparql/devEnv/guest/docker-compose.yml?inline=false
