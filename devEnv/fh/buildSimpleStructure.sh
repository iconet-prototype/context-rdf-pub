#!/bin/sh
set echo on
cd $CURRENT_APP_HOME
echo build $CURRENT_APP_NAME	
mvn -q clean install -DskipTests
mkdir -p ./target/dependency 
cd ./target/dependency
jar -xf $CURRENT_APP_HOME/./target/app.jar
cd ../..
#ls -l $CURRENT_APP_HOME/./target/dependency
docker build -t $CURRENT_APP_NAME:latest .
cd $SLR_BUILD_HOME



