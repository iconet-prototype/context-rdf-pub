package org.linkedopenactors.rdfpub.repository;

import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.slf4j.Logger;
import org.slf4j.event.Level;

public interface IdentityProvider {
	
	/**
	 * Searches for a user whose userId or preferred username matches the passed one. 
	 * @param actorName the userId or preferred username that have to match. 
	 * @return List of matching users.
	 */
	List<IdentityProviderUser> search(String actorName);
	
	/**
	 * Same as {@link #search(String)}, the actorIds localname is used as userName.
	 * @param actorId iri with userId or preferred username that have to match.
	 * @return List of matching users.
	 */
	List<IdentityProviderUser> search(IRI actorId);
	
	/**
	 * Searches ({@link #search(IRI)}) for a user with the passed actorId.
	 * @param actorId The AP actorId
	 * @return The found user, if exists
	 * @throws RuntimeException if there is mor than user for the passe actorId.
	 */
	Optional<IdentityProviderUser> getUser(IRI actorId);
	
	/**
	 * Determinates the actorIRI in it's original form with it's userId. E.g. http://localhost:8080/camel/0815
	 * We allow actor iri's with preferredUserNames, but the system (other camel components) expect the 'real' actor with it's userId.
	 * So we replace the preferredUserName by it's userId.
	 * @param actorId With preferredUserName or UserId. E.g. http://localhost:8080/camel/max or http://localhost:8080/camel/0815 
	 * @return The actor in it's original form with it's userId. E.g. http://localhost:8080/camel/0815
	 */
	IRI normalize(IRI actorId);
	void logIdentityProviderProperties(Logger logger, Level level);
	void logIdentityProviderProperties(Logger logger, Level level, String msg);
}
