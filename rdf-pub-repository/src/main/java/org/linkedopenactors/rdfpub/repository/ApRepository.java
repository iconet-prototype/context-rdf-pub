package org.linkedopenactors.rdfpub.repository;

import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

public interface ApRepository {

	public enum AvailableCollection {
		inbox, outbox, asPublic;
	}
	
	IRI save(ModelAndSubject activity, Set<IRI> contexts);
	Optional<ModelAndSubject> readObject(IRI activitySubject);
	SparqlResult executeSparql(AvailableCollection[] availableCollections, String query, String acceptHeader/*, String defaultGraphUri, String namedGraphUri*/);
	Model getProfile();
	Model getCollection(AvailableCollection availableCollection, int pageSize, int startIndex);
	void processUpdateOutboxActivity(IRI activityIri, IRI objectIri, Model model);
}
