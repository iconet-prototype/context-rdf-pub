package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.HashSet;
import java.util.List;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.federated.FedXFactory;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryImplConfig;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.sail.nativerdf.config.NativeStoreConfig;
import org.linkedopenactors.ns.rdfpub.RDFPUB;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluator;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OneForAllApRepositoryManager implements ApRepositoryManager {
	private SparqlQueryEvaluator sparqlQueryEvaluator;
	private static final String TYPE_REPOSITORY = "typeRepository";
	private static final String RDFPUB_REPOSITORY = "rdfPubRepository";
	private RepositoryManager repositoryManager;
	private IdentifyRecipients identifyRecipients;
	private IdentityProvider identityProvider;
	private Repository federatedRepository;

	public OneForAllApRepositoryManager(RepositoryManager repositoryManager, SparqlQueryEvaluator sparqlQueryEvaluator, IdentifyRecipients identifyRecipients, IdentityProvider identityProvider) {
		this.repositoryManager = repositoryManager;
		this.sparqlQueryEvaluator = sparqlQueryEvaluator;
		this.identifyRecipients = identifyRecipients;
		this.identityProvider = identityProvider;
	}
	
	@Override
	public ApRepository getActorsRepository(IRI actorIri) {
		IRI inboxCollectionIri;
		IRI outboxCollectionIri;  
		Repository actorsRepository = getRdfPubRepository();
		try (RepositoryConnection con = actorsRepository.getConnection()) {
			inboxCollectionIri = getCollectionIri(actorIri, AS.inbox, con);
			outboxCollectionIri = getCollectionIri(actorIri, AS.outbox, con);
		}
		ApRepositoryData apRepositoryData = new ApRepositoryData(actorIri, actorsRepository, getFederatedRepository(),
				inboxCollectionIri, outboxCollectionIri);
		return new SimpleApRepository(apRepositoryData, sparqlQueryEvaluator, identifyRecipients);
	}

	private Repository getFederatedRepository() {
		if(federatedRepository==null) {
			federatedRepository = FedXFactory.newFederation()
				.withRepositoryResolver(repositoryManager)
				.withResolvableEndpoint(RDFPUB_REPOSITORY)
				.withResolvableEndpoint(TYPE_REPOSITORY)
				.create();
		}
		return federatedRepository;
	}

	private Repository getRdfPubRepository()  {
		Repository repository = repositoryManager.getRepository(RDFPUB_REPOSITORY);
		if(repository==null) {			
			repository = createRepo(RDFPUB_REPOSITORY);
		}
		return repository;
	}
	
	private IRI getCollectionIri(IRI actorIri, IRI collectionIri, RepositoryConnection con) {
		List<Statement> collectionStatements = Iterations.asList(con.getStatements(actorIri, collectionIri, null));
		if(collectionStatements.size()!=1) {			
			Model model = new ModelBuilder().build();
			model.addAll(new HashSet<>(collectionStatements));
			ModelLogger.error(log, model, "profile of actor: ");
			throw new RuntimeException(collectionIri + " of " + actorIri + " has not exact one value but " + collectionStatements.size());
		}
		return iri(collectionStatements.get(0).getObject().stringValue());
	}

	@Override
	public Repository getRdfTypeRepository() {
		Repository typeRepository = repositoryManager.getRepository(TYPE_REPOSITORY);
		
		if(typeRepository == null) {
			typeRepository = createRepo(TYPE_REPOSITORY);
		}
		return typeRepository;
	}

	public ApRepository createActorsRepository4Person(IRI actorIri) {
		Repository repository = getRdfPubRepository();
		IRI inboxCollection =  iri(actorIri.stringValue(), "/"+AvailableCollection.inbox.name());
		IRI outboxCollection=  iri(actorIri.stringValue(), "/"+AvailableCollection.outbox.name());		
		IRI publicCollection=  iri(actorIri.stringValue(), "/"+AvailableCollection.asPublic.name());
		IRI sparqlOutbox =  iri(outboxCollection.stringValue() + "/sparql");
		Model model = new ModelBuilder()
				.subject(actorIri)
					.add(RDF.TYPE , AS.Person)
					.add(AS.inbox, inboxCollection)
					.add(AS.outbox, outboxCollection)
					.add(AS.Public, publicCollection)
					.add(RDFPUB.OUTBOX_SPARQL, sparqlOutbox)
					.add(AS.preferredUsername,
						identityProvider.getUser(actorIri)
								.orElseThrow(() -> new RuntimeException("no user found for " + actorIri))
								.getPreferredUserName())
					.build();
		
		try (RepositoryConnection con = repository.getConnection()) {
			con.add(model, actorIri);
		}
		
		ApRepository actorsRepository = getActorsRepository(actorIri);
		ModelLogger.trace(log, actorsRepository.getProfile(), "created profile: ");
		return actorsRepository;
	}
	
	private Repository createRepo(String repositoryId) {
		Repository actorsRepository;
		RepositoryImplConfig repositoryTypeSpec = new SailRepositoryConfig(new NativeStoreConfig());
		RepositoryConfig repConfig = new RepositoryConfig(repositoryId, repositoryTypeSpec);
		 
		repositoryManager.addRepositoryConfig(repConfig);
		actorsRepository = repositoryManager.getRepository(repositoryId);
		return actorsRepository;
	}


	@Override
	public boolean hasRepository(IRI actorIri) {
		ApRepository actorsRepository;
		try {
			actorsRepository = getActorsRepository(actorIri);
			return !actorsRepository.getProfile().isEmpty();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public ApRepository getPublicRepository(IRI baseUrlWithContext) {
		return new PublicRepository(baseUrlWithContext, getRdfPubRepository(), getFederatedRepository(), sparqlQueryEvaluator);
	}
}
