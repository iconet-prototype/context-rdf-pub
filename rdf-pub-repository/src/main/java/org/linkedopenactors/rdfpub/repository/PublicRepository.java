package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.Repository;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluator;
import de.naturzukunft.rdf4j.vocabulary.AS;


public class PublicRepository extends AbstractApRepository {

	private Repository repository;
	private Repository sparqlReadRepository;
	private IRI baseUrlWithContext;

	public PublicRepository(IRI baseUrlWithContext, Repository repository, Repository sparqlReadRepository, SparqlQueryEvaluator sparqlQueryEvaluator) {
		super(sparqlQueryEvaluator);
		this.baseUrlWithContext = baseUrlWithContext;
		this.repository = repository;
		this.sparqlReadRepository = sparqlReadRepository;
	}
	
	@Override
	public IRI save(ModelAndSubject activity, Set<IRI> contexts) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Model getProfile() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void processUpdateOutboxActivity(IRI activityIri, IRI objectIri, Model model) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected IRI getNamedGraph() {
		return AS.Public;
	}

	@Override
	protected Repository getRepository() {
		return repository;
	}

	@Override
	protected Repository getSparqlReadRepository() {
		return sparqlReadRepository;
	}

	@Override
	protected IRI determineNamedGraphToUse(AvailableCollection availableCollection) {
		return iri(baseUrlWithContext.stringValue() + AvailableCollection.asPublic.name());
	}
}
