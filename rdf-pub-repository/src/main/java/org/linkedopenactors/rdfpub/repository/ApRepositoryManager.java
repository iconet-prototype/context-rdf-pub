package org.linkedopenactors.rdfpub.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.Repository;

public interface ApRepositoryManager {

	boolean hasRepository(IRI actorIri);
	ApRepository getActorsRepository(IRI actorIri);
	ApRepository getPublicRepository(IRI baseUrlWithContext);
	ApRepository createActorsRepository4Person(IRI actorIri);
	
//	Repository getActorsRepository(IRI actorIri);

	Repository getRdfTypeRepository();

}
