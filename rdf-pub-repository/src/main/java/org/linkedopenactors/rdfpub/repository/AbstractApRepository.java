package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.sparql.EvaluateResult;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluator;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractApRepository implements ApRepository {

	private SparqlQueryEvaluator sparqlQueryEvaluator;
	
	public AbstractApRepository(SparqlQueryEvaluator sparqlQueryEvaluator) {
		this.sparqlQueryEvaluator = sparqlQueryEvaluator;
	}
	
	@Override
	public Optional<ModelAndSubject> readObject(IRI activitySubject) {
		Optional<Model> activityModelOptional = Optional.empty();		
		try(RepositoryConnection con = getRepository().getConnection()) {
			Model model = null;
			Set<Statement> allStatementsForActivityIri = Iterations.asSet(con.getStatements(activitySubject, null, null, getNamedGraph() ));
			if(!allStatementsForActivityIri.isEmpty()) {
				model = new ModelBuilder().build();			
				model.addAll(allStatementsForActivityIri);
			} else {
				log.trace("no statements for '"+activitySubject+"' in named graph '"+getNamedGraph()+"'");
			}
			activityModelOptional = Optional.ofNullable(model);
		}
		return activityModelOptional.map(m->new ModelAndSubject(activitySubject, m));
	}

	@Override
	public SparqlResult executeSparql(AvailableCollection[] availableCollections, String query, String acceptHeader) {
		
		String[] availableCollectionStringArray = Arrays.stream(availableCollections).map(availableCollection->{
			return determineNamedGraphToUse(availableCollection);	
		}).map(IRI::stringValue).collect(Collectors.toList()).toArray(new String[]{});
		
		String[] defaultGraphUris = new String[] { getNamedGraph().stringValue() };
		EvaluateResult evaluateResult = sparqlQueryEvaluator.evaluate(getSparqlReadRepository(), query, acceptHeader,
				defaultGraphUris, availableCollectionStringArray);
		return new SparqlResult(evaluateResult.getResult(), evaluateResult.getMimeType());
	}

	@Override
	public Model getCollection(AvailableCollection availableCollection, int pageSize, int startIndex) {
		
		IRI namedGraphOfCollectionToUse = determineNamedGraphToUse(availableCollection);

		Model constructedActivities = getActivities(namedGraphOfCollectionToUse, pageSize, startIndex);
		Model subjectStatementsOfconstructedActivities = constructedActivities.filter(null, RDF.TYPE, null);

		String subjectOfOrderedCollection = String.format("%s?pageSize=%s&startIndex=%s", namedGraphOfCollectionToUse.stringValue(), pageSize, startIndex);
		ModelBuilder orderedCollectionBuilder = new ModelBuilder()
			.subject(iri(subjectOfOrderedCollection))
				.add(RDF.TYPE, AS.OrderedCollectionPage)
				.add(AS.summary, literal("collection: " +availableCollection.name() + "; pageSize: " + pageSize + "; startIndex: " + startIndex))
				.add(AS.partOf, namedGraphOfCollectionToUse)
				.add(AS.totalItems, literal(subjectStatementsOfconstructedActivities.size()+""));
		
		subjectStatementsOfconstructedActivities
				.forEach(subjectStatement -> orderedCollectionBuilder.add(AS.items, subjectStatement.getSubject()));
		orderedCollectionBuilder.namedGraph(namedGraphOfCollectionToUse);
		Model model = orderedCollectionBuilder.build();
		model.addAll(constructedActivities);
		
		return model;
	}
	
	private Model getActivities(IRI namedGraphToUse, int pageSize, int startIndex) {
		String select = ("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "SELECT DISTINCT ?entity ?published ?type ?actor ?name ?summary ?object\n");
				
		if(namedGraphToUse!=null) {
			select = select + String.format("FROM <%s> \n", namedGraphToUse.stringValue());  
		}
				
		String queryString = String.format(select 
				+ "FROM <https://schema.org/schemaorg-all-https.ttl>\n"
				+ "FROM <https://www.w3.org/ns/activitystreams#activitystreams-definitions.ttl>\n"
				+ "WHERE\n"
				+ "{\n"
				+ "  ?entity rdf:type ?type.\n"
				+ "  OPTIONAL {?entity <https://www.w3.org/ns/activitystreams#published> ?published .}\n"
				+ "  OPTIONAL {?entity <https://www.w3.org/ns/activitystreams#actor> ?actor .}\n"
				+ "  OPTIONAL {?entity <https://www.w3.org/ns/activitystreams#name> ?name .}\n"
				+ "  OPTIONAL {?entity <https://www.w3.org/ns/activitystreams#summary> ?summary .}\n"
				+ "  OPTIONAL {?entity <https://www.w3.org/ns/activitystreams#object> ?object .}\n"
				+ "  ?type rdfs:subClassOf* <https://www.w3.org/ns/activitystreams#Activity> .\n"
				+ "}\n"
				+ "ORDER BY desc(?published)\n"
				+ "LIMIT %s\n"
				+ "OFFSET %s", pageSize, startIndex)
				;
		
		log.trace("queryString: " + queryString);
		
		ModelBuilder collectionItemsBuilder = new ModelBuilder();
		try (RepositoryConnection conn = getSparqlReadRepository().getConnection()) {
			TupleQuery tupleQuery = conn.prepareTupleQuery(queryString);
			TupleQueryResult result = tupleQuery.evaluate();
			log.trace("result.hasNext(): " + result.hasNext());
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				log.trace("bindingSet: " + bindingSet);
				collectionItemsBuilder.subject(iri(bindingSet.getBinding("entity").getValue().stringValue()));
				
				toIRI(bindingSet, "type").ifPresent(iri->collectionItemsBuilder.add( RDF.TYPE, iri));
				toLiteral(bindingSet, "published").ifPresent(literal->collectionItemsBuilder.add( AS.published, literal));
				toIRI(bindingSet, "actor").ifPresent(iri->collectionItemsBuilder.add( AS.actor, iri));
				toLiteral(bindingSet, "name").ifPresent(literal->collectionItemsBuilder.add( AS.name, literal));
				toLiteral(bindingSet, "summary").ifPresent(literal->collectionItemsBuilder.add( AS.summary, literal));
				toIRI(bindingSet, "object").ifPresent(iri->collectionItemsBuilder.add( AS.object, iri));
			}
		}
		Model buildItems = collectionItemsBuilder.build();
		return buildItems;
	}

	private Optional<Literal> toLiteral(BindingSet bindingSet, String bindingName) {		
		return Optional.ofNullable(
				bindingSet.getBinding(bindingName))
				.map(Binding::getValue)
				.map(Value::stringValue)
				.map(Values::literal);
	}

	private Optional<IRI> toIRI(BindingSet bindingSet, String bindingName) {		
		return Optional.ofNullable(
				bindingSet.getBinding(bindingName))
				.map(Binding::getValue)
				.map(Value::stringValue)
				.map(Values::iri);
	}

	protected abstract Repository getSparqlReadRepository();

	protected abstract IRI determineNamedGraphToUse(AvailableCollection availableCollection);

	protected abstract IRI getNamedGraph();

	protected abstract Repository getRepository();

}
