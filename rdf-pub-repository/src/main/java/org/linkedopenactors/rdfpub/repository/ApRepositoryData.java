package org.linkedopenactors.rdfpub.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.Repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApRepositoryData {
	private IRI actorIri;
	private Repository repository;
	private Repository sparqlReadRepository;
	private IRI inboxCollectionNamedGraph;
	private IRI outboxCollectionNamedGraph;  
}
