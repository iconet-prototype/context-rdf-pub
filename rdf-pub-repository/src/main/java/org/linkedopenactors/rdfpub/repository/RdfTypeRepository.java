package org.linkedopenactors.rdfpub.repository;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Statements;

import de.naturzukunft.rdf4j.vocabulary.AS;

public interface RdfTypeRepository {
	/**
	 * Adds the passed type definitions (RDF schema) to the passed connection unsing
	 * the passe <a href=
	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
	 * 
	 * @param definitionAsTurtle a RDF string that contains type definitions.
	 * @param context See https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext
	 * @return the IRI of the context
	 */
	IRI addTurtleTypeDefinition(String definitionAsTurtle, IRI context);

	/**
	 * @return all <a href=
	 *         "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">contexts</a>
	 *         that are part of that repository.
	 */
	Set<IRI> getContexts();

	/**
	 * @param context The IRI of the context.
	 * @return true, if the passed <a href=
	 *         "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>
	 *         is part of that repository, otherwise false.
	 */
	boolean containsContext(IRI context);

	/**
	 * Remove all {@link Statements} in the passed <a href=
	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
	 * @param context The IRI of the context.
	 * @return false if the passed context is not available.
	 */
	boolean removeAll(IRI context);
	
	/**
	 * Reads a definition.
	 * @param context the subject/context of the definition.
	 * @return The definition as Model (set of statements)
	 */
	Model read(IRI context);
	
	/**
	 * @param model the model to analyse
	 * @return All subjects from the passed model, that are derived (rdfs:subClassOf) from {@link AS#Object}.
	 */
	public Set<IRI> getAllSubjectsThatAreASubclassOfAsObject(Model model);
	
	/**
	 * @param types the types to validate.
	 * @param expected the type that is expected in the passed types.
	 * @return True, if the passed types contain a {@link IRI} that is a subClass of the passed expected type, otherwise false.   
	 */
	public boolean isSubclassOf(Set<IRI> types, IRI expected);
	
	/**
	 * 
	 * @param type the type to validate. 
	 * @param expected the type that is expected in the passed types.
	 * @return True, if the passed type is a subClass of the passed expected type, otherwise false.
	 */
	public boolean isSubclassOf(IRI type, IRI expected);
}
