package org.linkedopenactors.rdfpub.repository;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

public interface IdentifyRecipients {
	Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject);
}
