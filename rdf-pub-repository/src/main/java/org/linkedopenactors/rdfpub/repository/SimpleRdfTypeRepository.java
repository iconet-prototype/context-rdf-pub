package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Statements;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
class SimpleRdfTypeRepository implements RdfTypeRepository {
	
	private static final IRI TEMP_MODEL = iri("http://example.com/tempModel");
	
	private static final String ACTIVITYSTREAMS_DEFINITIONS = "activitystreams-definitions.ttl";
	private static final String SCHEMA_ORG_DEFINITIONS = "schemaorg-all-https.ttl";
	
	private static final IRI CONTEXT_ACTIVITYSTREAMS_DEFINITIONS = iri(AS.NAMESPACE + ACTIVITYSTREAMS_DEFINITIONS);
	private static final IRI CONTEXT_SCHEMA_ORG_DEFINITIONS = iri(SCHEMA_ORG.NAMESPACE + SCHEMA_ORG_DEFINITIONS);

	private Repository typeRepository;	

	public SimpleRdfTypeRepository(ApRepositoryManager apRepositoryManager) {
		this.typeRepository = apRepositoryManager.getRdfTypeRepository();
		init();
	}
	
	private void init() {
				
		try(RepositoryConnection con = typeRepository.getConnection()) {
			
			Set<IRI> contexts = getContexts(con);
			
			try {  
				if(!contexts.contains(CONTEXT_ACTIVITYSTREAMS_DEFINITIONS)) {
					// turtle file is available via maven dependency: de.naturzukunft.rdf4j:vocabulary
					addTypeDefinition(con, new ClassPathResource(ACTIVITYSTREAMS_DEFINITIONS).getInputStream(), CONTEXT_ACTIVITYSTREAMS_DEFINITIONS, RDFFormat.TURTLE);
				}
				if(!contexts.contains(CONTEXT_SCHEMA_ORG_DEFINITIONS)) {
					// turtle file is available via maven dependency: de.naturzukunft.rdf4j:vocabulary
					addTypeDefinition(con, new ClassPathResource(SCHEMA_ORG_DEFINITIONS).getInputStream(), CONTEXT_SCHEMA_ORG_DEFINITIONS, RDFFormat.TURTLE);
				}
			} catch (IOException e) {
				throw new RuntimeException("unable to read type definition - " + e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Adds the passed type definitions (RDF schema) to the passed connection unsing
	 * the passe <a href=
	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
	 * 
	 * @param definitionAsTurtle a RDF Turtle file that contains type definitions.
	 * @param context See https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext
	 */
	@Override
	public IRI addTurtleTypeDefinition(String definitionAsTurtle, IRI context) {
		log.debug("->addTurtleTypeDefinition("+context+")");
		if(containsContext(context)) {
			throw new RuntimeException("context already exists, please remove it before adding it again.");
		}
		try (RepositoryConnection con = typeRepository.getConnection()){
			addTypeDefinition(con, new StringReader(definitionAsTurtle), context, RDFFormat.TURTLE);
			return context;
		}
	}

	private void addTypeDefinition(RepositoryConnection con, StringReader definitionAsTurtle, IRI context, RDFFormat rdfFormat) {
		try {			
			con.add(definitionAsTurtle, rdfFormat, context);
			log.debug("context added: " + context.stringValue());
		} catch (RDFParseException | RepositoryException | IOException e) {
			throw new RuntimeException("error loading " + context.stringValue(), e);
		}
	}

	private void addTypeDefinition(RepositoryConnection con, InputStream definitionAsTurtle, IRI context, RDFFormat rdfFormat) {
		try {			
			con.add(definitionAsTurtle, rdfFormat, context);
			log.debug("context added: " + context.stringValue());
		} catch (RDFParseException | RepositoryException | IOException e) {
			throw new RuntimeException("error loading " + context.stringValue(), e);
		}
	}

	/**
	 * @return all <a href=
	 *         "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">contexts</a>
	 *         that are part of that repository.
	 */
	@Override
	public Set<IRI> getContexts() {
		try (RepositoryConnection con = typeRepository.getConnection()){
			return getContexts(con);
		}
	}
	
	private Set<IRI> getContexts(RepositoryConnection con) {
		RepositoryResult<Resource> result = con.getContextIDs();
		Set<IRI> contextIds = result.stream().map(contextId->(IRI)contextId).collect(Collectors.toSet());
		result.close();
		return contextIds;
	}

	/**
	 * @param context See https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext 
	 * @return true, if the passed <a href=
	 *         "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>
	 *         is part of that repository, otherwise false.
	 */
	@Override
	public boolean containsContext(IRI context) {
		return getContexts().contains(context);
	}

	/**
	 * Remove all {@link Statements} in the passed <a href=
	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
	 * @param context See https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext
	 * @return false if the passed context is not available.
	 */
	@Override
	public boolean removeAll(IRI context) {
		try (RepositoryConnection con = typeRepository.getConnection()){
			if(!getContexts(con).contains(context)) {
				return false;
			}
			con.remove((Resource)null, null, null, context);
			return true;
		}
	}

	public Set<IRI> getAllSubjectsThatAreASubclassOfAsObject(Model model) {
		try (RepositoryConnection conn = typeRepository.getConnection()) {
			conn.add(model, TEMP_MODEL); // ?? Was hab ich mir dabei gedacht ??
			
			String queryString = "SELECT DISTINCT * "
					+ "WHERE {?subject <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>/rdfs:subClassOf* <" + AS.Object + ">  . }";
						
			Set<IRI> result = conn.prepareTupleQuery(queryString).evaluate().stream()
								.map(bindingName->bindingName.getBinding("subject"))
								.map(binding->binding.getValue().stringValue())
								.map(subjectString->iri(subjectString))
								.collect(Collectors.toSet());
			
			conn.remove(model, TEMP_MODEL); // ?? Was hab ich mir dabei gedacht ??
			return result;
		}
	}
	
	public boolean isSubclassOf(Set<IRI> types, IRI expected) {
		boolean isSubclassOf = false;
		for (IRI type : types) {
			isSubclassOf = isSubclassOf(type, expected);
			if(isSubclassOf) {
				break;
			}
		}
		return isSubclassOf;
	}
	
	public boolean isSubclassOf(IRI type, IRI expected) {
		boolean isSubclass = false;
		String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "			SELECT DISTINCT * WHERE { <"+type+"> rdfs:subClassOf* ?superClass . }";
			
		List<String> superClasses = new ArrayList<>();
		try (RepositoryConnection conn = typeRepository.getConnection()) {
			TupleQuery tupleQuery = conn.prepareTupleQuery(queryString);
			TupleQueryResult result = tupleQuery.evaluate();
			while (result.hasNext()) {
				String superClass = result.next().getValue("superClass").stringValue();
				superClasses.add(superClass);
				if(expected.stringValue().equals(superClass)) {
					isSubclass = true;
				}
			}
		}
		String isOrIsNot = isSubclass ? "isSubClassOf" : "isNOTSubClassOf"; 
		log.trace(type  + " " + isOrIsNot + " " + expected + " - superClasses: " + superClasses);
		return isSubclass;
	}

	@Override
	public Model read(IRI context) {
		log.debug("->read("+context+")");
		try (RepositoryConnection conn = typeRepository.getConnection()) {
			Model model = new ModelBuilder().build();
			model.addAll(Iterations.asSet(conn.getStatements(null, null, null, context)));
			log.debug("<-read("+context+") statements: " + model.size());
			return model;
		}		
	}
}
