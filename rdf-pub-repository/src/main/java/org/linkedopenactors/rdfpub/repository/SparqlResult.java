package org.linkedopenactors.rdfpub.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SparqlResult {
	private String result;
	private String mimeType;
}
