package org.linkedopenactors.rdfpub.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IdentityProviderUser {
	private String userId;
	private String preferredUserName;
}
