package org.linkedopenactors.rdfpub.repository;


import static org.eclipse.rdf4j.model.util.Values.literal;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.util.Repositories;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluator;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleApRepository extends AbstractApRepository {

	private ApRepositoryData apRepositoryData;
	private IRI inboxCollectionNamedGraph;
	private IRI outboxCollectionNamedGraph;
	private IdentifyRecipients identifyRecipients;  
	
	public SimpleApRepository(ApRepositoryData apRepositoryData, SparqlQueryEvaluator sparqlQueryEvaluator, IdentifyRecipients identifyRecipients) {
		super(sparqlQueryEvaluator);
		this.apRepositoryData = apRepositoryData;
		this.identifyRecipients = identifyRecipients;		
		inboxCollectionNamedGraph = apRepositoryData.getInboxCollectionNamedGraph();
		outboxCollectionNamedGraph = apRepositoryData.getOutboxCollectionNamedGraph();
	}

	@Override
	protected IRI determineNamedGraphToUse(AvailableCollection availableCollection) {
		switch (availableCollection) {
		case inbox:
			return inboxCollectionNamedGraph;
		case outbox:
			return  outboxCollectionNamedGraph;
			 
		default:
			throw new RuntimeException("unsupported collection '"+availableCollection.name()+"'");
		}
	}

	@Override
	public Model getProfile() {
		try(RepositoryConnection con = apRepositoryData.getRepository().getConnection()) {
			return QueryResults.asModel(con.getStatements(apRepositoryData.getActorIri(), null, null, apRepositoryData.getActorIri()));
		}
	}

	@Override
	public void processUpdateOutboxActivity(IRI activityIri, IRI objectIri, Model model) {
		Model activityOnly = model.filter(activityIri, null, null);
		Model objectOnly = model.filter(objectIri, null, null);

		Repositories.consume(apRepositoryData.getRepository(), con -> {
			con.add(activityOnly, apRepositoryData.getActorIri(), outboxCollectionNamedGraph);

			if(objectExists(objectIri, con)) {
				objectOnly.remove(objectIri, AS.updated, null, outboxCollectionNamedGraph);
				objectOnly.add(objectIri, AS.updated, literal(Instant.now().toString()), outboxCollectionNamedGraph);
				ModelLogger.debug(log, objectOnly, "before save");
				objectOnly.stream().forEach(stmt->{
					log.trace("stmt: " + stmt);
					con.remove(stmt.getSubject(), stmt.getPredicate(), null, getContextsToClean(objectIri));
					con.add(stmt.getSubject(), stmt.getPredicate(), stmt.getObject(), getContextsToAdd(objectIri, model));
				});

			} else {
				throw new IllegalStateException("object '"+objectIri.stringValue()+"' does not exist");
			}
		});
	}

	private Resource[] getContextsToAdd(IRI objectIri, Model model) {
		Set<IRI> contextsToAdd = new HashSet<>();
		contextsToAdd.addAll(identifyRecipients.process(apRepositoryData.getActorIri(), new ModelAndSubject(objectIri, model)));				
		contextsToAdd.add(apRepositoryData.getActorIri());
		contextsToAdd.add(outboxCollectionNamedGraph);
		Resource[] contextsToAddArray = contextsToAdd.toArray(new Resource[] {});
		return contextsToAddArray;
	}

	private Resource[] getContextsToClean(IRI objectIri) {
		Set<IRI> contextsToClean = new HashSet<>();
		contextsToClean.addAll(determinateReceiversInExistingObject(objectIri));
		contextsToClean.add(apRepositoryData.getActorIri());
		contextsToClean.add(outboxCollectionNamedGraph);				
		Resource[] contextsToCleanArray = contextsToClean.toArray(new Resource[] {});
		return contextsToCleanArray;
	}

	private Set<IRI> determinateReceiversInExistingObject(IRI objectIri) {
		try(RepositoryConnection con = apRepositoryData.getRepository().getConnection()) {
			Model model = new ModelBuilder().build();
			con.getNamespaces().forEach(ns->model.setNamespace(ns));
			model.addAll(Iterations.asSet(con.getStatements(objectIri, null,  null)));
			return identifyRecipients.process(apRepositoryData.getActorIri(), new ModelAndSubject(objectIri, model));
		}
	}

	private boolean objectExists(IRI objectIri, RepositoryConnection con) {
		RepositoryResult<Statement> statements = con.getStatements(objectIri, null, null, outboxCollectionNamedGraph);
		List<Statement> s = Iterations.asList(statements);
		return !s.isEmpty();
	}

	@Override
	public IRI save(ModelAndSubject activity, Set<IRI> contexts) {
		log.trace("using contexts: " + contexts);
		Repositories.consume(apRepositoryData.getRepository(), con -> {
			Resource[] contextsArray = contexts.toArray(new Resource[] {});
			con.add(activity.getModel(), contextsArray);
		});
		return activity.getSubject();
	}

	@Override
	protected IRI getNamedGraph() {
		return apRepositoryData.getActorIri();
	}

	@Override
	protected Repository getRepository() {
		return apRepositoryData.getRepository();
	}

	@Override
	protected Repository getSparqlReadRepository() {
		return apRepositoryData.getSparqlReadRepository();
	}
}
