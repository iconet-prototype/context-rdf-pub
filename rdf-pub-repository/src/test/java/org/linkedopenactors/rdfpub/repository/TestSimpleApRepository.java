package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.event.Level;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;
import de.naturzukunft.rdf4j.vocabulary.AS;

class TestSimpleApRepository {

	private IdentityProvider identityProvider = new IdentityProvider() {
		@Override
		public List<IdentityProviderUser> search(IRI actorId) {
			return List.of(new IdentityProviderUser("userId", ACTOR1.getLocalName()));
		}

		@Override
		public void logIdentityProviderProperties(Logger logger, Level level) {
			logIdentityProviderProperties(logger, level, "logIdentityProviderProperties");
		}

		@Override
		public void logIdentityProviderProperties(Logger logger, Level level, String msgParam) {
			String msg = msgParam + " - DUMMY";
			switch (level) {
			case INFO:
				logger.info(msg);			
				break;

			case DEBUG:
				logger.debug(msg);			
				break;

			case TRACE:
				logger.trace(msg);			
				break;

			case WARN:
				logger.warn(msg);			
				break;

			case ERROR:
				logger.error(msg);			
				break;

			default:
				logger.trace(msg);
				break;
			}
		}

		@Override
		public IRI normalize(IRI actorId) {
			return actorId;
		}

		@Override
		public List<IdentityProviderUser> search(String actorName) {
			return List.of(new IdentityProviderUser("userId", ACTOR1.getLocalName()));
		}

		@Override
		public Optional<IdentityProviderUser> getUser(IRI actorId) {
			List<IdentityProviderUser> users = search(actorId);
			if(users.size()>1) {
				throw new RuntimeException("more than one user for " + actorId);
			} else {
				return users.stream().findFirst();
			}
		}
};

 	private IdentifyRecipients identifyRecipients = new IdentifyRecipients() {
		@Override
		public Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject) {
			return Collections.emptySet();
		}
 	};
 	
	private static final IRI SUBJECT_ACTIVITY1 = iri("http://example.com/activity1");
	private static final IRI SUBJECT_OBJECT1 = iri("http://example.com/object1");
	private static final IRI ACTOR1 = iri("http://example.com/actor1");
	private ApRepository simpleApRepository;

	@BeforeEach
	public void init() {
		RepositoryManager rm =  RepositoryProvider.getRepositoryManager("./target/repoManager");
		ApRepositoryManager apr = new OneForAllApRepositoryManager(rm, new SparqlQueryEvaluatorRdf4j(), identifyRecipients, identityProvider);
		apr.createActorsRepository4Person(ACTOR1);
		simpleApRepository = apr.getActorsRepository(ACTOR1);
	}
	
	@Test
	void testCreateAndReadActivity() {
		
		Model initialModel = new ModelBuilder()
				.subject(SUBJECT_ACTIVITY1)
				.add(RDF.TYPE, AS.Create)
				.add(AS.name, literal("Test Activity"))
				.add(AS.object, SUBJECT_OBJECT1)
				.subject(SUBJECT_OBJECT1)
				.add(RDF.TYPE, iri("http://someObject"))
				.add(AS.name, literal("object name"))			
				.build();
		IRI activityIRI = simpleApRepository.save(new ModelAndSubject(SUBJECT_ACTIVITY1, initialModel), Set.of(ACTOR1, iri(ACTOR1.stringValue() + "/outbox")));
//		IRI activityIRI = simpleApRepository.createInboxActivity(new ModelAndSubject(SUBJECT_ACTIVITY1, initialModel));
		
		ModelAndSubject modelAndSubjectReaded = simpleApRepository.readObject(activityIRI).orElseThrow(()->new RuntimeException("cannot find created activity"));
		assertEquals(modelAndSubjectReaded.getSubject(), SUBJECT_ACTIVITY1);
		
		modelAndSubjectReaded.getModel().forEach(stmt->{
			Statement initialStmt = initialModel.filter(stmt.getSubject(), stmt.getPredicate(), stmt.getObject()).stream().findFirst()
					.orElseThrow(() -> new RuntimeException("not found: " + stmt));			
			assertEquals(initialStmt.getSubject(), stmt.getSubject());
			assertEquals(initialStmt.getPredicate(), stmt.getPredicate());
			assertEquals(initialStmt.getObject(), stmt.getObject());
			assertEquals(iri(ACTOR1.stringValue()), stmt.getContext());
		});
		// Only the activity is read, not the object!
		assertEquals(0, modelAndSubjectReaded.getModel().filter(SUBJECT_OBJECT1, null, null).size());
	}

	@Test
	void testCreateAndReadActivityMissingSubject() {
		Model initialModel = new ModelBuilder()
				.subject(SUBJECT_ACTIVITY1)
				.add(RDF.TYPE, AS.Create)
				.add(AS.name, literal("Test Activity"))
				.build();

		NullPointerException ex = Assertions.assertThrows(NullPointerException.class, () -> {
			simpleApRepository.save(new ModelAndSubject(null, initialModel), Collections.emptySet());
		  }); 
		assertEquals("subject is marked non-null but is null", ex.getMessage());
	}

	@Test
	void testNotFound() {
		Optional<ModelAndSubject> objectOptional = simpleApRepository.readObject(Values.iri("http://example.com/test123"));
		assertTrue(objectOptional.isEmpty());
	}

	@Test
	void testCreateAndReadActivityMissingModel() {
		NullPointerException ex = Assertions.assertThrows(NullPointerException.class, () -> {
			simpleApRepository.save(new ModelAndSubject(SUBJECT_ACTIVITY1, null), Collections.emptySet());
		  }); 
		assertEquals("model is marked non-null but is null", ex.getMessage());
	}
}
