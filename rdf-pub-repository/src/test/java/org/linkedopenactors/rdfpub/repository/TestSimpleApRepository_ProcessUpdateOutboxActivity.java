package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.Collections;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class TestSimpleApRepository_ProcessUpdateOutboxActivity {

	private static final IRI SUBJECT_ACTIVITY1 = iri("http://example.com/activity1");
	private static final IRI UPDATE_ACTIVITY1 = iri("http://example.com/activityUpdate");
	private static final IRI SUBJECT_OBJECT1 = iri("http://example.com/object1");	
	private static final IRI ACTOR1 = iri("http://example.com/actor1");
	private static final IRI INBOX = iri("http://example.com/actor1/inbox");
	private static final IRI OUTBOX = iri("http://example.com/actor1/outbox");
	private Repository repo;
	private ApRepositoryData apRepositoryData;
	private SimpleApRepository simpleApRepository;

 	private IdentifyRecipients identifyRecipients = new IdentifyRecipients() {
		@Override
		public Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject) {
			return Collections.emptySet();
		}
 	};

	@BeforeEach
	public void init() {
		repo = new SailRepository(new MemoryStore());
		apRepositoryData = new ApRepositoryData(ACTOR1, repo, repo, INBOX, OUTBOX);
		simpleApRepository = new SimpleApRepository(apRepositoryData, new SparqlQueryEvaluatorRdf4j(), identifyRecipients);
	}

	@Test
	void testUpdateExistingObject() {
		Model initialModel = new ModelBuilder()
				.subject(SUBJECT_ACTIVITY1)
					.add(RDF.TYPE, AS.Create)
					.add(AS.name, literal("Test Activity"))
					.add(AS.published, literal(Instant.now().toString()))
					.add(AS.object, SUBJECT_OBJECT1)
				.subject(SUBJECT_OBJECT1)
					.add(RDF.TYPE, iri("http://someObject"))
					.add(AS.published, literal(Instant.now().toString()))
					.add(AS.name, literal("object name"))			
				.build();
		simpleApRepository.save(new ModelAndSubject(SUBJECT_ACTIVITY1, initialModel), Set.of(ACTOR1, iri(ACTOR1.stringValue() + "/outbox")));
		
		ModelAndSubject objectReaded = simpleApRepository.readObject(SUBJECT_OBJECT1).orElseThrow(()->new RuntimeException("cannot find created activity"));
		ModelLogger.debug(log, objectReaded.getModel(), "rereadActivity");
		
		String publishedBeforeUpdate = Models.getPropertyLiteral(objectReaded.getModel(), SUBJECT_OBJECT1, AS.published).orElseThrow().stringValue();
		
		// Create update activity
		Model updateModel = new ModelBuilder()
				.subject(UPDATE_ACTIVITY1)
					.add(RDF.TYPE, AS.Update)
					.add(AS.name, literal("Test Update Activity"))
					.add(AS.object, SUBJECT_OBJECT1)
				.subject(SUBJECT_OBJECT1)
					.add(AS.name, literal("updated object name"))			
				.build();
		
		// process update		
		simpleApRepository.processUpdateOutboxActivity(SUBJECT_ACTIVITY1, SUBJECT_OBJECT1, updateModel);
		
		// reread object
		objectReaded = simpleApRepository.readObject(SUBJECT_OBJECT1).orElseThrow(()->new RuntimeException("cannot find created activity"));
		ModelLogger.debug(log, objectReaded.getModel(), "UPDATED: ");

		// check updated properties
		assertEquals(literal("updated object name"), Models.getPropertyLiteral(objectReaded.getModel(), SUBJECT_OBJECT1, AS.name).orElseThrow());

		// check published is unchanged
		String publishedAfterUpdate = Models.getPropertyLiteral(objectReaded.getModel(), SUBJECT_OBJECT1, AS.published).orElseThrow().stringValue();		
		
		// check updated date
		assertTrue(publishedBeforeUpdate.equals(publishedAfterUpdate));		
	}
	
	@Test
	void testUpdateNewObject() {
		IRI newObjectSubject = iri("http://example.com/max/outbox/o1");

		Model updateModel = new ModelBuilder()
				.subject(UPDATE_ACTIVITY1)
					.add(RDF.TYPE, AS.Update)
					.add(AS.name, literal("Test Update Activity"))
					.add(AS.object, newObjectSubject)
				.subject(newObjectSubject)
					.add(AS.name, literal("updated object name"))			
				.build();
		
		Assertions.assertThrows(IllegalStateException.class, () -> {
			simpleApRepository.processUpdateOutboxActivity(SUBJECT_ACTIVITY1, newObjectSubject, updateModel);
		});
	}
}
