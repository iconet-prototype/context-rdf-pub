package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.repository.ApRepository.AvailableCollection;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class TestSimpleApRepository_Collection {

	private static final IRI SUBJECT_ACTIVITY1 = iri("http://example.com/activity1");
	private static final IRI SUBJECT_OBJECT1 = iri("http://example.com/object1");
	private static final IRI ACTOR1 = iri("http://example.com/actor1");
	private static final IRI INBOX = iri("http://example.com/actor1/inbox");
	private static final IRI OUTBOX = iri("http://example.com/actor1/outbox");
	private Repository repo;
	private ApRepositoryData apRepositoryData;
	private SimpleApRepository simpleApRepository;

 	private IdentifyRecipients identifyRecipients = new IdentifyRecipients() {
		@Override
		public Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject) {
			return Collections.emptySet();
		}
 	};

	@BeforeEach
	public void init() {
		repo = new SailRepository(new MemoryStore());
		apRepositoryData = new ApRepositoryData(ACTOR1, repo, repo, INBOX, OUTBOX);
		simpleApRepository = new SimpleApRepository(apRepositoryData, new SparqlQueryEvaluatorRdf4j(), identifyRecipients);
	}

	@Test
	void testOutboxEasy() {
		Model outbox = simpleApRepository.getCollection(AvailableCollection.outbox, 100, 0);
		String items = Models.getPropertyLiteral(outbox, iri(ACTOR1.stringValue() + "/outbox?pageSize=100&startIndex=0"), AS.totalItems).orElseThrow().stringValue();
		assertEquals("0", items);
		
		Model initialModel = buildActivity(SUBJECT_ACTIVITY1, "Test Activity 1");
		IRI activityIRI = simpleApRepository.save(new ModelAndSubject(SUBJECT_ACTIVITY1, initialModel), Set.of(ACTOR1, iri(ACTOR1.stringValue() + "/outbox")));
		
		ModelAndSubject modelAndSubjectReaded = simpleApRepository.readObject(activityIRI).orElseThrow(()->new RuntimeException("cannot find created activity"));
		assertEquals(modelAndSubjectReaded.getSubject(), SUBJECT_ACTIVITY1);
		
		modelAndSubjectReaded.getModel().forEach(stmt->{
			Statement initialStmt = initialModel.filter(stmt.getSubject(), stmt.getPredicate(), stmt.getObject()).stream().findFirst()
					.orElseThrow(() -> new RuntimeException("not found: " + stmt));			
			assertEquals(initialStmt.getSubject(), stmt.getSubject());
			assertEquals(initialStmt.getPredicate(), stmt.getPredicate());
			assertEquals(initialStmt.getObject(), stmt.getObject());
			assertEquals(iri(ACTOR1.stringValue()), stmt.getContext());
		});
		// Only the activity is read, not the object!
		assertEquals(0, modelAndSubjectReaded.getModel().filter(SUBJECT_OBJECT1, null, null).size());
		
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, 100, 0);
		items = Models.getPropertyLiteral(outbox, iri(ACTOR1.stringValue() + "/outbox?pageSize=100&startIndex=0"), AS.totalItems).orElseThrow().stringValue();
		assertEquals("1", items);		
	}

	private Model buildActivity(IRI subject, String name) {
		return new ModelBuilder()
				.subject(subject)
					.add(RDF.TYPE, AS.Activity) // Do not use subtypes, because the repository does not have the data to determine inheritance
					.add(AS.name, literal(name))
					.add(AS.summary, literal("A simple summary"))
					.add(AS.published, Values.literal(Instant.now().toString()))
					.add(AS.actor, ACTOR1)
					.add(AS.object, SUBJECT_OBJECT1)
				.subject(SUBJECT_OBJECT1)
				.build();
	}
	
	@Test
	void testGetCollectionComplex() {
		Model outbox = simpleApRepository.getCollection(AvailableCollection.outbox, 100, 0);
		String items = Models.getPropertyLiteral(outbox, iri(ACTOR1.stringValue() + "/outbox?pageSize=100&startIndex=0"), AS.totalItems).orElseThrow().stringValue();
		assertEquals("0", items);
		Set<Model> models = new HashSet<>();
		for (int i = 0; i < 16; i++) {
			log.trace("building activity " + i);
			Model model = buildActivity(iri(SUBJECT_ACTIVITY1.getNamespace() + "/activity_"+(i+1)),"Test-Activity " + (i+1));
			models.add(model);
			simpleApRepository.save(new ModelAndSubject(SUBJECT_ACTIVITY1, model), Set.of(ACTOR1, iri(ACTOR1.stringValue() + "/outbox")));
		}

		int pageSize = 4;
		int offset = 0;
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, pageSize, offset);
		String subject = String.format(ACTOR1.stringValue() + "/outbox?pageSize=%d&startIndex=%d",pageSize,offset);
		items = Models.getPropertyLiteral(outbox, iri(subject), AS.totalItems).orElseThrow().stringValue();
		assertEquals("4", items);
		
		int i = 16;
		for (Iterator<Statement> iterator = outbox.getStatements(iri(subject), AS.items, null).iterator(); iterator.hasNext();) {			
			assertEquals(iri("http://example.com//activity_"+i), iterator.next().getObject());
			i--;
		}
		
		//// page 2
		offset = 4;
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, pageSize, offset);
		subject = String.format(ACTOR1.stringValue() + "/outbox?pageSize=%d&startIndex=%d",pageSize,offset);
		items = Models.getPropertyLiteral(outbox, iri(subject), AS.totalItems).orElseThrow().stringValue();
		assertEquals("4", items);
		
		for (Iterator<Statement> iterator = outbox.getStatements(iri(subject), AS.items, null).iterator(); iterator.hasNext();) {			
			assertEquals(iri("http://example.com//activity_"+i), iterator.next().getObject());
			i--;
		}

		//// page 3
		offset = 8;
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, pageSize, offset);
		subject = String.format(ACTOR1.stringValue() + "/outbox?pageSize=%d&startIndex=%d",pageSize,offset);
		items = Models.getPropertyLiteral(outbox, iri(subject), AS.totalItems).orElseThrow().stringValue();
		assertEquals("4", items);
		
		for (Iterator<Statement> iterator = outbox.getStatements(iri(subject), AS.items, null).iterator(); iterator.hasNext();) {			
			assertEquals(iri("http://example.com//activity_"+i), iterator.next().getObject());
			i--;
		}

		//// page 4
		offset = 12;
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, pageSize, offset);
		subject = String.format(ACTOR1.stringValue() + "/outbox?pageSize=%d&startIndex=%d",pageSize,offset);
		items = Models.getPropertyLiteral(outbox, iri(subject), AS.totalItems).orElseThrow().stringValue();
		assertEquals("4", items);
		
		for (Iterator<Statement> iterator = outbox.getStatements(iri(subject), AS.items, null).iterator(); iterator.hasNext();) {			
			assertEquals(iri("http://example.com//activity_"+i), iterator.next().getObject());
			i--;
		}

		//// page 5
		offset = 16;
		outbox = simpleApRepository.getCollection(AvailableCollection.outbox, pageSize, offset);
		subject = String.format(ACTOR1.stringValue() + "/outbox?pageSize=%d&startIndex=%d",pageSize,offset);
		items = Models.getPropertyLiteral(outbox, iri(subject), AS.totalItems).orElseThrow().stringValue();
		assertEquals("0", items);
	}
}
