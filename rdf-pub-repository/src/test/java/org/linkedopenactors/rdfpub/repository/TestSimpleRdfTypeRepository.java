package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;

class TestSimpleRdfTypeRepository {

	@Autowired
	private IdentityProvider identityProvider;
	private SimpleRdfTypeRepository rdfTypeRepository;
	
 	private IdentifyRecipients identifyRecipients = new IdentifyRecipients() {
 		@Override
		public Set<IRI> process(IRI actorId, ModelAndSubject modelAndSubject) {
			return Collections.emptySet();
		}
 	};

	@BeforeEach
	public void init() throws Exception {
		File tmpdir = new File(FilenameUtils.concat(System.getProperty("java.io.tmpdir"), "repoManager" + System.nanoTime()));
		RepositoryManager rm =  RepositoryProvider.getRepositoryManager(tmpdir);
		rm.init();
		rdfTypeRepository = new SimpleRdfTypeRepository(new OneForAllApRepositoryManager(rm, new SparqlQueryEvaluatorRdf4j(), identifyRecipients, identityProvider));
	}

	@Test
	void testAddTurtleTypeDefinition_Check_Remove_Check() throws IOException {
		IRI context = iri("http://www.w3.org/2005/01/wf/flow#");
		assertTrue(rdfTypeRepository.read(context).isEmpty());		
		String turtle = new String(new ClassPathResource("IssueTrackingWorkflow.ttl").getInputStream().readAllBytes(), StandardCharsets.UTF_8);
		rdfTypeRepository.addTurtleTypeDefinition(turtle, context);
		
		// check if exists
		assertTrue(rdfTypeRepository.read(context).size()>10);
		assertTrue(rdfTypeRepository.containsContext(context));
		assertTrue(rdfTypeRepository.getContexts().contains(context));
		
		assertTrue(rdfTypeRepository.removeAll(context));
		assertFalse(rdfTypeRepository.containsContext(context));
	}
	
	@Test
	void testAddTurtleTypeDefinitionTwice() throws IOException {
		IRI context = iri("http://www.w3.org/2005/01/wf/flow#");
		assertTrue(rdfTypeRepository.read(context).isEmpty());
		String turtle = new String(new ClassPathResource("IssueTrackingWorkflow.ttl").getInputStream().readAllBytes(), StandardCharsets.UTF_8);	
		rdfTypeRepository.addTurtleTypeDefinition(turtle, context);
		
		RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> {
			rdfTypeRepository.addTurtleTypeDefinition(turtle, context);
			});
		assertEquals("context already exists, please remove it before adding it again.", thrown.getMessage());
	}
	
	
	@Test
	void testAddInvalidTurtleTypeDefinition() throws IOException {
		RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> {
			rdfTypeRepository.addTurtleTypeDefinition(
					" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's",
					iri("http://example.com/test#"));
		});
		assertEquals("error loading http://example.com/test#", thrown.getMessage());
	}
	
	@Test
	void testIsSubClassOf() throws IOException {
		IRI context = iri("http://www.w3.org/2005/01/wf/flow#");
		assertTrue(rdfTypeRepository.read(context).isEmpty());		
		String turtle = new String(new ClassPathResource("IssueTrackingWorkflow.ttl").getInputStream().readAllBytes(), StandardCharsets.UTF_8);
		rdfTypeRepository.addTurtleTypeDefinition(turtle, context);
		assertTrue(rdfTypeRepository.isSubclassOf(iri("http://www.w3.org/2005/01/wf/flow#ActionItem"), iri("http://www.w3.org/2005/01/wf/flow#Task")));
	}		

	@Test
	void testRemoveNotExisting() throws IOException {
		assertFalse(rdfTypeRepository.removeAll(iri("http://example.com/test#")));
	}
}
