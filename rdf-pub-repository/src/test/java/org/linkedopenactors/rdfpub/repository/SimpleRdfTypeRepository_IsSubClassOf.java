package org.linkedopenactors.rdfpub.repository;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.naturzukunft.rdf4j.utils.sparql.SparqlQueryEvaluatorRdf4j;
import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class SimpleRdfTypeRepository_IsSubClassOf {
	@Autowired
	private IdentityProvider identityProvider;
	
	@Autowired
	private IdentifyRecipients identifyRecipients;
	
	private SimpleRdfTypeRepository isSubClassOf;
	
	@BeforeEach
	public void init() throws Exception {
		File tmpdir = new File(FilenameUtils.concat(System.getProperty("java.io.tmpdir"), "repoManager" + System.nanoTime()));
		RepositoryManager rm =  RepositoryProvider.getRepositoryManager(tmpdir);
		rm.init();
		isSubClassOf = new SimpleRdfTypeRepository(new OneForAllApRepositoryManager(rm, new SparqlQueryEvaluatorRdf4j(), identifyRecipients, identityProvider));
	}

	@Test
	public void testIsCreate()  {
		assertThat(isSubClassOf.isSubclassOf(AS.Create, AS.Create), is(true));
		assertThat(isSubClassOf.isSubclassOf(AS.Event, AS.Object), is(true));
	}

	@Test
	public void testIsCreateWithSet()  {
		assertThat(isSubClassOf.isSubclassOf(Set.of(AS.Create, AS.Event), AS.Object), is(true));
	}

	@Test
	public void testIsAnnounce()  {
		assertThat(isSubClassOf.isSubclassOf(AS.Announce, AS.Announce), is(true));	
	}
	
	@Test
	public void testNegative()  {
		assertThat(isSubClassOf.isSubclassOf(AS.Announce, RDF.TYPE), is(false));	
	}
	
	@Test
	public void testGetAllSubjectsThatAreASubclassOfAsObject() {
		IRI object = iri("http://example.com/Object");
		IRI create = iri("http://example.com/create");
		
		Model model = new ModelBuilder().build();
		model.add(iri("http://example.com/objectProperty"), RDF.TYPE, AS.object);		
		model.add(object, RDF.TYPE, AS.Object);		
		model.add(create, RDF.TYPE, AS.Create);
		model.add(iri("http://example.com/email"), RDF.TYPE, SCHEMA_ORG.EmailMessage);
		
		Set<IRI> objects = isSubClassOf.getAllSubjectsThatAreASubclassOfAsObject(model);
		
		assertThat(objects.contains(object), is(true));
		assertThat(objects.contains(create), is(true));
		assertThat(objects.size(), is(2));
	}
}
